#!/bin/bash
# - Setup a local DV dev folder
# - Copy there isolation Tuple Tools and compile them
PLATFORM=x86_64-slc6-gcc62-opt
DVVER=v42r1
# Pick analysis version that corresponds to the DV version
ANALYSISVER="$(lb-run DaVinci/$DVVER echo \$ANALYSIS_PROJECT_ROOT | sed -r 's/.*ANALYSIS_(v.*)/\1/')"
DVPARFOLDER=$PWD # Where you wish your DV dev to live
mkdir -p $DVPARFOLDER
DVDEV=${DVPARFOLDER}/DaVinciDev_${DVVER}
ISODEVDIR=./IsoTTDev
LbLogin -c $PLATFORM && \
cd $DVPARFOLDER
# Either a DV dev folder exists or make one
[[ -e `basename ${DVDEV}` || $(lb-dev DaVinci $DVVER) ]] && \
if [[ "$1" = "-u" ]]
then
    echo "Updating Analysis and Phys" && \
	cd DaVinciDev_${DVVER}/ && \
	git lb-use Analysis && \
	git lb-checkout Analysis/$ANALYSISVER Phys/DecayTreeTuple && \
	cd -
fi
#cp -v /afs/cern.ch/work/d/dgerstel/TupleProduction/DaVinciDev_v42r1/Phys/DecayTreeTuple/src/* ${DVDEV}/Phys/DecayTreeTuple/src/ && \
# Copy all needed tuple tools
cp -v ${DVPARFOLDER}/otherTTs/* ${DVDEV}/Phys/DecayTreeTuple/src/ && \
cp -v ${ISODEVDIR}/TupleToolIsoGeneric.* ${DVDEV}/Phys/DecayTreeTuple/src/ && \
# Copy TTIG weights
mkdir -p ${DVDEV}/Phys/DecayTreeTuple/src/weights && \
cp -v ${ISODEVDIR}/weights/* ${DVDEV}/Phys/DecayTreeTuple/src/weights/ && \
# Get rid of unwanted TT
mv ${DVDEV}/Phys/DecayTreeTuple/src/*Bs2KmuNu* ./tmp/
lb-run DaVinci/${DVVER} make -j8 -C $DVDEV configure && \
lb-run DaVinci/${DVVER} make -j8 -C $DVDEV install
