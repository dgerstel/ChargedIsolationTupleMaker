"""
 Script to create a DecayTreeTuple and BTA tuple for B2JpsiK, Bd2JpsiKstar and Bs2JpsiPhi analyses:
	"""
__author__ = "Vanessa Mueller <vanessa.mueller@tu-dortmund.de>", "Mirco Dorigo <mirco.dorigo@cern.ch>"

##############################################################################################
##### SETTINGS
##############################################################################################

from Configurables import DaVinci

DaVinci().EvtMax = -1
DaVinci().SkipEvents = 0
DaVinci().PrintFreq = 500
DaVinci().DataType = "2012"
DaVinci().Simulation   = False
DaVinci().Lumi = True
DaVinci().TupleFile = "DTT_B2JpsiX_ALTR.root"


from Configurables import CondDB

if DaVinci().Simulation == False and DaVinci().DataType == "2012":
	CondDB( LatestGlobalTagByDataType = "2012" )

if DaVinci().Simulation == False and DaVinci().DataType == "2011":
	CondDB( LatestGlobalTagByDataType = "2011" )

if DaVinci().Simulation == False and DaVinci().DataType == "2015":
	CondDB( LatestGlobalTagByDataType = "2015" )

f DaVinci().Simulation == False and DaVinci().DataType == "2016":
	CondDB( LatestGlobalTagByDataType = "2016" )

## do the restripping (only applicable to simulation)
restrip_mc = False

## update _TRACK_GhostProb (relevant for downstream tracks)
update_track_ghostprob = False

preselection = True

## use the DecayTreeFitter
dtf = True 
import os , sys
os.getcwd()
sys.path.append(os.getcwd())

##############################################################################################
##### GLOBALS
##############################################################################################
prefix = ''
if DaVinci().Simulation == True:
	prefix = "AllStreams"
else:
	prefix = "Dimuon"

print("prefix is " + prefix)

##############################################################################################
##### IMPORTS
##############################################################################################

import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
import copy
from Configurables import (BTagging,
			   BTaggingTool,
			   CheckPV,
			   CombineParticles,
			   DaVinci,
			   TESCheck,
			   FilterDesktop,
			   FitDecayTrees,
			   GaudiSequencer,
			   OfflineVertexFitter,
			   PrintDecayTree,
			   PrintDecayTreeTool,
			   LoKi__Hybrid__TupleTool,
			   TupleToolDecay,
			   TupleToolP2VV,
			   TupleToolRecoStats,
			   TupleToolTagging,
			   TupleToolTISTOS,
			   TupleToolStripping,
			   TupleToolTrackPosition,
			   TupleToolTrigger,
			   TupleToolEventInfo,
			   TupleToolVtxIsoln,
			   TupleToolGeometry,
			   TupleToolTrackInfo,
			   TupleToolPid,
			   TupleToolMassHypo,
			   EventTuple,
			   TupleToolMCTruth,
			   MCTupleToolPrimaries,
			   MCTupleToolHierarchy,
			   MCTupleToolKinematic)

from Configurables import (BTaggingAnalysis,
			   TriggerTisTos,
			   TaggingUtils,
			   TaggingUtilsChecker,
			   BDecayTool,
			   MCDecayFinder)

from Configurables import (NNetTool_MLP,
			   TaggerMuonTool,
			   TaggerElectronTool,
			   TaggerKaonOppositeTool,
			   TaggerVertexChargeTool,
			   TaggerCharmTool)

from Configurables import TrackSmearState as SMEAR
from Configurables import TrackScaleState

import re

from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand

if DaVinci().Simulation == True:
	from Configurables import (TupleToolMCDecayTree, TupleToolTaggingMC)

class Decay(object):
	def __init__(self, other=None):
		if other != None:
			self.__dict__ = other.__dict__.copy()

##############################################################################################

##############################################################################################
##### SELECTIONS
##############################################################################################
seqB2JpsiX        = GaudiSequencer('seqB2JpsiX')
TupleSeq          = GaudiSequencer('TupleSeq')

if preselection:

  import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
  from Configurables import CombineParticles
  
  inputBd2JpsiKst = DataOnDemand( Location = prefix + "/Phys/BetaSBd2JpsiKstarDetachedLine/Particles" )
  inputBu2JpsiK = DataOnDemand( Location = prefix + "/Phys/BetaSBu2JpsiKDetachedLine/Particles" )


  cutsBd2JpsiKst_mu = "INGENERATION( (ABSID=='mu+') & (MIPCHI2DV(PRIMARY)>16) & (PT>500*MeV) & (PIDmu>0),2 )"
  cutsBd2JpsiKst_jpsi = "INTREE( (ID=='J/psi(1S)') & (M>3036*MeV) & (M<3156*MeV) )"
  cutsBd2JpsiKst_k = "INGENERATION( (ABSID=='K+') & (MIPCHI2DV(PRIMARY)>2) & (PIDK > 0) & (PT>500*MeV),2 )"
  cutsBd2JpsiKst_pi = "INGENERATION( (ABSID=='pi+') & (MIPCHI2DV(PRIMARY)>2) & (PT>500*MeV),2 )"
  cutsBd2JpsiKst_kst = "INTREE( (ABSID=='K*(892)0') & (M>826*MeV) & (M<966*MeV) )"
  cutsBd2JpsiKst_b = "(M>5000*MeV) & (M<6000*MeV) & (BPVIPCHI2()<25) & (BPVLTIME('PropertimeFitter/properTime:PUBLIC')>0.2*ps)"

  cutsBd2JpsiKst = "(" + cutsBd2JpsiKst_mu + " & " + cutsBd2JpsiKst_jpsi + " & " + cutsBd2JpsiKst_k + " & " + cutsBd2JpsiKst_pi + " & " + cutsBd2JpsiKst_kst + " & " + cutsBd2JpsiKst_b + ")"
  # cutsBd2JpsiKst = "ALL"

  cutsBu2JpsiK_mu = "INGENERATION( (ABSID=='mu+') & (PT>500*MeV) & (PIDmu>0),2 )"
  cutsBu2JpsiK_jpsi = "INTREE( (ID=='J/psi(1S)') & (M>3036*MeV) & (M<3156*MeV) )"
  cutsBu2JpsiK_k = "INGENERATION( (ABSID=='K+') & (PT>1000*MeV) & (P>10000*MeV) & (PIDK>0),1 )"
  cutsBu2JpsiK_b = "(M>5000*MeV) & (M<6000*MeV) & (BPVIPCHI2()<25) & (BPVLTIME('PropertimeFitter/properTime:PUBLIC')>0.2*ps)"

  cutsBu2JpsiK   = "(" + cutsBu2JpsiK_mu + " & " + cutsBu2JpsiK_jpsi + " & " + cutsBu2JpsiK_k + " & " + cutsBu2JpsiK_b + ")"
  # cutsBu2JpsiK = "ALL"
  
  filterBd2JpsiKst = FilterDesktop("filterBd2JpsiKst",
                           Code = cutsBd2JpsiKst
                           )
  filterBu2JpsiK = FilterDesktop("filterBu2JpsiK",
                           Code = cutsBu2JpsiK
                           )
  
  selBd2JspiKst = Selection("selBd2JspiKst",
                          Algorithm =  filterBd2JpsiKst ,
                          RequiredSelections = [ inputBd2JpsiKst  ] )
  selBuJspiK    = Selection("selBuJspiK",
                          Algorithm =  filterBu2JpsiK ,
                          RequiredSelections = [ inputBu2JpsiK  ] )
  
  seqBd2JspiKst = SelectionSequence("seqBd2JspiKst", TopSelection = selBd2JspiKst)
  seqBuJspiK = SelectionSequence("seqBuJspiK", TopSelection = selBuJspiK )
  
  DaVinci().appendToMainSequence( [seqBd2JspiKst.sequence(), seqBuJspiK.sequence()] )


##############################################################################################

##############################################################################################
##### DECAYS
##############################################################################################
# bs2jpsiphi_detached_dict = {
# 	"tuple_name"        : "Bs2JpsiPhiDetached",
# 	"decay_descriptor"  : "[B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-)]CC",
# 	"inputs"            : prefix + "/Phys/BetaSBs2JpsiPhiDetachedLine/Particles",
# 	"p2vv"              : "Bs2JpsiPhiAngleCalculator",
# 	"daughters_to_constrain" : [["J/psi(1S)", "phi(1020)"]],
# 	"BTA_name"          : "Bs2JpsiPhi_TBA"
# 	}
# bs2jpsiphi_detached = Decay()
# bs2jpsiphi_detached.__dict__ = bs2jpsiphi_detached_dict

bd2jpsikstar_detached_dict = {
	"tuple_name"          : "Bd2JpsiKstarDetached",
	"decay_descriptor"    : "[B0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(K*(892)0 -> ^K+ ^pi-)]CC",
	"inputs"              : seqBd2JspiKst.outputLocation(),
	"p2vv"                : "Bd2KstarMuMuAngleCalculator",
	"daughters_to_constrain" : [["J/psi(1S)"]],
	"BTA_name"            : "Bd2JpsiKstar_TBA"
	}
bd2jpsikstar_detached = Decay()
bd2jpsikstar_detached.__dict__ = bd2jpsikstar_detached_dict

bu2jpsik_detached_dict = {
	"tuple_name"          : "Bu2JpsiKDetached",
	"decay_descriptor"    : "[B+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^K+]CC",
	"inputs"              : seqBuJspiK.outputLocation(),
	"daughters_to_constrain" : [["J/psi(1S)"]],
	"BTA_name"            : "Bu2JpsiK_TBA"
	}
bu2jpsik_detached = Decay()
bu2jpsik_detached.__dict__ = bu2jpsik_detached_dict

# lb2Jpsimumul_dict = {
#   "tuple_name"        : "Lambdab2JpsimumuLambda",
#   "decay_descriptor"  : "[Lambda_b0 -> ^(Lambda0 -> ^p+ ^pi-) ^(J/psi(1S) -> ^mu+ ^mu-) ]CC",
#   "inputs"            : prefix + "/Phys/B2XMuMu_Line/Particles",  
#   "daughters_to_constrain" : [["J/psi(1S)", "Lambda0"]],  
#   "particle_masses_to_constrain" : [[3096.916, 1115.683]], 
#   "BTA_name"            : "Lb2JpsiL_TBA"
# }
# lb2Jpsimumul = Decay()
# lb2Jpsimumul.__dict__ = lb2Jpsimumul_dict

##############################################################################################
##### Update of _TRACK_GhostProb for downstream tracks
##############################################################################################
# from STTools import STOfflineConf
# STOfflineConf.DefaultConf().configureTools()
# from Configurables import RefitParticleTracks
# refitter = RefitParticleTracks()
# refitter.DoFit = True
# # shouldnt make a difference. either confirm that, or leave it as it is
# refitter.DoProbability = True
# refitter.UpdateProbability = True
# refitter.ReplaceTracks = True
# # shouldnt make a difference. either confirm that, or leave it as it is

# ## I never fully convinced myself whether the following three lines are
# ## necessary or not. Either way theyre not wrong:
# from Configurables import TrackInitFit, TrackMasterFitter
# from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter

# refitter.addTool(TrackInitFit,"TrackInitFit")
# refitter.TrackInitFit.addTool(TrackMasterFitter,"Fit")
# ConfiguredMasterFitter(refitter.TrackInitFit.Fit)

##############################################################################################
##### TOOLS AND TRIGGERS AND STRIPPING LINES AND LOKI VARIABLES
##############################################################################################
from CharmTagger import *
from ParticleTagger import *
from Configurables import (DaVinci ,
			   TupleToolIsoGeneric,
			   TupleToolVtxCharge)

tuple_tools = ["TupleToolKinematic",
	       "TupleToolPropertime",
	       "TupleToolPrimaries",
	       "TupleToolVtxCharge"
	       #,"TupleToolPid"
	       ]

tuple_tools_mc = ["TupleToolMCTruth",
		  "TupleToolMCBackgroundInfo",
		  "TupleToolMCDecayTree"]

loki_variables = {"LOKI_ENERGY"     : "E",
		  "LOKI_ETA"        : "ETA",
		  "LOKI_PHI"        : "PHI"}

loki_variables_B = {
		    "LOKI_DTF_CTAU"                   : "DTF_CTAU( 0, True )",
		    "LOKI_MASS_JpsiConstr"            : "DTF_FUN ( M , True , 'J/psi(1S)' )" 
		    }

stripping_lines = ['StrippingBetaSBs2JpsiPhiDetachedLineDecision',
		   'StrippingBetaSBd2JpsiKstarDetachedLineDecision',
		   'StrippingBetaSBu2JpsiKDetachedLineDecision']

# dictionary for relevant trigger lines
dict_trigger_lines = {
    '2011' : {
        'L0':   ['L0PhysicsDecision',
                 'L0MuonDecision',
                 'L0DiMuonDecision',
                 'L0MuonHighDecision',
                 'L0HadronDecision',
                 'L0ElectronDecision',
                 'L0PhotonDecision'],
        'HLT1': ['Hlt1GlobalDecision',
                 'Hlt1DiMuonHighMassDecision',
                 'Hlt1TrackMuonDecision',
                 'Hlt1TrackAllL0Decision'],
        'HLT2': ['Hlt2GlobalDecision',
                 'Hlt2DiMuonJPsiDecision',
                 'Hlt2DiMuonDetachedJPsiDecision'],
    },
    '2012' : {
        'L0':   ['L0PhysicsDecision',
                 'L0MuonDecision',
                 'L0DiMuonDecision',
                 'L0MuonHighDecision',
                 'L0HadronDecision',
                 'L0ElectronDecision',
                 'L0PhotonDecision'],
        'HLT1': ['Hlt1GlobalDecision',
                 'Hlt1DiMuonHighMassDecision',
                 'Hlt1TrackMuonDecision',
                 'Hlt1TrackAllL0Decision'],
        'HLT2': ['Hlt2GlobalDecision',
                 'Hlt2DiMuonJPsiDecision',
                 'Hlt2DiMuonDetachedJPsiDecision'],   
    },
    '2015' : {
        'L0':   ['L0PhysicsDecision',
                 'L0MuonDecision',
                 'L0DiMuonDecision',
                 'L0MuonHighDecision',
                 'L0HadronDecision',
                 'L0ElectronDecision',
                 'L0PhotonDecision'],
        'HLT1': ['Hlt1GlobalDecision',
                 'Hlt1DiMuonHighMassDecision',
                 'Hlt1TrackMVADecision', 
                 'Hlt1TwoTrackMVADecision', 
                 'Hlt1TrackMuonMVADecision'],
        'HLT2': ['Hlt2GlobalDecision',
                 'Hlt2DiMuonJPsiDecision',
                 'Hlt2DiMuonDetachedJPsiDecision'],
    },
    '2016' : {
        'L0':   ['L0PhysicsDecision',
                 'L0MuonDecision',
                 'L0DiMuonDecision',
                 'L0MuonHighDecision',
                 'L0HadronDecision',
                 'L0ElectronDecision',
                 'L0PhotonDecision'],
        'HLT1': ['Hlt1GlobalDecision',
                 'Hlt1DiMuonHighMassDecision',
                 'Hlt1TrackMVADecision', 
                 'Hlt1TwoTrackMVADecision', 
                 'Hlt1TrackMuonDecision'],
        'HLT2': ['Hlt2GlobalDecision',
                 'Hlt2DiMuonJPsiDecision',
                 'Hlt2DiMuonDetachedJPsiDecision'],
    },
}

trigger_lines = []

if DaVinci().DataType == "2011":
  triggerlists = dict_trigger_lines[2011]	
  trigger_lines = triggerlists['L0']+triggerlists['HLT1']+triggerlists['HLT2']
elif DaVinci().DataType =="2012":
  triggerlists = dict_trigger_lines[2012]	
  trigger_lines = triggerlists['L0']+triggerlists['HLT1']+triggerlists['HLT2']
elif DaVinci().DataType == "2015":
  triggerlists = dict_trigger_lines[2015]	
  trigger_lines = triggerlists['L0']+triggerlists['HLT1']+triggerlists['HLT2']
elif DaVinci().DataType == "2016":
   triggerlists = dict_trigger_lines[2016]	
  trigger_lines = triggerlists['L0']+triggerlists['HLT1']+triggerlists['HLT2']

def CreateTupleTool(decay):
  print "-----------------------------------------------------------------------" 
  print "---------------        Making B tuples             --------------------" 
  print "Adding decay " + decay.decay_descriptor
  print "Tuple name:", decay.tuple_name

  tuple = DecayTreeTuple(decay.tuple_name)
  tuple.OutputLevel = 6
  tuple.Inputs = [decay.inputs]
  tuple.Decay = decay.decay_descriptor
  tuple.ReFitPVs = True
  tools = copy.copy(tuple_tools)

  if DaVinci().Simulation == True:
	  tt_mct = tuple.addTupleTool("TupleToolMCTruth")
	  tt_mct.ToolList += ["MCTupleToolKinematic", "MCTupleToolHierarchy"]
	  tools.extend(tuple_tools_mc)

  tuple.ToolList = tools
  
  print tuple.ToolList
  # FOR TAGGING TRAINNING + STUDIES :
  #============================================================================
  addParticleTaggerTuple( tuple, decay )
  addCharmTuple( tuple , decay )
  #============================================================================
  ## specific tuple tools
  tt_stripping = tuple.addTupleTool("TupleToolStripping")
  tt_stripping.Verbose = True
  tt_stripping.StrippingList = stripping_lines

  tt_tagging = None
  if DaVinci().Simulation == True:
	  tt_tagging = tuple.addTupleTool("TupleToolTaggingMC")
  else:
	  tt_tagging = tuple.addTupleTool("TupleToolTagging")
  tt_tagging.Verbose = True
  #tt_tagging.StoreTaggersInfo = True

  if DaVinci().Simulation == True:
	  tt_tagging.TaggingToolName = "BTaggingTool/BsTaggingTool"
	  tt_tagging.addTool(BTaggingTool,name="BsTaggingTool")
	  #tt_tagging.Verbose = True
	  from FlavourTagging.Tunings import TuneTool
	  myTuneTool = TuneTool(tt_tagging,"Reco14_MC12","BsTaggingTool")  # sets the Reco14_MC12 tuning (default is Reco14_2012)

  #tt_trigger = tuple.addTupleTool("TupleToolTrigger")
  #tt_trigger.Verbose = True
  #tt_trigger.TriggerList = trigger_lines

  tt_eventinfo = tuple.addTupleTool("TupleToolEventInfo")
  #tt_eventinfo.Verbose = True

  tt_loki_general = tuple.addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_general")
  tt_loki_general.Variables = loki_variables

  #tt_trackpos = tuple.addTupleTool("TupleToolTrackPosition")
  #tt_trackpos.Z = 7500.

  #tt_recostats = tuple.addTupleTool("TupleToolRecoStats")
  #tt_recostats.Verbose = True

  tt_geometry = tuple.addTupleTool("TupleToolGeometry")
  #tt_geometry.Verbose = True
  #tt_geometry.RefitPVs = True
  #tt_geometry.FillMultiPV = True

  #tt_trackinfo = tuple.addTupleTool("TupleToolTrackInfo")
  #tt_trackinfo.Verbose = True

  name_cc_particle_decaydec = "J/psi(1S)"
  name_cc_particle_branch   = "J_psi_1S"

  branches = {}
  descriptor_B = decay.decay_descriptor.replace("^","")
  descriptor_cc = descriptor_B.replace("(" + name_cc_particle_decaydec, "^(" + name_cc_particle_decaydec)

  name_bbranch = "B"
  branches[name_bbranch] = "^(" + descriptor_B + ")"
  if "J/psi(1S)" in decay.decay_descriptor:
	  branches[name_cc_particle_branch] = descriptor_cc

  print descriptor_B

  if "add_branches" in decay.__dict__:
	  for name_branch, value in decay.add_branches.iteritems():
		  branches[name_branch] = descriptor_B.replace("(" + value[0], "^(" + value[0])
		  tuple.addTupleTool(TupleToolDecay, name=name_branch)
		  branch_infos = value[1]
		  if len(branch_infos) > 0:
			  tt_loki_br = tuple.__getattr__(name_branch).addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_"+name_branch)
			  tt_loki_br.Variables = branch_infos
		  else:
			  print "For branch " + name_branch + " no other LOKI variables specified."

  print "Decay branches:", branches
  tuple.addBranches(branches)
  tt_loki_B = tuple.__getattr__(name_bbranch).addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_B")
  if "loki_variables_B" in decay.__dict__:
	  tt_loki_B.Variables = dict(loki_variables_B.items() + decay.loki_variables_B.items())
  else:
	  tt_loki_B.Variables = loki_variables_B

  #tuple.addTupleTool(TupleToolDecay, name=name_bbranch)
  #tt_tistos_b = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolTISTOS")
  #tt_tistos_b.Verbose = True
  #tt_tistos_b.TriggerList = trigger_lines
  '''
  if "J/psi(1S)" in decay.decay_descriptor:
	  tuple.addTupleTool(TupleToolDecay, name="J_psi_1S")
	  tt_tistos_jpsi = tuple.__getattr__("J_psi_1S" ).addTupleTool("TupleToolTISTOS")
	  tt_tistos_jpsi.Verbose = True
	  tt_tistos_jpsi.TriggerList = trigger_lines

  ## DecayTreeFitter variables
  if dtf:
	  # without any constraints
	  FitwithoutConst = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolDecayTreeFitter/FitwithoutConst")
	  FitwithoutConst.Verbose = True
	  FitwithoutConst.UpdateDaughters = True
	  FitwithoutConst.daughtersToConstrain = []
	  FitwithoutConst.constrainToOriginVertex = False

	  # constraint only on PV (not on daughters)
	  FitPVConst = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolDecayTreeFitter/FitPVConst")
	  FitPVConst.Verbose = True
	  FitPVConst.UpdateDaughters = True
	  FitPVConst.daughtersToConstrain = []
	  FitPVConst.constrainToOriginVertex = True
	  
          # constraint only on daughters (not on PV)
	  i = 0
	  for daughters in decay.daughters_to_constrain:
		  fit_name = 'FitDaughtersConst'
		  if i>0:
			  fit_name = fit_name + str(i)
		  FitDaughtersConst = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolDecayTreeFitter/" + fit_name)
		  FitDaughtersConst.Verbose = True
		  FitDaughtersConst.UpdateDaughters = True
		  FitDaughtersConst.daughtersToConstrain = daughters
		  FitDaughtersConst.constrainToOriginVertex = False
		  i += 1

       	  # constraint on daughters and on the origin PV
	  i = 0
	  for daughters in decay.daughters_to_constrain:
		  fit_name = 'FitDaughtersPVConst'
		  if i>0:
			  fit_name = fit_name + str(i)
		  FitDaughtersPVConst = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolDecayTreeFitter/" + fit_name)
		  FitDaughtersPVConst.Verbose = True
		  FitDaughtersPVConst.UpdateDaughters = True
		  FitDaughtersPVConst.daughtersToConstrain = daughters
		  FitDaughtersPVConst.constrainToOriginVertex = True
		  i += 1

  '''
  print ""
  return tuple


# TupleSeq.Members += [  CreateTupleTool(bs2jpsiphi_detached)   ]
TupleSeq.Members += [  CreateTupleTool(bd2jpsikstar_detached) ]
TupleSeq.Members += [  CreateTupleTool(bu2jpsik_detached)     ]
# TupleSeq.Members += [  CreateTupleTool(lb2Jpsimumul)     ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
# check PVs
checkPV = CheckPV("CheckForOnePV")
checkPV.MinPVs = 1
print checkPV

##############################################################################

seqB2JpsiX.Members      += [checkPV, TupleSeq ]
seqB2JpsiX.ModeOR        = True
seqB2JpsiX.ShortCircuit  = False

evtTuple                 = EventTuple()
evtTuple.ToolList       += ["TupleToolEventInfo"]

from Configurables import GaudiSequencer
subseq_annpid = GaudiSequencer('SeqANNPID')

from Configurables import ChargedProtoANNPIDConf
conf_ann_pid = ChargedProtoANNPIDConf('ANNPIDConf')
conf_ann_pid.RecoSequencer = subseq_annpid

if DaVinci().Simulation == True:
	DaVinci().UserAlgorithms += [mctuple]
	if restrip_mc:
		DaVinci().UserAlgorithms += [sc.sequence()]

if DaVinci().Simulation == True:  
  smear = SMEAR( 'Smear' )  
  DaVinci().UserAlgorithms += [smear]

if DaVinci().Simulation == False:
  scaler = TrackScaleState('scaler')
  DaVinci().UserAlgorithms += [scaler]


if update_track_ghostprob:
	DaVinci().UserAlgorithms += [subseq_annpid]
	DaVinci().UserAlgorithms += [refitter]

DaVinci().UserAlgorithms += [seqB2JpsiX ]
#DaVinci().UserAlgorithms += [evtTuple]

CondDB().UseLatestTags = ["2012"] 
from GaudiConf import IOHelper
IOHelper().inputFiles([
	#'root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/DST/00047192_00000021_1.dimuon.dst'
	'/fhgfs/users/vmueller/dst_files/DIMUON_2012_MU_Reco14_Stripping21/00041834_00000007_1.dimuon.dst',
	'/fhgfs/users/vmueller/dst_files/DIMUON_2012_MU_Reco14_Stripping21/00041834_00000021_1.dimuon.dst',
	'/fhgfs/users/vmueller/dst_files/DIMUON_2012_MU_Reco14_Stripping21/00041834_00000035_1.dimuon.dst'
	], clear=True)
