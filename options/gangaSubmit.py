# Basem khanji,basem.khanji@cern.ch: ganga script to submit tagging tuples for RunI, RunII data (both DIMUON & BHADRON streams) in one go !
#
import os , re
#os.getcwd()
#sys.path.append(os.getcwd())

#
year         = [ '11'   ,    '12'    ,  '15'   , '16'   ]
energy       = [ '3500' ,    '4000'  ,  '6500' , '6500' ]  
strip_v      = [ '21r1' ,      '21'  ,  '24'   , '26'   ]
Reco_v       = [  '14'  ,     '14'   ,  '15a'  , '16'  ]
polarity     = [  'Up'  ,    'Down'  ]
streams      = ['DIMUON']

# make the paths list
job_setting   = { }
List_Of_Paths = []

for stm in streams:
    for pol in polarity:
        for i in range(len(year)):
            PATH_name ='/LHCb/Collision'+year[i]+'/Beam'+energy[i]+'GeV-VeloClosed-Mag'+pol+'/Real Data/Reco'+Reco_v[i]+'/Stripping'+strip_v[i]+'/90000000/'+stm+'.DST'
            print PATH_name
            job_name =  '20' +year[i] + '_Reco' + Reco_v[i] + 'Strip' + strip_v[i] + '_' + pol + '_' + stm 
            job_setting[job_name] = PATH_name 
            List_Of_Paths.append( PATH_name )

print '========================================'
print 'Filled the list of PATHS for ganga jobs'
print '========================================'
print job_setting            


for job_name , path_dict in job_setting.items():
    print '======================================'   
    print 'Sumbitting a new job ...'
    print path_dict , ','  , job_name
    bk_query = BKQuery( path  =  path_dict )
    dataset = bk_query.getDataset()
    job=Job(application=DaVinci(version='v40r2') ,
            name = job_name )
    Year = bool('2011' in job_name)*' "2011" '+bool('2012' in job_name)*' "2012" '+ bool('2015' in job_name)*' "2015"  ' + bool('2016' in job_name)*' "2016"  '
    job.application.optsfile = [
        bool('DIMUON' in job_name)*'./B2JpsiX_ALTR.py'  +  bool('BHADRONCOMPLETEEVENT' in job_name)*'./B2Dpi_ALTR.py'
        ]

    job.application.extraopts  =     (
        'DaVinci().TupleFile     = "DTT_'+ job_name + '.root"  ; ' +
        'DaVinci().EvtMax        =              -1             ; ' +
        'CondDB().UseLatestTags  =  ['+ Year +']             ; ' +
        'DaVinci().DataType      =   '+ Year +'              ; '
        )
    print "Create job for the jobs: ", job.name
    job.inputdata  = dataset 
    #job.outputfiles= [LocalFile(namePattern='*.root')] #['DTT_B02DstD_data.root']
    job.outputfiles= [DiracFile(namePattern='*.root',locations=['CERN-USER']) ] # keep my Tuples on grid element (retrive manually) 
    print "Calls splitter"
    job.splitter = SplitByFiles(filesPerJob = 20 , maxFiles = -1 , ignoremissing = True)
    # for LSF :
    print "Finally submit the job"
    job.backend    = Dirac()
    # queues.add(job.submit)
    print '======================================'   
    print "job: ", job.name + " submitted" 
    print '======================================'   
print " Jobs submitted .... bye "

