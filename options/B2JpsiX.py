"""
 Script to create a DecayTreeTuple and BTA tuple for B2JpsiK, Bd2JpsiKstar and Bs2JpsiPhi analyses:
	"""
__author__ = "Vanessa Mueller <vanessa.mueller@tu-dortmund.de>", "Mirco Dorigo <mirco.dorigo@cern.ch>"

##############################################################################################
##### SETTINGS
##############################################################################################

from Configurables import DaVinci

DaVinci().EvtMax = -1
DaVinci().SkipEvents = 0
DaVinci().PrintFreq = 500
DaVinci().DataType = "2012"
DaVinci().Simulation   = False
DaVinci().Lumi = True
DaVinci().TupleFile = "DTT.root"


from Configurables import CondDB

if DaVinci().Simulation == False and DaVinci().DataType == "2012":
	CondDB( LatestGlobalTagByDataType = "2012" )

if DaVinci().Simulation == False and DaVinci().DataType == "2011":
	CondDB( LatestGlobalTagByDataType = "2011" )

if DaVinci().Simulation == False and DaVinci().DataType == "2015":
	CondDB( LatestGlobalTagByDataType = "2015" )


## do the restripping (only applicable to simulation)
restrip_mc = False

## update _TRACK_GhostProb (relevant for downstream tracks)
update_track_ghostprob = False

## use the DecayTreeFitter
dtf = True

## number of events to process
max_events = -1
skip_events = -1



##############################################################################################
##### GLOBALS
##############################################################################################
prefix = ''
if DaVinci().Simulation == True:
	prefix = "AllStreams"
else:
	prefix = "Dimuon"

print("prefix is " + prefix)

from Configurables import TrackScaleState
if DaVinci().Simulation == False:
	scaler = TrackScaleState('scaler')
	DaVinci().UserAlgorithms += [scaler]

##############################################################################################
##### IMPORTS
##############################################################################################

import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
import copy
from Configurables import (BTagging,
			   BTaggingTool,
			   CheckPV,
			   CombineParticles,
			   DaVinci,
			   TESCheck,
			   FilterDesktop,
			   FitDecayTrees,
			   GaudiSequencer,
			   OfflineVertexFitter,
			   PrintDecayTree,
			   PrintDecayTreeTool,
			   LoKi__Hybrid__TupleTool,
			   TupleToolDecay,
			   TupleToolP2VV,
			   TupleToolRecoStats,
			   TupleToolTagging,
			   TupleToolTISTOS,
			   TupleToolStripping,
			   TupleToolTrackPosition,
			   TupleToolTrigger,
			   TupleToolEventInfo,
			   TupleToolVtxIsoln,
			   TupleToolGeometry,
			   TupleToolTrackInfo,
			   TupleToolPid,
			   TupleToolMassHypo,
			   EventTuple,
			   TupleToolMCTruth,
			   MCTupleToolPrimaries,
			   MCTupleToolHierarchy,
			   MCTupleToolKinematic)

from Configurables import (BTaggingAnalysis,
			   TriggerTisTos,
			   TaggingUtils,
			   TaggingUtilsChecker,
			   BDecayTool,
			   MCDecayFinder)


from Configurables import (SingleBCandidateSelection_BuJpsiK
			   ,SingleBCandidateSelection_BuDpi
			   ,SingleBCandidateSelection_BdJpsiKst
			   ,SingleBCandidateSelection_BsJpsiphi)

from Configurables import (NNetTool_MLP,
			   TaggerMuonTool,
			   TaggerElectronTool,
			   TaggerKaonOppositeTool,
			   TaggerVertexChargeTool,
			   TaggerCharmTool)

import re

from PhysSelPython.Wrappers import Selection, SelectionSequence

if DaVinci().Simulation == True:
	from Configurables import (TupleToolMCDecayTree, TupleToolTaggingMC)

class Decay(object):
	def __init__(self, other=None):
		if other != None:
			self.__dict__ = other.__dict__.copy()

##############################################################################################
# FOR TAGGING TRAINNING + STUDIES :
from CharmTagger import *
from ParticleTagger import *
createVeloTracks()
configIso()
DaVinci().appendToMainSequence( [ AllPions_seq ])
DaVinci().appendToMainSequence( [ AllCharmTaggers_seq ])
##############################################################################################

##############################################################################################
##### SELECTIONS
##############################################################################################
seqB2JpsiX        = GaudiSequencer('seqB2JpsiX')
TupleSeq          = GaudiSequencer('TupleSeq')
##############################################################################################

##############################################################################################
##### DECAYS
##############################################################################################
bs2jpsiphi_detached_dict = {
	"tuple_name"        : "Bs2JpsiPhiDetached",
	"decay_descriptor"  : "[B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-)]CC",
	"inputs"            : prefix + "/Phys/BetaSBs2JpsiPhiDetachedLine/Particles",
	"p2vv"              : "Bs2JpsiPhiAngleCalculator",
	"daughters_to_constrain" : [["J/psi(1S)", "phi(1020)"]],
	"BTA_name"          : "Bs2JpsiPhi_TBA",
	"selOneBCand"       :  SingleBCandidateSelection_BsJpsiphi("BsJpsiphi"),
	"selOneBCand_name"  : "BsJpsiphi",
}
bs2jpsiphi_detached = Decay()
bs2jpsiphi_detached.__dict__ = bs2jpsiphi_detached_dict

bd2jpsikstar_detached_dict = {
	"tuple_name"          : "Bd2JpsiKstarDetached",
	"decay_descriptor"    : "[B0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(K*(892)0 -> ^K+ ^pi-)]CC",
	"inputs"              : prefix + "/Phys/BetaSBd2JpsiKstarDetachedLine/Particles",
	"p2vv"                : "Bd2KstarMuMuAngleCalculator",
	"daughters_to_constrain" : [["J/psi(1S)"]],
	"BTA_name"            : "Bd2JpsiKstar_TBA",
	"selOneBCand"         :  SingleBCandidateSelection_BdJpsiKst("BdJpsiKst"),
	"selOneBCand_name"    : "BdJpsiKst",
}
bd2jpsikstar_detached = Decay()
bd2jpsikstar_detached.__dict__ = bd2jpsikstar_detached_dict

bu2jpsik_detached_dict = {
	"tuple_name"          : "Bu2JpsiKDetached",
	"decay_descriptor"    : "[B+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^K+]CC",
	"inputs"              : prefix + "/Phys/BetaSBu2JpsiKDetachedLine/Particles",
	"daughters_to_constrain" : [["J/psi(1S)"]],
	"BTA_name"            : "Bu2JpsiK_TBA",
	"selOneBCand"         : SingleBCandidateSelection_BuJpsiK("BuJpsiK"),
	"selOneBCand_name"    : "BuJpsiK",
}
bu2jpsik_detached = Decay()
bu2jpsik_detached.__dict__ = bu2jpsik_detached_dict

##############################################################################################
##### Update of _TRACK_GhostProb for downstream tracks
##############################################################################################
from STTools import STOfflineConf
STOfflineConf.DefaultConf().configureTools()
from Configurables import RefitParticleTracks
refitter = RefitParticleTracks()
refitter.DoFit = True
# shouldnt make a difference. either confirm that, or leave it as it is
refitter.DoProbability = True
refitter.UpdateProbability = True
refitter.ReplaceTracks = True
# shouldnt make a difference. either confirm that, or leave it as it is

## I never fully convinced myself whether the following three lines are
## necessary or not. Either way theyre not wrong:
from Configurables import TrackInitFit, TrackMasterFitter
from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter

refitter.addTool(TrackInitFit,"TrackInitFit")
refitter.TrackInitFit.addTool(TrackMasterFitter,"Fit")
ConfiguredMasterFitter(refitter.TrackInitFit.Fit)

##############################################################################################
##### TOOLS AND TRIGGERS AND STRIPPING LINES AND LOKI VARIABLES
##############################################################################################
tuple_tools = ["TupleToolKinematic",
	       "TupleToolPropertime",
	       "TupleToolPrimaries",
	       #"TupleToolIsoGeneric",
	       "TupleToolPid"]

tuple_tools_mc = ["TupleToolMCTruth",
		  "TupleToolMCBackgroundInfo",
		  "TupleToolMCDecayTree"]

loki_variables = {"LOKI_ENERGY"     : "E",
		  "LOKI_ETA"        : "ETA",
		  "LOKI_PHI"        : "PHI"}

loki_variables_B = {"LOKI_FDCHI2"          : "BPVVDCHI2",
		    "LOKI_FDS"             : "BPVDLS",
		    "LOKI_DIRA"            : "BPVDIRA",
		    ###### DecayTreeFitVariables
		    "LOKI_DTF_CTAU"                   : "DTF_CTAU( 0, True )",
		    "LOKI_DTF_CTAU_NOPV"              : "DTF_CTAU( 0, False )",
		    "LOKI_DTF_CTAUS"                  : "DTF_CTAUSIGNIFICANCE( 0, True )",
		    "LOKI_DTF_CHI2NDOF"               : "DTF_CHI2NDOF( True )",
		    "LOKI_DTF_CTAUERR"                : "DTF_CTAUERR( 0, True )",
		    "LOKI_DTF_CTAUERR_NOPV"           : "DTF_CTAUERR( 0, False )",
		    "LOKI_MASS_JpsiConstr"            : "DTF_FUN ( M , True , 'J/psi(1S)' )" ,
		    "LOKI_MASS_JpsiConstr_NoPVConstr" : "DTF_FUN ( M , False , 'J/psi(1S)' )" ,
		    "LOKI_MASSERR_JpsiConstr"         : "sqrt(DTF_FUN ( M2ERR2 , True , 'J/psi(1S)' ))" ,
		    "LOKI_DTF_VCHI2NDOF"              : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )",
		    "LOKI_DTF_CTAU_D1"                : "DTF_CTAU(1, True)",
		    "LOKI_DTF_CTAU_D2"                : "DTF_CTAU(2, True)",
		    "LOKI_DTF_CTAUERR_D1"             : "DTF_CTAUERR(1, True)",
		    "LOKI_DTF_CTAUERR_D2"             : "DTF_CTAUERR(2, True)",
		    "LOKI_DOCA_1_2"                   : "DOCA(1,2)"
		    }

stripping_lines = ['StrippingBetaSBs2JpsiPhiDetachedLineDecision'
		   ,'StrippingBetaSBd2JpsiKstarDetachedLineDecision'
		   ,'StrippingBetaSBu2JpsiKDetachedLineDecision']

l0_lines = ['L0PhysicsDecision'
	    ,'L0MuonDecision'
	    ,'L0DiMuonDecision'
	    ,'L0MuonHighDecision'
	    ,'L0HadronDecision'
	    ,'L0ElectronDecision'
	    ,'L0PhotonDecision'
	    ,'L0MuonNoSPDDecision']

hlt1_lines = ['Hlt1DiMuonHighMassDecision'
	      ,'Hlt1DiMuonLowMassDecision'
	      ,'Hlt1SingleMuonNoIPDecision'
	      ,'Hlt1SingleMuonHighPTDecision'
	      ,'Hlt1SingleElectronNoIPDecision'
	      ,'Hlt1TrackAllL0Decision'
	      ,'Hlt1TrackAllL0TightDecision'
	      ,'Hlt1TrackMuonDecision'
	      ,'Hlt1TrackPhotonDecision'
	      ,'Hlt1TrackForwardPassThroughDecision'
	      ,'Hlt1TrackForwardPassThroughLooseDecision'
	      ,'Hlt1LumiDecision'
	      ,'Hlt1LumiMidBeamCrossingDecision'
	      ,'Hlt1MBNoBiasDecision'
	      ,'Hlt1CharmCalibrationNoBiasDecision'
	      ,'Hlt1MBMicroBiasVeloDecision'
	      ,'Hlt1MBMicroBiasTStationDecision'
	      ,'Hlt1L0AnyDecision'
	      ,'Hlt1L0AnyNoSPDDecision'
	      ,'Hlt1L0HighSumETJetDecision'
	      ,'Hlt1NoPVPassThroughDecision'
	      ,'Hlt1DiProtonDecision'
	      ,'Hlt1DiProtonLowMultDecision'
	      ,'Hlt1BeamGasNoBeamBeam1Decision'
	      ,'Hlt1BeamGasNoBeamBeam2Decision'
	      ,'Hlt1BeamGasBeam1Decision'
	      ,'Hlt1BeamGasBeam2Decision'
	      ,'Hlt1BeamGasCrossingEnhancedBeam1Decision'
	      ,'Hlt1BeamGasCrossingEnhancedBeam2Decision'
	      ,'Hlt1BeamGasCrossingForcedRecoDecision'
	      ,'Hlt1BeamGasCrossingForcedRecoFullZDecision'
	      ,'Hlt1BeamGasHighRhoVerticesDecision'
	      ,'Hlt1ODINTechnicalDecision'
	      ,'Hlt1Tell1ErrorDecision'
	      ,'Hlt1VeloClosingMicroBiasDecision'
	      ,'Hlt1VertexDisplVertexDecision'
	      ,'Hlt1BeamGasCrossingParasiticDecision'
	      ,'Hlt1ErrorEventDecision'
	      ,'Hlt1GlobalDecision'
	      ,'Hlt1TrackMuonNoSPDDecision'
	      ,'Hlt2SingleMuonNoSPDDecision'
	      ,'Hlt1TrackMVADecision'
	      ,'Hlt1TwoTrackMVADecision']



hlt2_lines = ['Hlt2SingleElectronTFLowPtDecision'
	      ,'Hlt2SingleElectronTFHighPtDecision'
	      ,'Hlt2DiElectronHighMassDecision'
	      ,'Hlt2DiElectronBDecision'
	      ,'Hlt2B2HHLTUnbiasedDecision'
	      ,'Hlt2Topo2BodySimpleDecision'
	      ,'Hlt2Topo3BodySimpleDecision'
	      ,'Hlt2Topo4BodySimpleDecision'
	      ,'Hlt2Topo2BodyBBDTDecision'
	      ,'Hlt2Topo3BodyBBDTDecision'
	      ,'Hlt2Topo4BodyBBDTDecision'
	      ,'Hlt2TopoMu2BodyBBDTDecision'
	      ,'Hlt2TopoMu3BodyBBDTDecision'
	      ,'Hlt2TopoMu4BodyBBDTDecision'
	      ,'Hlt2TopoE2BodyBBDTDecision'
	      ,'Hlt2TopoE3BodyBBDTDecision'
	      ,'Hlt2TopoE4BodyBBDTDecision'
	      ,'Hlt2IncPhiDecision'
	      ,'Hlt2IncPhiSidebandsDecision'
	      ,'Hlt2MuonFromHLT1Decision'
	      ,'Hlt2SingleMuonDecision'
	      ,'Hlt2SingleMuonHighPTDecision'
	      ,'Hlt2SingleMuonLowPTDecision'
	      ,'Hlt2DiMuonDecision'
	      ,'Hlt2DiMuonLowMassDecision'
	      ,'Hlt2DiMuonJPsiDecision'
	      ,'Hlt2DiMuonJPsiHighPTDecision'
	      ,'Hlt2DiMuonPsi2SDecision'
	      ,'Hlt2DiMuonBDecision'
	      ,'Hlt2DiMuonZDecision'
	      ,'Hlt2DiMuonDY1Decision'
	      ,'Hlt2DiMuonDY2Decision'
	      ,'Hlt2DiMuonDY3Decision'
	      ,'Hlt2DiMuonDY4Decision'
	      ,'Hlt2DiMuonDetachedDecision'
	      ,'Hlt2DiMuonDetachedHeavyDecision'
	      ,'Hlt2DiMuonDetachedJPsiDecision'
	      ,'Hlt2DiMuonNoPVDecision'
	      ,'Hlt2TriMuonDetachedDecision'
	      ,'Hlt2TriMuonTauDecision'
	      ,'Hlt2DiMuonDetachedJPsiDecision'
	      ,'Hlt2DiMuonDetachedDecision'
	      ,'Hlt2Topo2BodyDecision'
	      ,'Hlt2Topo3BodyDecision'
	      ,'Hlt2Topo4BodyDecision'
	      ,'Hlt2TopoMu2BodyDecision'
	      ,'Hlt2TopoMu3BodyDecision'
	      ,'Hlt2TopoMu4BodyDecision']

trigger_lines = l0_lines + hlt1_lines + hlt2_lines

#trigger_lines = []
#if DaVinci().DataType == "2012":
#  trigger_lines = trigger_lines
#elif DaVinci().DataType == "2011":
#  trigger_lines = trigger_lines
#elif DaVinci().DataType == "2015":
#   trigger_lines = trigger_lines


def CreateBTA(decay):

  myOutputLevel = 6
  print "-----------------------------------------------------------------------" 
  print "---------------        Making BTA tuples           --------------------" 
  print "Decay : " + decay.decay_descriptor
  print "Tuple name: ", decay.BTA_name

  myselB = decay.selOneBCand
  
  if DaVinci().Simulation == True:
	  myselB.ReFitPVs = True  # problems with MC ---- clarify!!

  myselB.InputLocation =  decay.inputs
  myselB.OutputLevel = myOutputLevel

  BPartVtxlocation = "Phys/"+ decay.selOneBCand_name + "/Particles"
  bcandsel = BPartVtxlocation

  print "Location: ", bcandsel

  tagana = BTaggingAnalysis(decay.BTA_name)
  tagana.OutputLevel = myOutputLevel
  tagana.Inputs = [
	  bcandsel,
	  "Phys/TaggingElectrons",
	  "Phys/TaggingMuons",
	  "Phys/TaggingPions"
	  ]

  tagana.TagOutputLocation =  bcandsel + "/FlavourTags"
  tagana.ChoosePVCriterium = "bestPV"  # to be = to the FT
  tagana.PVReFit = True
  tagana.BHypoCriterium = "MinChi2"
  tagana.RequireTisTos = True

  if DaVinci().Simulation == True:
	  tagana.EnableMC = True
	  tagana.addTool( BDecayTool )
	  tagana.BDecayTool.addTool( MCDecayFinder )
	  tagana.BDecayTool.MCDecayFinder.Decay = decay.decay_descriptor
  else:
	  tagana.EnableMC = False

  #tagana.addTool( TriggerTisTos )
  #tagana.TriggerTisTos.OutputLevel = myOutputLevel
  #tagana.addTool( TaggingUtilsChecker )
  #tagana.TaggingUtilsChecker.OutputLevel = myOutputLevel

  tagana.addTool(BTaggingTool)
  tagana.BTaggingTool.EnableCharmTagger = True
#tagana.BTaggingTool.EnableKaonOSTagger = False
  tagana.BTaggingTool.EnableNNetKaonOSTagger = False
  tagana.BTaggingTool.VetoFailedRefits = True
  tagana.BTaggingTool.OutputLevel = myOutputLevel

  from FlavourTagging.Tunings import TuneTool
  if DaVinci().Simulation == True:
	  TuneTool(tagana,"Reco14_MC12", "BTaggingTool" )  # sets the Reco14_MC12 tuning (default is Reco14_2012)
#  else:
#	  TuneTool(tagana,"Reco14_2012", "BTaggingTool" )  # default is Reco14_2012

  from Configurables import TrackNNGhostId
  tagana.addTool ( TrackNNGhostId )
  tagana.TrackNNGhostId.OutputLevel = myOutputLevel
  return [myselB , tagana ] 


def CreateTupleTool(decay):
  print "-----------------------------------------------------------------------" 
  print "---------------        Making B tuples             --------------------" 
  print "Adding decay " + decay.decay_descriptor
  print "Tuple name:", decay.tuple_name

  tuple = DecayTreeTuple(decay.tuple_name)
  
  tuple.OutputLevel = 6
  myselB = decay.selOneBCand
  
  if DaVinci().Simulation == True:
	  myselB.ReFitPVs = True  # problems with MC ---- clarify!!
	  
  myselB.InputLocation = decay.inputs
  BPartVtxlocation = "Phys/" + decay.selOneBCand_name  + "/Particles"
  #BPartVtxlocation = "Phys/SingleBCandidateSelection_BuJpsiK" #/Particles"
  #BPartVtxlocation = "Phys/BuJpsiK/Particles"
  
  bcandsel = [BPartVtxlocation]

  tuple.Inputs = bcandsel
  
  print "Location:", bcandsel
  
  tuple.Decay = decay.decay_descriptor
  tuple.ReFitPVs = True

  tools = copy.copy(tuple_tools)

  if DaVinci().Simulation == True:
	  tt_mct = tuple.addTupleTool("TupleToolMCTruth")
	  tt_mct.ToolList += ["MCTupleToolKinematic", "MCTupleToolHierarchy"]
	  tools.extend(tuple_tools_mc)

  tuple.ToolList = tools

  print tuple.ToolList
  addCharmTuple(tuple)
  addParticleTaggerTuple(tuple)
    
  ## specific tuple tools
  tt_stripping = tuple.addTupleTool("TupleToolStripping")
  tt_stripping.Verbose = True
  tt_stripping.StrippingList = stripping_lines

  tt_tagging = None
  if DaVinci().Simulation == True:
	  tt_tagging = tuple.addTupleTool("TupleToolTaggingMC")
  else:
	  tt_tagging = tuple.addTupleTool("TupleToolTagging")
  tt_tagging.Verbose = True
  tt_tagging.StoreTaggersInfo = True

  if DaVinci().Simulation == True:
	  tt_tagging.TaggingToolName = "BTaggingTool/BsTaggingTool"
	  tt_tagging.addTool(BTaggingTool,name="BsTaggingTool")
	  tt_tagging.Verbose = True
	  from FlavourTagging.Tunings import TuneTool
	  myTuneTool = TuneTool(tt_tagging,"Reco14_MC12","BsTaggingTool")  # sets the Reco14_MC12 tuning (default is Reco14_2012)

  #tt_trigger = tuple.addTupleTool("TupleToolTrigger")
  #tt_trigger.Verbose = True
  #tt_trigger.TriggerList = trigger_lines

  tt_eventinfo = tuple.addTupleTool("TupleToolEventInfo")
  tt_eventinfo.Verbose = True

  tt_loki_general = tuple.addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_general")
  tt_loki_general.Variables = loki_variables

  tt_trackpos = tuple.addTupleTool("TupleToolTrackPosition")
  tt_trackpos.Z = 7500.

  tt_recostats = tuple.addTupleTool("TupleToolRecoStats")
  tt_recostats.Verbose = True

  tt_geometry = tuple.addTupleTool("TupleToolGeometry")
  tt_geometry.Verbose = True
  tt_geometry.RefitPVs = True
  tt_geometry.FillMultiPV = True

  tt_trackinfo = tuple.addTupleTool("TupleToolTrackInfo")
  tt_trackinfo.Verbose = True

  name_cc_particle_decaydec = "J/psi(1S)"
  name_cc_particle_branch   = "J_psi_1S"

  branches = {}
  descriptor_B = decay.decay_descriptor.replace("^","")
  descriptor_cc = descriptor_B.replace("(" + name_cc_particle_decaydec, "^(" + name_cc_particle_decaydec)

  name_bbranch = "B"
  branches[name_bbranch] = "^(" + descriptor_B + ")"
  if "J/psi(1S)" in decay.decay_descriptor:
	  branches[name_cc_particle_branch] = descriptor_cc

  print descriptor_B

  if "add_branches" in decay.__dict__:
	  for name_branch, value in decay.add_branches.iteritems():
		  branches[name_branch] = descriptor_B.replace("(" + value[0], "^(" + value[0])
		  tuple.addTupleTool(TupleToolDecay, name=name_branch)
		  branch_infos = value[1]
		  if len(branch_infos) > 0:
			  tt_loki_br = tuple.__getattr__(name_branch).addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_"+name_branch)
			  tt_loki_br.Variables = branch_infos
		  else:
			  print "For branch " + name_branch + " no other LOKI variables specified."

  print "Decay branches:", branches
  tuple.addBranches(branches)
  tt_loki_B = tuple.__getattr__(name_bbranch).addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_B")
  if "loki_variables_B" in decay.__dict__:
	  tt_loki_B.Variables = dict(loki_variables_B.items() + decay.loki_variables_B.items())
  else:
	  tt_loki_B.Variables = loki_variables_B

  tuple.addTupleTool(TupleToolDecay, name=name_bbranch)
  tt_tistos_b = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolTISTOS")
  tt_tistos_b.Verbose = True
  tt_tistos_b.TriggerList = trigger_lines

  if "J/psi(1S)" in decay.decay_descriptor:
	  tuple.addTupleTool(TupleToolDecay, name="J_psi_1S")
	  tt_tistos_jpsi = tuple.__getattr__("J_psi_1S" ).addTupleTool("TupleToolTISTOS")
	  tt_tistos_jpsi.Verbose = True
	  tt_tistos_jpsi.TriggerList = trigger_lines

  ## DecayTreeFitter variables
  if dtf:
	  # without any constraints
	  FitwithoutConst = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolDecayTreeFitter/FitwithoutConst")
	  FitwithoutConst.Verbose = True
	  FitwithoutConst.UpdateDaughters = True
	  FitwithoutConst.daughtersToConstrain = []
	  FitwithoutConst.constrainToOriginVertex = False

	  # constraint only on PV (not on daughters)
	  FitPVConst = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolDecayTreeFitter/FitPVConst")
	  FitPVConst.Verbose = True
	  FitPVConst.UpdateDaughters = True
	  FitPVConst.daughtersToConstrain = []
	  FitPVConst.constrainToOriginVertex = True
	  
          # constraint only on daughters (not on PV)
	  i = 0
	  for daughters in decay.daughters_to_constrain:
		  fit_name = 'FitDaughtersConst'
		  if i>0:
			  fit_name = fit_name + str(i)
		  FitDaughtersConst = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolDecayTreeFitter/" + fit_name)
		  FitDaughtersConst.Verbose = True
		  FitDaughtersConst.UpdateDaughters = True
		  FitDaughtersConst.daughtersToConstrain = daughters
		  FitDaughtersConst.constrainToOriginVertex = False
		  i += 1

       	  # constraint on daughters and on the origin PV
	  i = 0
	  for daughters in decay.daughters_to_constrain:
		  fit_name = 'FitDaughtersPVConst'
		  if i>0:
			  fit_name = fit_name + str(i)
		  FitDaughtersPVConst = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolDecayTreeFitter/" + fit_name)
		  FitDaughtersPVConst.Verbose = True
		  FitDaughtersPVConst.UpdateDaughters = True
		  FitDaughtersPVConst.daughtersToConstrain = daughters
		  FitDaughtersPVConst.constrainToOriginVertex = True
		  i += 1

  for input in bcandsel:
	  print "Trying to add " + input + " to refitter"
	  if input not in refitter.Inputs:
		  print "Adding " + input + " to refitter"
		  refitter.Inputs.append(input)
  print "All refitter inputs: " + str(refitter.Inputs)
  print ""
  return tuple


TupleSeq.Members += [  CreateBTA(bs2jpsiphi_detached)[0]  , CreateTupleTool(bs2jpsiphi_detached)  , CreateBTA(bs2jpsiphi_detached)[1]  ]
TupleSeq.Members += [  CreateBTA(bd2jpsikstar_detached)[0], CreateTupleTool(bd2jpsikstar_detached), CreateBTA(bd2jpsikstar_detached)[1]]
TupleSeq.Members += [  CreateBTA(bu2jpsik_detached)[0]    , CreateTupleTool(bu2jpsik_detached)    , CreateBTA(bu2jpsik_detached)[1]    ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
# check PVs
checkPV = CheckPV("CheckForOnePV")
checkPV.MinPVs = 1
print checkPV

##############################################################################

seqB2JpsiX.Members      += [checkPV, TupleSeq ]
seqB2JpsiX.ModeOR        = True
seqB2JpsiX.ShortCircuit  = False

evtTuple                 = EventTuple()
evtTuple.ToolList       += ["TupleToolEventInfo"]

from Configurables import GaudiSequencer
subseq_annpid = GaudiSequencer('SeqANNPID')

from Configurables import ChargedProtoANNPIDConf
conf_ann_pid = ChargedProtoANNPIDConf('ANNPIDConf')
conf_ann_pid.RecoSequencer = subseq_annpid

if DaVinci().Simulation == True:
	DaVinci().UserAlgorithms += [mctuple]
	if restrip_mc:
		DaVinci().UserAlgorithms += [sc.sequence()]

if update_track_ghostprob:
	DaVinci().UserAlgorithms += [subseq_annpid]
	DaVinci().UserAlgorithms += [refitter]

DaVinci().UserAlgorithms += [seqB2JpsiX ]
DaVinci().UserAlgorithms += [evtTuple]

CondDB().UseLatestTags = ["2012"] 

