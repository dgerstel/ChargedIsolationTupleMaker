# Basem khanji,basem.khanji@cern.ch: ganga script to submit tagging tuples for RunI, RunII data (both DIMUON & BHADRON streams) in one go !
#
import os , re
#os.getcwd()
#sys.path.append(os.getcwd())
debug = True
year = '16'
years        = [ '12'   ,'16' ]
sim_v        = [ '9a'   ,'9b' ]
strip_v      = [ 'Stripping21Filtered'   ,'Stripping26NoPrescalingFlagged' ]
nu           = [ '2.5'  ,'1.6-25ns' ]
energy       = [ '4000' ,'6500' ]
#
#polarity     = [ 'Up' ]
#polarity     = [ 'Up' ]
#polarity     = [ 'Down' ]
polarity     = [ 'Up', 'Down' ]
reco_v       = [ '14c'  ,'16/Turbo03' ]
tck          = [ 'Trig0x409f0045','Trig0x6138160F' ]
streams      = [ 'B0S2DSSTH.STRIP', 'ALLSTREAMS' ]

# make the paths list
job_setting   = { }
List_Of_Paths = []
for pol in polarity:
    for i in range(len(streams)):
        if years[i]==year:
            PATH_name = 'MC/20%s/Beam%sGeV-20%s-Mag%s-Nu%s-Pythia8/Sim0%s/%s/Reco%s/%s/13264021/%s.DST'%(years[i],energy[i],years[i],pol,nu[i],sim_v[i],tck[i],reco_v[i],strip_v[i],streams[i])
            job_name =  '20' +years[i] + '_Reco' + reco_v[i] + '_' + strip_v[i] + '_' + pol + '_' + streams[i]
            job_setting[job_name] = PATH_name 
            List_Of_Paths.append( PATH_name )

if debug:
    for path in List_Of_Paths:
        print path

print '========================================'
print 'Filled the list of PATHS for ganga jobs'
print '========================================'
print job_setting            


for job_name , path_dict in job_setting.items():
    print '======================================'   
    print 'Sumbitting a new job ...'
    print path_dict , ','  , job_name
    bk_query = BKQuery( path  =  path_dict )
    dataset = bk_query.getDataset()
    myApp = GaudiExec()
    myApp.directory = "$HOME/DaVinciDev_v42r1"
    myApp.options = [ './Bs2DspiMC_ALTR.py' ]
    
    job=Job(name = job_name )
    Year = bool('2011' in job_name)*' "2011" '+bool('2012' in job_name)*' "2012" '+ bool('2015' in job_name)*' "2015"  ' + bool('2016' in job_name)*' "2016"  '
    job.application= myApp
    job.inputfiles=['./ParticleTagger.py' ]
    #job.inputfiles=['./CharmTagger.py'  , './ParticleTagger.py' ]
    job.application.extraOpts  =     (
        'from Configurables import DaVinci                     ; ' +
        'DaVinci().TupleFile     = "DTT_B2Dpi_MC_20%s.root"    ; '%year +
        'DaVinci().EvtMax        =              -1             ; ' +
        'from Configurables import CondDB                      ; ' + 
        'CondDB().UseLatestTags  =    ['+ Year +']             ; ' +
        'DaVinci().DataType      =     '+ Year +'              ; '
        )

    print "Create job for the jobs: ", job.name
    job.inputdata  = dataset 
    job.outputfiles= [LocalFile(namePattern='*.root')] #['DTT_B02DstD_data.root']
    #job.outputfiles= [DiracFile(namePattern='*.root',locations=['CERN-USER']) ] # keep my Tuples on grid element (retrive manually) 
    print "Calls splitter"
    job.splitter = SplitByFiles(filesPerJob = 8, maxFiles = -1 , ignoremissing = True)
    # for LSF :
    print "Finally submit the job"
    job.backend    = Dirac()
    job.submit()
    # queues.add(job.submit)
    print '======================================'   
    print "job: ", job.name + " submitted" 
    print '======================================'   
print " Jobs submitted .... bye "

