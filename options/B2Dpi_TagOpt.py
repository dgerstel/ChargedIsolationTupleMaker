"""
 Script to create a DecayTreeTuple for Bd2Dpi, Bs2Dspi and Bu2Dpi analyses:
"""
__author__ = "Vincenzo Battista <vincenzo.battista@cern.ch>"

##############################################################################################
##### SETTINGS
##############################################################################################

from Configurables import DaVinci
                           
DaVinci().EvtMax = -1
DaVinci().SkipEvents = 0
DaVinci().PrintFreq = 20000
DaVinci().DataType = "2016"
DaVinci().Simulation   = False
#DaVinci().VerboseMessages = True
DaVinci().TupleFile = "DTT_B2Dpi_TagOpt_"+str( DaVinci().DataType )+".root"

from Configurables import CondDB

if DaVinci().Simulation == False and DaVinci().DataType == "2011":
  CondDB( LatestGlobalTagByDataType = "2011" )

if DaVinci().Simulation == False and DaVinci().DataType == "2012":
  CondDB( LatestGlobalTagByDataType = "2012" )

if DaVinci().Simulation == False and DaVinci().DataType == "2015":
  CondDB( LatestGlobalTagByDataType = "2015" )

if DaVinci().Simulation == False and DaVinci().DataType == "2016":
  CondDB( LatestGlobalTagByDataType = "2016" )

## do the restripping (only applicable to simulation)
restrip_mc = False

## update _TRACK_GhostProb (relevant for downstream tracks)
update_track_ghostprob = False

preselection = True

## use the DecayTreeFitter
dtf = False 
import os , sys
os.getcwd()
sys.path.append(os.getcwd())

##############################################################################################
##### GLOBALS
##############################################################################################
prefix = ''
if DaVinci().Simulation == True:
  prefix = 'AllStreams'
else:
  prefix = 'BhadronCompleteEvent'
  
print("prefix is " + prefix)
##############################################################################################
##### IMPORTS
##############################################################################################

import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
import copy
from Configurables import(BTagging,
                          BTaggingTool,
                          CheckPV,
                          CombineParticles,
                          DaVinci,
                          TESCheck,
                          FilterDesktop,
                          FitDecayTrees,
                          GaudiSequencer,
                          OfflineVertexFitter,
                          PrintDecayTree,
                          PrintDecayTreeTool,
                          LoKi__Hybrid__TupleTool,
                          TupleToolDecay,
                          TupleToolP2VV,
                          TupleToolRecoStats,
                          TupleToolTagging,
                          TupleToolTISTOS,
                          TupleToolStripping,
                          TupleToolTrackPosition,
                          TupleToolTrigger,
                          TupleToolEventInfo,
                          TupleToolVtxIsoln,
                          TupleToolGeometry,
                          TupleToolTrackInfo,
                          TupleToolPid,
                          TupleToolMassHypo,
                          EventTuple,
                          TupleToolMCTruth,
                          MCTupleToolPrimaries,
                          MCTupleToolHierarchy,
                          MCTupleToolKinematic,
                          SubstitutePID)

from Configurables import (BTaggingAnalysis,
                           TriggerTisTos,
                           TaggingUtils,
                           TaggingUtilsChecker,
                           BDecayTool,
                           MCDecayFinder)

from Configurables import (NNetTool_MLP,
                           TaggerMuonTool,
                           TaggerElectronTool,
                           TaggerKaonOppositeTool,
                           TaggerVertexChargeTool,
                           TaggerCharmTool)

from Configurables import TrackSmearState as SMEAR
from Configurables import TrackScaleState

import re

from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand

if DaVinci().Simulation == True:
  from Configurables import (TupleToolMCDecayTree, TupleToolTaggingMC)

class Decay(object):
  def __init__(self, other=None):
    if other != None:
      self.__dict__ = other.__dict__.copy()

##############################################################################################
##### SELECTIONS
##############################################################################################

inputB0 = prefix + "/Phys/B02DPiD2HHHBeauty2CharmLine/Particles"
inputBu = prefix + "/Phys/B2D0PiD2HHBeauty2CharmLine/Particles"

def MakeDs(input):

    from PhysSelPython.Wrappers import Selection
    from PhysSelPython.Wrappers import SelectionSequence
    from PhysSelPython.Wrappers import DataOnDemand
    
    strippingSels = input
    
    subPID = SubstitutePID(
      'MakeDs',
      Code = "DECTREE('[[B0]CC -> (D- -> pi- K- K+) pi+]CC')",
      Substitutions = {
      'Beauty -> ^K+ Charm' : 'pi+',
      'Beauty -> ^K- Charm' : 'pi-',
      'Beauty -> Meson ^D+' : 'D_s+',
      'Beauty -> Meson ^D-' : 'D_s-'
      }
      )
    
    selSub = Selection(
      'D_to_Ds_Sel',
      Algorithm=subPID,
      RequiredSelections=strippingSels
      )
    
    selSeq = SelectionSequence('SelSeq', TopSelection=selSub)
    
    return selSeq

seqB2OC          = GaudiSequencer('seqB2OC')
TupleSeq         = GaudiSequencer('TupleSeq')
dsSeq            = MakeDs( [DataOnDemand( Location = inputB0 )] )

if preselection:

  import GaudiConfUtils.ConfigurableGenerators as ConfigurableGenerators
  from Configurables import CombineParticles

  inputB0Particles = DataOnDemand( Location = inputB0 )
  inputBuParticles = DataOnDemand( Location = inputBu )
  inputBsParticles = DataOnDemand( Location = dsSeq.outputLocation() )

  #B meson cuts
  bCuts = "( (BPVDIRA>0.9999) & (BPVIPCHI2()<16) & (BPVLTIME('PropertimeFitter/properTime:PUBLIC')>0.2*ps) & (MIPCHI2DV(PRIMARY)<15.0) )"

  #Bachelor cuts
  bacCuts = "( INGENERATION( (ABSID=='pi+') & (P>2000*MeV) & (PT>400*MeV) & (BPVIPCHI2()>36) & (TRCHI2DOF<2.0),1 ) )"

  #Pi's from D cuts
  piFromDCuts = "( INGENERATION( (ABSID=='pi+') & (TRCHI2DOF<2.5) & (BPVIPCHI2()>9) & (PIDK<8.0),2 ) )"

  #K's from D 
  kFromDCuts = "( INGENERATION( (ABSID=='K+') & (TRCHI2DOF<2.5) & (BPVIPCHI2()>9) & (PIDK>-2.0),2 ) )"

  #D- cuts
  dmCuts = "( INTREE( (ABSID=='D-') & (M>1834*MeV) & (M<1904*MeV) & (BPVIPCHI2()>4) & (BPVVDCHI2>2)) )"

  #D0 cuts
  d0Cuts = "( INTREE( (ABSID=='D0') & (M>1829*MeV) & (M<1899*MeV) & (BPVIPCHI2()>4) & (BPVVDCHI2>2)) )"

  #Ds cuts
  dsCuts = "( INTREE( (ABSID=='D_s-') & (M>1933*MeV) & (M<2003*MeV) & (BPVIPCHI2()>4) & (BPVVDCHI2>2)) )"

  #HLT1 trigger cuts
  hlt1Cuts = "( (TOS('Hlt1TrackAllL0Decision','Hlt1TriggerTisTos')) | (TOS('Hlt1TrackMVADecision','Hlt1TriggerTisTos')) | (TOS('Hlt1TwoTrackMVADecision','Hlt1TriggerTisTos')) )"

  #HLT2 trigger cuts
  hlt2Cuts = "( (TOS('Hlt2Topo2BodyBBDTDecision','Hlt2TriggerTisTos')) | (TOS('Hlt2Topo3BodyBBDTDecision','Hlt2TriggerTisTos')) | (TOS('Hlt2Topo4BodyBBDTDecision','Hlt2TriggerTisTos')) | (TOS('Hlt2Topo2BodyDecision','Hlt2TriggerTisTos')) | (TOS('Hlt2Topo3BodyDecision','Hlt2TriggerTisTos')) | (TOS('Hlt2Topo4BodyDecision','Hlt2TriggerTisTos')) )"

  ##Global Bs->DsPi cut
  cutsBs = "( "+bCuts+" & "+bacCuts+" & "+piFromDCuts+" & "+kFromDCuts+" & "+dsCuts+" & "+hlt1Cuts+" & "+hlt2Cuts+" )"

  ##Global Bd->DPi cut
  cutsBd = "( "+bCuts+" & "+bacCuts+" & "+piFromDCuts+" & "+kFromDCuts+" & "+dmCuts+" & "+hlt1Cuts+" & "+hlt2Cuts+" )"

  ##Global Bu->D0Pi cut
  cutsBu = "( "+bCuts+" & "+bacCuts+" & "+piFromDCuts+" & "+kFromDCuts+" & "+d0Cuts+" & "+hlt1Cuts+" & "+hlt2Cuts+" )"


  #BsFilter = FilterDesktop("BsFilter",
  #                         Code = cutsBs
  #                         )
  #BdFilter = FilterDesktop("BdFilter",
  #                         Code = cutsBd
  #                       )
  BuFilter = FilterDesktop("BuFilter",
                           Code = cutsBu
                           )

  #BsSelection = Selection("BsSelection",
  #                        Algorithm =  BsFilter ,
  #                        RequiredSelections = [ inputBsParticles ] )
  #BdSelection = Selection("BdSelection",
  #                        Algorithm =  BdFilter ,
  #                        RequiredSelections = [ inputB0Particles  ] )
  BuSelection = Selection("BuSelection",
                          Algorithm =  BuFilter ,
                          RequiredSelections = [ inputBuParticles  ] )
  
  #BsSequence = SelectionSequence("BsSequence", TopSelection = BsSelection)
  #BdSequence = SelectionSequence("BdSequence", TopSelection = BdSelection)
  BuSequence = SelectionSequence("BuSequence", TopSelection = BuSelection)
  
  #DaVinci().appendToMainSequence( [dsSeq.sequence(), BsSequence.sequence() , BdSequence.sequence(), BuSequence.sequence()] )
  DaVinci().appendToMainSequence( [BuSequence.sequence()] )
  
##############################################################################################

##############################################################################################
##### DECAYS
##############################################################################################
'''
bs2dspi_detached_dict = {
  "tuple_name"             : "Bs2DspiDetached",
  "decay_descriptor"       : "[[B0]CC -> ^(D_s- -> ^pi- ^K- ^K+) ^pi+]CC",
  "inputs"                 : BsSequence.outputLocation(),
  "daughters_to_constrain" : [["D_s-"]],
  "descriptor_B"           : "^([[B0]CC -> (D_s- -> pi- K- K+) pi+]CC)",
  "descriptor_D"           : "[[B0]CC -> ^(D_s- -> pi- K- K+) pi+]CC",
  "descriptor_Bac"         : "[[B0]CC -> (D_s- -> pi- K- K+) ^pi+]CC",
  "descriptors_Ddaughters" : ["[[B0]CC -> (D_s- -> pi- K- ^K+) pi+]CC",
                              "[[B0]CC -> (D_s- -> pi- ^K- K+) pi+]CC",
                              "[[B0]CC -> (D_s- -> ^pi- K- K+) pi+]CC"],
  "BTA_name"               : "Bs2Dspi_TBA",
  "loki_variables_B"         : {"LOKI_DTF_CTAU"                   : "DTF_CTAU( 0, True )",
                                "LOKI_DTF_CTAUERR"                : "DTF_CTAUERR( 0, True )",
                                "LOKI_MASS_DConstr"               : "DTF_FUN ( M , True , 'D_s-' )"
                                }
}
bs2dspi_detached = Decay()
bs2dspi_detached.__dict__ = bs2dspi_detached_dict

bd2dpi_detached_dict = {
  "tuple_name"             : "Bd2DpiDetached",
  "decay_descriptor"       : "[[B0]CC -> ^(D- -> ^pi- ^pi- ^K+) ^pi+]CC",
  "inputs"                 : BdSequence.outputLocation(),
  "daughters_to_constrain" : [["D-"]],
  "descriptor_B"           : "^([[B0]CC -> (D- -> pi- pi- K+) pi+]CC)",
  "descriptor_D"           : "[[B0]CC -> ^(D- -> pi- pi- K+) pi+]CC",
  "descriptor_Bac"         : "[[B0]CC -> (D- -> pi- pi- K+) ^pi+]CC",
  "descriptors_Ddaughters" : ["[[B0]CC -> (D- -> pi- pi- ^K+) pi+]CC",
                              "[[B0]CC -> (D- -> pi- ^pi- K+) pi+]CC",
                              "[[B0]CC -> (D- -> ^pi- pi- K+) pi+]CC"],
  "BTA_name"               : "Bd2Dpi_TBA",
  "loki_variables_B"         : {"LOKI_DTF_CTAU"                   : "DTF_CTAU( 0, True )",
                                "LOKI_DTF_CTAUERR"                : "DTF_CTAUERR( 0, True )",
                                "LOKI_MASS_DConstr"               : "DTF_FUN ( M , True , 'D-' )"
                                }
  }
bd2dpi_detached = Decay()
bd2dpi_detached.__dict__ = bd2dpi_detached_dict
'''
bu2dpi_detached_dict = {
  "tuple_name"             : "Bu2DpiDetached",
  "decay_descriptor"       : "(B- -> ^(D0 -> ^K- ^pi+) ^pi-) || (B- -> ^(D0 -> ^K+ ^pi-) ^pi-) || (B+ -> ^(D0 -> ^K+ ^pi-) ^pi+) || (B+ -> ^(D0 -> ^K- ^pi+) ^pi+)",
  "inputs"                 : BuSequence.outputLocation(),
  "daughters_to_constrain" : [["D0"]],
  "descriptor_B"           : "^((B- -> (D0 -> K- pi+) pi-) || (B- -> (D0 -> K+ pi-) pi-) || (B+ -> (D0 -> K+ pi-) pi+) || (B+ -> (D0 -> K- pi+) pi+))",
  "descriptor_D"           : "(B- -> ^(D0 -> K- pi+) pi-) || (B- -> ^(D0 -> K+ pi-) pi-) || (B+ -> ^(D0 -> K+ pi-) pi+) || (B+ -> ^(D0 -> K- pi+) pi+)",
  "descriptor_Bac"         : "(B- -> (D0 -> K- pi+) ^pi-) || (B- -> (D0 -> K+ pi-) ^pi-) || (B+ -> (D0 -> K+ pi-) ^pi+) || (B+ -> (D0 -> K- pi+) ^pi+)",
  "descriptors_Ddaughters"  : ["(B- -> (D0 -> ^K- pi+) pi-) || (B- -> (D0 -> ^K+ pi-) pi-) || (B+ -> (D0 -> ^K+ pi-) pi+) || (B+ -> (D0 -> ^K- pi+) pi+)",
                              "(B- -> (D0 -> K- ^pi+) pi-) || (B- -> (D0 -> K+ ^pi-) pi-) || (B+ -> (D0 -> K+ ^pi-) pi+) || (B+ -> (D0 -> K- ^pi+) pi+)"],
  "BTA_name"               : "Bu2Dpi_TBA",
  "loki_variables_B"         : {"LOKI_DTF_CTAU"                   : "DTF_CTAU( 0, True )",
                                "LOKI_MASS_DConstr"               : "DTF_FUN ( M , True , 'D0' )"
                                }
  }
bu2dpi_detached = Decay()
bu2dpi_detached.__dict__ = bu2dpi_detached_dict


##############################################################################################
##### TOOLS AND TRIGGERS AND STRIPPING LINES AND LOKI VARIABLES
##############################################################################################
from Configurables import DaVinci

tuple_tools = ["TupleToolKinematic",
               "TupleToolPropertime",
               "TupleToolPrimaries",
               "TupleToolPid"]

tuple_tools_mc = ["TupleToolMCTruth",
                  "TupleToolMCBackgroundInfo",
                  "TupleToolMCDecayTree"]

loki_variables = {"LOKI_ENERGY"     : "E",
                  "LOKI_ETA"        : "ETA",
                  "LOKI_PHI"        : "PHI"}

stripping_lines = ['StrippingB02DPiD2HHHBeauty2CharmLineDecision'
                   ,'StrippingB2D0PiD2HHBeauty2CharmLineDecision']

trigger_lines = [ "Hlt1TrackAllL0Decision"
                  ,"Hlt1TrackMVADecision"
                  ,"Hlt1TwoTrackMVADecision"
                  ,"Hlt2Topo2BodyDecision"
                  ,"Hlt2Topo3BodyDecision"
                  ,"Hlt2Topo4BodyDecision"
                  ,"Hlt2Topo2BodyBBDTDecision"
                  ,"Hlt2Topo3BodyBBDTDecision"
                  ,"Hlt2Topo4BodyBBDTDecision"]


def CreateTupleTool(decay):

  print "-----------------------------------------------------------------------"
  print "Adding decay " + decay.decay_descriptor
  print "Location:", decay.inputs
  print "Tuple name:", decay.tuple_name

  tuple = DecayTreeTuple(decay.tuple_name)
  tuple.OutputLevel = 6
  tuple.Inputs =  [decay.inputs]
  tuple.Decay = decay.decay_descriptor
  tuple.ReFitPVs = True
  tools = copy.copy(tuple_tools)
  
  if DaVinci().Simulation == True:
    tt_mct = tuple.addTupleTool("TupleToolMCTruth")
    tt_mct.ToolList += ["MCTupleToolKinematic", "MCTupleToolHierarchy"]
    tools.extend(tuple_tools_mc)
  
  tuple.ToolList = tools

  print "Tuple tool list"
  print tuple.ToolList

  tt_stripping = tuple.addTupleTool("TupleToolStripping")
  tt_stripping.Verbose = True
  tt_stripping.StrippingList = stripping_lines
 
  tt_tagging = None
  if DaVinci().Simulation == True:
    tt_tagging = tuple.addTupleTool("TupleToolTaggingMC")
  else:
    tt_tagging = tuple.addTupleTool("TupleToolTagging")
  tt_tagging.Verbose = True
  tt_tagging.AddMVAFeatureInfo = True
  tt_tagging.AddTagPartsInfo = True

  from Configurables import BTaggingTool
  btagtool = tt_tagging.addTool(BTaggingTool, name = "MyBTaggingTool")

  from FlavourTagging.Tunings import applyTuning as applyFTTuning
  applyFTTuning(btagtool, tuning_version="TupleDevelopment")
  tt_tagging.TaggingToolName = btagtool.getFullName()

  if DaVinci().Simulation == True:
    tt_tagging.TaggingToolName = "BTaggingTool/BsTaggingTool"
    tt_tagging.addTool(BTaggingTool,name="BsTaggingTool")
    #tt_tagging.Verbose = True
    from FlavourTagging.Tunings import TuneTool
    myTuneTool = TuneTool(tt_tagging,"Reco14_MC12","BsTaggingTool") 

  tt_trigger = tuple.addTupleTool("TupleToolTrigger")
  tt_trigger.Verbose = True
  tt_trigger.TriggerList = trigger_lines

  tt_eventinfo = tuple.addTupleTool("TupleToolEventInfo")

  tt_loki_general = tuple.addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_general")
  tt_loki_general.Variables = loki_variables

  tt_geometry = tuple.addTupleTool("TupleToolGeometry")
  tt_recostats = tuple.addTupleTool("TupleToolRecoStats")
  tt_trackinfo = tuple.addTupleTool("TupleToolTrackInfo")
  tt_trackinfo.Verbose = True
  
  branches = {}
  name_bbranch = "lab0"
  branches[name_bbranch] = decay.descriptor_B
  '''
  branches["lab2"] = decay.descriptor_D
  branches["lab1"] = decay.descriptor_Bac
  lab=3
  for daughter in decay.descriptors_Ddaughters:
    branches["lab"+str(lab)] = daughter
    lab=lab+1
  '''

  print decay.descriptor_B

  print "Decay branches:", branches
  tuple.addBranches(branches)
  tt_loki_B = tuple.__getattr__(name_bbranch).addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_B")

  if "loki_variables_B" in decay.__dict__:
    tt_loki_B.Variables = decay.loki_variables_B

  tuple.UseLabXSyntax = True
  tuple.RevertToPositiveID = False

  tuple.addTupleTool("TupleToolDecay/lab0")
  tt_tistos_b = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolTISTOS")
  tt_tistos_b.VerboseL0 = True
  tt_tistos_b.VerboseHlt1 = True
  tt_tistos_b.VerboseHlt2 = True
  tt_tistos_b.TriggerList = trigger_lines  

  return tuple

#TupleSeq.Members += [  CreateTupleTool(bs2dspi_detached)   ]
#TupleSeq.Members += [  CreateTupleTool(bd2dpi_detached)    ]
TupleSeq.Members += [  CreateTupleTool(bu2dpi_detached)    ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
checkPV = CheckPV("CheckForOnePV")
checkPV.MinPVs = 1
print checkPV

##############################################################################

seqB2OC.Members      += [checkPV, TupleSeq]
seqB2OC.ModeOR        = True
seqB2OC.ShortCircuit  = False

evtTuple                 = EventTuple()
evtTuple.ToolList       += ["TupleToolEventInfo"]

from Configurables import GaudiSequencer
subseq_annpid = GaudiSequencer('SeqANNPID')

from Configurables import ChargedProtoANNPIDConf
conf_ann_pid = ChargedProtoANNPIDConf('ANNPIDConf')
conf_ann_pid.RecoSequencer = subseq_annpid

if DaVinci().Simulation == True:
  DaVinci().UserAlgorithms += [mctuple]
  if restrip_mc:
    DaVinci().UserAlgorithms += [sc.sequence()]

if DaVinci().Simulation == True:
  smear = SMEAR( 'Smear' )
  DaVinci().UserAlgorithms += [smear]

if DaVinci().Simulation == False:
  scaler = TrackScaleState('scaler')
  DaVinci().UserAlgorithms += [scaler]

if update_track_ghostprob:
  DaVinci().UserAlgorithms += [subseq_annpid]
  DaVinci().UserAlgorithms += [refitter]

DaVinci().UserAlgorithms += [seqB2OC]


from GaudiConf import IOHelper
if DaVinci().DataType == "2012":
  IOHelper().inputFiles([
    'root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/DST/BHADRONCOMPLETEEVENT/2012/00041834_00000016_1.bhadroncompleteevent.dst',
    'root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/DST/BHADRONCOMPLETEEVENT/2012/00041836_00000164_1.bhadroncompleteevent.dst',
    'root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/DST/BHADRONCOMPLETEEVENT/2012/00041836_00000243_1.bhadroncompleteevent.dst',
    'root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/DST/BHADRONCOMPLETEEVENT/2012/00041836_00000271_1.bhadroncompleteevent.dst'
    ], clear=True)
if DaVinci().DataType == "2016":
  IOHelper().inputFiles([
    'root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/DST/BHADRONCOMPLETEEVENT/2016/00053485_00000001_1.bhadroncompleteevent.dst',
    'root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/DST/BHADRONCOMPLETEEVENT/2016/00053485_00000004_1.bhadroncompleteevent.dst',
    'root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/DST/BHADRONCOMPLETEEVENT/2016/00053485_00000006_1.bhadroncompleteevent.dst',
    ], clear=True)
