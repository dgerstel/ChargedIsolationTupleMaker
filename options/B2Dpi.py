"""
 Script to create a DecayTreeTuple for Bd2Dpi, Bs2Dspi and Bu2Dpi analyses:
"""
__author__ = "Ulrich Eitschberger <ulrich.eitschberger@tu-dortmund.de>"

##############################################################################################
##### SETTINGS
##############################################################################################

from Configurables import DaVinci
DaVinci().EvtMax = -1
DaVinci().SkipEvents = 0
DaVinci().PrintFreq = 500
DaVinci().DataType = "2012"
DaVinci().Simulation   = False
DaVinci().Lumi = True
DaVinci().TupleFile = "DTT.root"

## do the restripping (only applicable to simulation)
restrip_mc = False

## use the DecayTreeFitter
dtf = True

## number of events to process
max_events = -1
skip_events = -1

##############################################################################################
##### GLOBALS
##############################################################################################
prefix = ''
#if DaVinci().Simulation == True:
#  prefix = 'AllStreams'
#else:
prefix = 'BhadronCompleteEvent'
  
print("prefix is " + prefix)
##############################################################################################
##### IMPORTS
##############################################################################################

import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
import copy
from Configurables import *

import re

from PhysSelPython.Wrappers import Selection, SelectionSequence

from Configurables import ( SingleBCandidateSelection_BdDpi ,SingleBCandidateSelection_BuDpi
                            )
                           

from Configurables import TrackScaleState
from Configurables import CondDB

class Decay(object):
  def __init__(self, other=None):
    if other != None:
      self.__dict__ = other.__dict__.copy()

##############################################################################################
##### SELECTIONS
##############################################################################################
seqB2OC          = GaudiSequencer('seqB2OC')
TupleSeq         = GaudiSequencer('TupleSeq')

##############################################################################################

##############################################################################################
##### DECAYS
##############################################################################################
#bs2dspi_detached_dict = {
#  "tuple_name"             : "Bs2DspiDetached",
#  "decay_descriptor"       : "[[B0]CC -> ^(D- -> ^K+ ^K- ^pi-) ^pi+]CC",
#  "inputs"                 : [prefix + "/Phys/B02DPiD2HHHBeauty2CharmLine/Particles"],
#  "daughters_to_constrain" : [["D_s-"]],
#  "descriptor_B"           : "^([[B0]CC -> (D- -> K+ K- pi-) pi+]CC)"
#}
#bs2dspi_detached = Decay()
#bs2dspi_detached.__dict__ = bs2dspi_detached_dict

bd2dpi_detached_dict = {
  "tuple_name"             : "Bd2DpiDetached",
  "decay_descriptor"       : "[[B0]CC -> ^(D- -> ^pi- ^pi- ^K+) ^pi+]CC",
  "inputs"                 : prefix + "/Phys/B02DPiD2HHHBeauty2CharmLine/Particles",
  "daughters_to_constrain" : [["D-"]],
  "descriptor_B"           : "^([[B0]CC -> (D- -> pi- pi- K+) pi+]CC)",
  "BTA_name"               : "Bd2Dpi_TBA",
  "selOneBCand"            :  SingleBCandidateSelection_BdDpi("Bd2Dpi"),
  "selOneBCand_name"       : "Bd2Dpi"
  }

bd2dpi_detached = Decay()
bd2dpi_detached.__dict__ = bd2dpi_detached_dict

bu2dpi_detached_dict = {
  "tuple_name"             : "Bu2DpiDetached",
  "decay_descriptor"       : "(B- -> ^(D0 -> ^K- ^pi+) ^pi-) || (B- -> ^(D0 -> ^K+ ^pi-) ^pi-) || (B+ -> ^(D0 -> ^K+ ^pi-) ^pi+) || (B+ -> ^(D0 -> ^K- ^pi+) ^pi+)",
  "inputs"                 : prefix + "/Phys/B2D0PiD2HHBeauty2CharmLine/Particles",
  "daughters_to_constrain" : [["D0"]],
  "descriptor_B"           : "^((B- -> (D0 -> K- pi+) pi-) || (B- -> (D0 -> K+ pi-) pi-) || (B+ -> (D0 -> K+ pi-) pi+) || (B+ -> (D0 -> K- pi+) pi+))",
  "BTA_name"               : "Bu2Dpi_TBA",
  "selOneBCand"            :  SingleBCandidateSelection_BuDpi("Bu2Dpi"),
  "selOneBCand_name"       : "Bu2Dpi"
}
bu2dpi_detached = Decay()
bu2dpi_detached.__dict__ = bu2dpi_detached_dict

###############################################################################################
###### Update of _TRACK_GhostProb for downstream tracks
###############################################################################################
#from STTools import STOfflineConf
#STOfflineConf.DefaultConf().configureTools()
#from Configurables import RefitParticleTracks
#refitter = RefitParticleTracks()
#refitter.DoFit = True
## shouldn't make a difference. either confirm that, or leave it as it is
#refitter.DoProbability = True
#refitter.UpdateProbability = True
#refitter.ReplaceTracks = True
## shouldn't make a difference. either confirm that, or leave it as it is
#
### I never fully convinced myself whether the following three lines are
### necessary or not. Either way they're not wrong:
#from Configurables import TrackInitFit, TrackMasterFitter
#from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter
#
#refitter.addTool(TrackInitFit,"TrackInitFit")
#refitter.TrackInitFit.addTool(TrackMasterFitter,"Fit")
#ConfiguredMasterFitter(refitter.TrackInitFit.Fit)

# prepare tagging :
from CharmTagger import *
from ParticleTagger import *
createVeloTracks()
configIso()
DaVinci().appendToMainSequence( [ AllPions_seq ])
DaVinci().appendToMainSequence( [ AllCharmTaggers_seq ])
##############################################################################################
##### TOOLS AND TRIGGERS AND STRIPPING LINES AND LOKI VARIABLES
##############################################################################################
tuple_tools = ["TupleToolMassHypo",
               "TupleToolTrackPosition",
               "TupleToolPropertime",
               "TupleToolPid",
               "TupleToolAngles",
               "TupleToolPrimaries"]

tuple_tools_mc = ["TupleToolMCTruth",
                  "TupleToolMCBackgroundInfo",
                  "TupleToolMCDecayTree"]

loki_variables = {"LOKI_ENERGY"     : "E",
                  "LOKI_ETA"        : "ETA",
                  "LOKI_PHI"        : "PHI"}

#loki_variables_B = {"LOKI_FDCHI2"          : "BPVVDCHI2",
#                    "LOKI_FDS"             : "BPVDLS",
#                    "LOKI_DIRA"            : "BPVDIRA",
#                    ###### DecayTreeFitVariables
#                    "LOKI_DTF_CTAU"                   : "DTF_CTAU( 0, True )",
#                    "LOKI_DTF_CTAU_NOPV"              : "DTF_CTAU( 0, False )",
#                    "LOKI_DTF_CTAUS"                  : "DTF_CTAUSIGNIFICANCE( 0, True )",
#                    "LOKI_DTF_CHI2NDOF"               : "DTF_CHI2NDOF( True )",
#                    "LOKI_DTF_CTAUERR"                : "DTF_CTAUERR( 0, True )",
#                    "LOKI_DTF_CTAUERR_NOPV"           : "DTF_CTAUERR( 0, False )",
#                    "LOKI_MASS_DConstr"               : "DTF_FUN ( M , True , 'D-' )" ,
#                    "LOKI_MASS_DConstr_NoPVConstr"    : "DTF_FUN ( M , False , 'D-' )" ,
#                    "LOKI_MASSERR_DConstr"            : "sqrt(DTF_FUN ( M2ERR2 , True , 'D-' ))" ,
#                    "LOKI_DTF_VCHI2NDOF"              : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )",
#                    "LOKI_DTF_CTAU_D1"                : "DTF_CTAU(1, True)",
#                    "LOKI_DTF_CTAU_D2"                : "DTF_CTAU(2, True)",
#                    "LOKI_DTF_CTAUERR_D1"             : "DTF_CTAUERR(1, True)",
#                    "LOKI_DTF_CTAUERR_D2"             : "DTF_CTAUERR(2, True)",
#                    "LOKI_DOCA_1_2"                   : "DOCA(1,2)"
#                    }

stripping_lines = ['StrippingB02DPiD2HHHBeauty2CharmLineDecision'
                  ,'StrippingB2D0PiD2HHBeauty2CharmLineDecision']

l0_lines = ['L0PhysicsDecision'
           ,'L0MuonDecision'
           ,'L0DiMuonDecision'
           ,'L0MuonHighDecision'
           ,'L0HadronDecision'
           ,'L0ElectronDecision'
           ,'L0PhotonDecision']

hlt1_lines = ['Hlt1DiMuonHighMassDecision' 
                  ,'Hlt1DiMuonLowMassDecision' 
                  ,'Hlt1SingleMuonNoIPDecision' 
                  ,'Hlt1SingleMuonHighPTDecision' 
                  ,'Hlt1SingleElectronNoIPDecision' 
                  ,'Hlt1TrackAllL0Decision' 
                  ,'Hlt1TrackAllL0TightDecision' 
                  ,'Hlt1TrackMuonDecision' 
                  ,'Hlt1TrackPhotonDecision' 
                  ,'Hlt1TrackForwardPassThroughDecision' 
                  ,'Hlt1TrackForwardPassThroughLooseDecision' 
                  ,'Hlt1LumiDecision' 
                  ,'Hlt1LumiMidBeamCrossingDecision' 
                  ,'Hlt1MBNoBiasDecision' 
                  ,'Hlt1CharmCalibrationNoBiasDecision' 
                  ,'Hlt1MBMicroBiasVeloDecision' 
                  ,'Hlt1MBMicroBiasTStationDecision' 
                  ,'Hlt1L0AnyDecision' 
                  ,'Hlt1L0AnyNoSPDDecision' 
                  ,'Hlt1L0HighSumETJetDecision' 
                  ,'Hlt1NoPVPassThroughDecision' 
                  ,'Hlt1DiProtonDecision' 
                  ,'Hlt1DiProtonLowMultDecision' 
                  ,'Hlt1BeamGasNoBeamBeam1Decision' 
                  ,'Hlt1BeamGasNoBeamBeam2Decision' 
                  ,'Hlt1BeamGasBeam1Decision' 
                  ,'Hlt1BeamGasBeam2Decision' 
                  ,'Hlt1BeamGasCrossingEnhancedBeam1Decision' 
                  ,'Hlt1BeamGasCrossingEnhancedBeam2Decision' 
                  ,'Hlt1BeamGasCrossingForcedRecoDecision' 
                  ,'Hlt1BeamGasCrossingForcedRecoFullZDecision' 
                  ,'Hlt1BeamGasHighRhoVerticesDecision' 
                  ,'Hlt1ODINTechnicalDecision' 
                  ,'Hlt1Tell1ErrorDecision' 
                  ,'Hlt1VeloClosingMicroBiasDecision' 
                  ,'Hlt1VertexDisplVertexDecision' 
                  ,'Hlt1BeamGasCrossingParasiticDecision' 
                  ,'Hlt1ErrorEventDecision' 
                  ,'Hlt1GlobalDecision']

  

hlt2_lines = ["Hlt2SingleElectronTFLowPtDecision"
                  ,"Hlt2SingleElectronTFHighPtDecision"
                  ,"Hlt2DiElectronHighMassDecision"
                  ,"Hlt2DiElectronBDecision"
                  ,"Hlt2B2HHLTUnbiasedDecision"
                  ,"Hlt2Topo2BodyDecision"
                  ,"Hlt2Topo3BodyDecision"
                  ,"Hlt2Topo4BodyDecision"
                  ,"Hlt2TopoMu2BodyDecision"
                  ,"Hlt2TopoMu3BodyDecision"
                  ,"Hlt2TopoMu4BodyDecision"
                  ,"Hlt2TopoE2BodyDecision"
                  ,"Hlt2TopoE3BodyDecision"
                  ,"Hlt2TopoE4BodyDecision"
                  ,"Hlt2IncPhiDecision"
                  ,"Hlt2IncPhiSidebandsDecision"
                  ,"Hlt2MuonFromHLT1Decision"
                  ,"Hlt2SingleMuonDecision"
                  ,"Hlt2SingleMuonHighPTDecision"
                  ,"Hlt2SingleMuonLowPTDecision"
                  ,"Hlt2DiMuonDecision"
                  ,"Hlt2DiMuonLowMassDecision"
                  ,"Hlt2DiMuonJPsiDecision"
                  ,"Hlt2DiMuonJPsiHighPTDecision"
                  ,"Hlt2DiMuonPsi2SDecision"
                  ,"Hlt2DiMuonBDecision"
                  ,"Hlt2DiMuonZDecision"
                  ,"Hlt2DiMuonDY1Decision"
                  ,"Hlt2DiMuonDY2Decision"
                  ,"Hlt2DiMuonDY3Decision"
                  ,"Hlt2DiMuonDY4Decision"
                  ,"Hlt2DiMuonDetachedHeavyDecision"
                  ,"Hlt2DiMuonDetachedJPsiDecision"
                  ,"Hlt2DiMuonNoPVDecision"
                  ,"Hlt2DiMuonDetachedPsi2SDecision"
                  ,"Hlt2TriMuonDetachedDecision"
                  ,"Hlt2TriMuonTauDecision"
                  ,"Hlt2DiMuonDetachedJPsiDecision"
                  ,"Hlt2DiMuonDetachedDecision"
                  ,"Hlt2ExpressBeamHaloDecision"
                  ,"Hlt2DisplVerticesSingleLoosePSDecision"
                  ,"Hlt2DiMuonAndGammaDecision"
                  ,"Hlt2B2HHDecision"]

trigger_lines = l0_lines + hlt1_lines + hlt2_lines

if DaVinci().DataType == "2012":
  trigger_lines = trigger_lines
elif DaVinci().DataType == "2011":
  trigger_lines = trigger_lines
elif DaVinci().DataType == "2015":
  trigger_lines = trigger_lines

def CreateBTA(decay):
  
  myOutputLevel = 6
  print "-----------------------------------------------------------------------"
  print "---------------        Making BTA tuples           --------------------"
  print "Decay : " + decay.decay_descriptor
  print "Tuple name: ", decay.BTA_name
  
  myselB = decay.selOneBCand
  
  
  BPartVtxlocation = "Phys/"+ decay.selOneBCand_name + "/Particles"
  bcandsel = BPartVtxlocation
  
  print "Location: ", bcandsel
  
  tagana = BTaggingAnalysis(decay.BTA_name)
  tagana.OutputLevel = myOutputLevel
  tagana.Inputs = [
    bcandsel,
    "Phys/TaggingElectrons",
    "Phys/TaggingMuons",
    "Phys/TaggingPions"
    ]
  
  tagana.TagOutputLocation =  bcandsel + "/FlavourTags"
  tagana.ChoosePVCriterium = "bestPV"  # to be = to the FT
  tagana.PVReFit = True
  tagana.BHypoCriterium = "MinChi2"
  tagana.RequireTisTos = True
  
  if DaVinci().Simulation == True:
    tagana.EnableMC = True
    tagana.addTool( BDecayTool )
    tagana.BDecayTool.addTool( MCDecayFinder )
    tagana.BDecayTool.MCDecayFinder.Decay = decay.decay_descriptor
  else:
    tagana.EnableMC = False
     
  #tagana.addTool( TriggerTisTos )
  #tagana.TriggerTisTos.OutputLevel = myOutputLevel
  #tagana.addTool( TaggingUtilsChecker )
  #tagana.TaggingUtilsChecker.OutputLevel = myOutputLevel
  tagana.addTool(BTaggingTool)
  tagana.BTaggingTool.EnableCharmTagger = True
  #tagana.BTaggingTool.EnableKaonOSTagger = False
  tagana.BTaggingTool.EnableNNetKaonOSTagger = False
  tagana.BTaggingTool.VetoFailedRefits = True
  tagana.BTaggingTool.OutputLevel = myOutputLevel
  
  from FlavourTagging.Tunings import TuneTool
  #else:
  # TuneTool(tagana,"Reco14_2012", "BTaggingTool" )  # default is Reco14_2012
     
  from Configurables import TrackNNGhostId
  tagana.addTool ( TrackNNGhostId )
  tagana.TrackNNGhostId.OutputLevel = myOutputLevel
  return [myselB , tagana ]

def CreateTupleTool(decay):
  print "-----------------------------------------------------------------------"
  print "Adding decay " + decay.decay_descriptor
  print "Location:", decay.inputs
  print "Tuple name:", decay.tuple_name

  tuple = DecayTreeTuple(decay.tuple_name)
  myselB = decay.selOneBCand
  
  
  myselB.InputLocation = decay.inputs
  BPartVtxlocation = "Phys/" + decay.selOneBCand_name  + "/Particles"
                   
  tuple.Inputs =  [BPartVtxlocation] #decay.inputs
  tuple.Decay = decay.decay_descriptor
  tuple.ReFitPVs = True

  tools = copy.copy(tuple_tools)

  tuple.ToolList = tools

  print tuple.ToolList
 
  ## specific tuple tools
  tt_eventinfo = tuple.addTupleTool("TupleToolEventInfo")
  tt_eventinfo.Verbose = True

  addCharmTuple(tuple)
  addParticleTaggerTuple(tuple)
    
  
  tt_stripping = tuple.addTupleTool("TupleToolStripping")
  tt_stripping.Verbose = True
  tt_stripping.StrippingList = stripping_lines
 
  tt_tagging = tuple.addTupleTool("TupleToolTagging")
  tt_tagging.Verbose = True
  tt_tagging.StoreTaggersInfo = True

  tt_trigger = tuple.addTupleTool("TupleToolTrigger")
  tt_trigger.Verbose = True
  tt_trigger.TriggerList = trigger_lines
 
  tt_kinematic = tuple.addTupleTool("TupleToolKinematic")
  tt_kinematic.Verbose = True

  tt_loki_general = tuple.addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_general")
  tt_loki_general.Variables = loki_variables

  tt_recostats = tuple.addTupleTool("TupleToolRecoStats")
  tt_recostats.Verbose = True

  tt_geometry = tuple.addTupleTool("TupleToolGeometry")
  tt_geometry.Verbose = True
  tt_geometry.RefitPVs = True
  tt_geometry.FillMultiPV = True

  tt_trackinfo = tuple.addTupleTool("TupleToolTrackInfo")
  tt_trackinfo.Verbose = True
  
  branches = {}
  name_bbranch = "lab0"
  branches[name_bbranch] = decay.descriptor_B

  print decay.descriptor_B

  print "Decay branches:", branches
  tuple.addBranches(branches)

#  tt_loki_B = tuple.__getattr__(name_bbranch).addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_B")
#  if "loki_variables_B" in decay.__dict__:
#    tt_loki_B.Variables = dict(loki_variables_B.items() + decay.loki_variables_B.items())
#  else:
#    tt_loki_B.Variables = loki_variables_B

  tuple.UseLabXSyntax = True
  tuple.RevertToPositiveID = False

  tuple.addTupleTool("TupleToolDecay/lab0")
  tt_tistos_b = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolTISTOS")
  tt_tistos_b.VerboseL0 = True
  tt_tistos_b.VerboseHlt1 = True
  tt_tistos_b.VerboseHlt2 = True
  tt_tistos_b.TriggerList = trigger_lines


  ## DecayTreeFitter variables  
  if dtf:  
    # without any constraints
    FitwithoutConst = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolDecayTreeFitter/FitwithoutConst")
    FitwithoutConst.Verbose = True
    FitwithoutConst.UpdateDaughters = True
    FitwithoutConst.daughtersToConstrain = []
    FitwithoutConst.constrainToOriginVertex = False

    # constraint only on PV (not on daughters)
    FitPVConst = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolDecayTreeFitter/FitPVConst")
    FitPVConst.Verbose = True
    FitPVConst.UpdateDaughters = True
    FitPVConst.daughtersToConstrain = []
    FitPVConst.constrainToOriginVertex = True

    # constraint only on daughters (not on PV) 
    i = 0
    for daughters in decay.daughters_to_constrain:
      fit_name = 'FitDaughtersConst'
      if i>0:
        fit_name = fit_name + str(i)
      FitDaughtersConst = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolDecayTreeFitter/" + fit_name)
      FitDaughtersConst.Verbose = True
      FitDaughtersConst.UpdateDaughters = True
      FitDaughtersConst.daughtersToConstrain = daughters
      FitDaughtersConst.constrainToOriginVertex = False
      if decay.tuple_name == "Bs2DspiDetached":
        FitDaughtersConst.Substitutions = {
          'B0 -> ^(D- -> K+ K- pi-) pi+' : 'D_s-',
          'B0 -> ^(D+ -> K- K+ pi+) pi-' : 'D_s+',
          'B~0 -> ^(D- -> K+ K- pi-) pi+' : 'D_s-',
          'B~0 -> ^(D+ -> K- K+ pi+) pi-' : 'D_s+'
        }
      i += 1

    # constraint on daughters and on the origin PV
    i = 0
    for daughters in decay.daughters_to_constrain:
      fit_name = 'FitDaughtersPVConst'
      if i>0:
        fit_name = fit_name + str(i)
      FitDaughtersPVConst = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolDecayTreeFitter/" + fit_name)
      FitDaughtersPVConst.Verbose = True
      FitDaughtersPVConst.UpdateDaughters = True
      FitDaughtersPVConst.daughtersToConstrain = daughters
      FitDaughtersPVConst.constrainToOriginVertex = True
      if decay.tuple_name == "Bs2DspiDetached":
        FitDaughtersPVConst.Substitutions = {
          'B0 -> ^(D- -> K+ K- pi-) pi+' : 'D_s-',
          'B0 -> ^(D+ -> K- K+ pi+) pi-' : 'D_s+',
          'B~0 -> ^(D- -> K+ K- pi-) pi+' : 'D_s-',
          'B~0 -> ^(D+ -> K- K+ pi+) pi-' : 'D_s+'
        }
      i += 1
  
#  for input in decay.inputs:
#      print "Trying to add " + input + " to refitter"
#      if input not in refitter.Inputs:
#        print "Adding " + input + " to refitter"
#        refitter.Inputs.append(input)
#  print "All refitter inputs: " + str(refitter.Inputs)
#  print ""
  return tuple

#TupleSeq.Members += [ CreateBTA(bs2dspi_detached)[0] , CreateTupleTool(bs2dspi_detached) ,  CreateBTA(bs2dspi_detached)[1] ]
TupleSeq.Members += [ CreateBTA(bd2dpi_detached)[0]  , CreateTupleTool(bd2dpi_detached)  ,  CreateBTA(bd2dpi_detached)[1]  ]
TupleSeq.Members += [ CreateBTA(bu2dpi_detached)[0]  , CreateTupleTool(bu2dpi_detached)  ,  CreateBTA(bu2dpi_detached)[1]  ]

TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False

# check PVs
checkPV = CheckPV("CheckForOnePV")
checkPV.MinPVs = 1
print checkPV

##############################################################################

seqB2OC.Members      += [checkPV,TupleSeq]
seqB2OC.ModeOR        = True
seqB2OC.ShortCircuit  = False

evtTuple                 = EventTuple()
evtTuple.ToolList       += ["TupleToolEventInfo", "TupleToolTrigger"]

DaVinci().EvtMax   = max_events
DaVinci().SkipEvents = skip_events
DaVinci().Lumi=True

# database tags
CondDB( LatestGlobalTagByDataType = "2012" )

DaVinci().TupleFile       = "DTT.root" # Ntuple

from Configurables import GaudiSequencer
subseq_annpid = GaudiSequencer('SeqANNPID')
 
from Configurables import ChargedProtoANNPIDConf
conf_ann_pid = ChargedProtoANNPIDConf('ANNPIDConf')
conf_ann_pid.RecoSequencer = subseq_annpid

#if update_track_ghostprob:
#  DaVinci().UserAlgorithms += [subseq_annpid]
#  DaVinci().UserAlgorithms += [refitter]

scaler = TrackScaleState('scaler')
DaVinci().UserAlgorithms += [scaler]

DaVinci().UserAlgorithms += [seqB2OC]
DaVinci().UserAlgorithms += [evtTuple]

#Stripping 21 test
#EventSelector().Input = ["/fhgfs/groups/e5/lhcb/bookkeeping/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00041836/0000/00041836_00000292_1.bhadroncompleteevent.dst"]


