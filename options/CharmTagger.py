"""
 Tagging sequences: provides variables specific for FT  training.
 Sequences can be imported in any otpion file and appended to DecayTreeTuple sequance 
"""
__author__ = "Basem Khanji <basem.khanji@cern.ch>" 

##############################################################################################
##### SETTINGS
from Configurables import DaVinci , LoKi__Hybrid__TupleTool, LoKi__Hybrid__ArrayTupleTool
from PhysSelPython.Wrappers import ( AutomaticData , Selection
                                     , SelectionSequence , DataOnDemand
                                     , Selection , MergedSelection , SelectionSequence )
import GaudiKernel.SystemOfUnits as Units
##############################################################################################
# SETTINGS :
CharmTagger    = 'C_T_'
charmModes  = [] 
D0Kpi   = DataOnDemand( Location ='Phys/Tag_StdD02KPi/Particles' )
D0K3Pi  = DataOnDemand( Location ='Phys/Tag_StdD02KPiPiPi/Particles' )
D0KPiPi0= DataOnDemand( Location ='Phys/Tag_StdD02KPiPi0/Particles' )
DpK2Pi  = DataOnDemand( Location ='Phys/Tag_StdDp2KPiPi/Particles' )
DKPipart     = DataOnDemand( Location ='Phys/Tag_StdD2KPipart/Particles' )
DKepart      = DataOnDemand( Location ='Phys/Tag_StdD2Kepart/Particles' )
D2Kmupart    = DataOnDemand( Location ='Phys/Tag_StdD2Kmupart/Particles' )
LambdaC2PKPi = DataOnDemand( Location ='Phys/Tag_StdLambdaC2PKPi/Particles' )
charmModes     =  [ D0Kpi, D0K3Pi , D0KPiPi0 , DpK2Pi , DKPipart , DKepart , D2Kmupart , LambdaC2PKPi  ]
Charmmode_flags = {}
for mode in charmModes:
    mode.outputLocation()
    VarName = CharmTagger + mode.name().split('Std')[1].split("_")[0]
    MyFlag  = "switch( INTES('" + mode.outputLocation()  + "'),0,1)"
    Charmmode_flags[VarName] = MyFlag
##############################################################################################
loki_var_Charmtagger = {

	CharmTagger + "Charge"           : "Q",
	CharmTagger + 'M'                : '0.001*M',
	CharmTagger + 'PT'               : '0.001*PT' ,
	CharmTagger + 'P'                : '0.001*P' ,
	CharmTagger + "E"                : "0.001*E",		          
	CharmTagger + "CORRM"            : "0.001*BPVCORRM",

	CharmTagger + "Eta"              : "ETA",		  
	CharmTagger + "THETA"            : "atan ((PT/PZ))",	  
	CharmTagger + "Phi"              : "PHI",		  

	CharmTagger + "MinIP"            : "MIPDV(PRIMARY)",	  
	CharmTagger + "MinIPChi2"        : "MIPCHI2DV(PRIMARY)",  
	CharmTagger + "BPVIP"            : "BPVIP()",	          
	CharmTagger + "BPVIPCHI2"        : "BPVIPCHI2()",         

	CharmTagger + "FD"               : "BPVVDR",
	CharmTagger + "FDCHI2"           : "BPVVDCHI2",                                                     
	CharmTagger + "FDS"              : "BPVDLS",                                                        
	CharmTagger + "DIRA"             : "BPVDIRA",

        CharmTagger + "Costheta_star1"   : "LV01",
        CharmTagger + "Costheta_star2"   : "LV02",

	CharmTagger + "DTF_TAU"          : "DTF_CTAU( 0, True )/0.299792458",				  
	CharmTagger + "DTF_CTAUS"        : "DTF_CTAUSIGNIFICANCE( 0, True )",			  
	CharmTagger + "DTF_CTAUERR"      : "DTF_CTAUERR( 0, True )",				  
	CharmTagger + "DTF_VCHI2NDOF"    : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )",		  

	CharmTagger + "DOCAMAX"          : "DOCAMAX",                                           
	CharmTagger + 'DOCACHI2MAX'      : 'DOCACHI2MAX' ,
	CharmTagger + 'MAXGHOSTPROB'     : 'MAXTREE(ISBASIC, PROBNNghost)' ,
	CharmTagger + 'SUMPT'            : 'SUMTREE(PT, ISBASIC)' ,

	CharmTagger + 'prot_PT'          : 'MAXTREE("p+" ==ABSID,         PT)' ,
	CharmTagger + 'prot_P'           : 'MAXTREE("p+" ==ABSID,          P)' ,
	CharmTagger + 'prot_PIDp'        : 'MAXTREE("p+" ==ABSID,       PIDp)' ,
	CharmTagger + 'prot_PIDK'        : 'MAXTREE("p+" ==ABSID,       PIDK)' ,
	CharmTagger + 'prot_ProbNNK'     : 'MAXTREE("p+" ==ABSID,    PROBNNk)' ,
	CharmTagger + 'prot_ProbNNp'     : 'MAXTREE("p+" ==ABSID,    PROBNNp)' ,
	CharmTagger + 'prot_ProbNNpi'    : 'MAXTREE("p+" ==ABSID,   PROBNNpi)' ,
	CharmTagger + 'prot_IP'          : 'MAXTREE("p+" ==ABSID,    BPVIP())' ,
	CharmTagger + 'prot_MIPCHI2'     : 'MAXTREE("p+" ==ABSID,MIPCHI2DV())' ,

	CharmTagger + 'K_PT'             : 'MAXTREE("K-"==ABSID, PT)' ,
	CharmTagger + 'K_P'              : 'MAXTREE("K-"==ABSID, P)' ,
	CharmTagger + 'K_PIDp'           : 'MAXTREE("K-"==ABSID, PIDp)' ,
	CharmTagger + 'K_PIDK'           : 'MAXTREE("K-"==ABSID, PIDK)' ,
	CharmTagger + 'K_ProbNNK'        : 'MAXTREE("K-"==ABSID, PROBNNk)' ,
	CharmTagger + 'K_ProbNNp'        : 'MAXTREE("K-"==ABSID, PROBNNp)' ,
	CharmTagger + 'K_ProbNNpi'       : 'MAXTREE("K-"==ABSID, PROBNNpi)' ,
	CharmTagger + 'K_IP'             : 'MAXTREE("K-"==ABSID, BPVIP())' ,
	CharmTagger + 'K_MIPCHI2'        : 'MAXTREE("K-"==ABSID, MIPCHI2DV())' ,

	CharmTagger + 'pi_PT'            : 'MINTREE("pi-"==ABSID,         PT)' ,
	CharmTagger + 'pi_P'             : 'MINTREE("pi-"==ABSID,          P)' ,
	CharmTagger + 'pi_PIDp'          : 'MAXTREE("pi-"==ABSID,       PIDp)' ,
	CharmTagger + 'pi_PIDK'          : 'MAXTREE("pi-"==ABSID,       PIDK)' ,
	CharmTagger + 'pi_ProbNNK'       : 'MAXTREE("pi-"==ABSID,    PROBNNk)' ,
	CharmTagger + 'pi_ProbNNp'       : 'MAXTREE("pi-"==ABSID,    PROBNNp)' ,
	CharmTagger + 'pi_ProbNNpi'      : 'MINTREE("pi-"==ABSID,   PROBNNpi)' ,
	CharmTagger + 'pi_IP'            : 'MINTREE("pi-"==ABSID,    BPVIP())' ,
	CharmTagger + 'pi_MIPCHI2'       : 'MINTREE("pi-"==ABSID, MIPCHI2DV())',

	CharmTagger + 'e_PT'            : 'MAXTREE("e-"==ABSID, PT)' ,
	CharmTagger + 'e_P'             : 'MAXTREE("e-"==ABSID, P)' ,
	CharmTagger + 'e_PIDp'          : 'MAXTREE("e-"==ABSID, PIDp)' ,
	CharmTagger + 'e_PIDK'          : 'MAXTREE("e-"==ABSID, PIDK)' ,
	CharmTagger + 'e_ProbNNK'       : 'MAXTREE("e-"==ABSID, PROBNNk)' ,
	CharmTagger + 'e_ProbNNp'       : 'MAXTREE("e-"==ABSID, PROBNNp)' ,
	CharmTagger + 'e_ProbNNpi'      : 'MAXTREE("e-"==ABSID, PROBNNpi)' ,
	CharmTagger + 'e_ProbNNe'       : 'MAXTREE("e-"==ABSID, PROBNNe)' ,
	CharmTagger + 'e_ProbNNmu'      : 'MAXTREE("e-"==ABSID, PROBNNmu)' ,
	CharmTagger + 'e_IP'            : 'MAXTREE("e-"==ABSID, BPVIP())' ,
	CharmTagger + 'e_MIPCHI2'       : 'MAXTREE("e-"==ABSID, MIPCHI2DV())' ,

	CharmTagger + 'mu_PT'            : 'MAXTREE("mu-"==ABSID, PT)' ,
	CharmTagger + 'mu_P'             : 'MAXTREE("mu-"==ABSID, P)' ,
	CharmTagger + 'mu_PIDp'          : 'MAXTREE("mu-"==ABSID, PIDp)' ,
	CharmTagger + 'mu_PIDK'          : 'MAXTREE("mu-"==ABSID, PIDK)' ,
	CharmTagger + 'mu_ProbNNK'       : 'MAXTREE("mu-"==ABSID, PROBNNk)' ,
	CharmTagger + 'mu_ProbNNp'       : 'MAXTREE("mu-"==ABSID, PROBNNp)' ,
	CharmTagger + 'mu_ProbNNpi'      : 'MAXTREE("mu-"==ABSID, PROBNNpi)' ,
	CharmTagger + 'mu_ProbNNe'       : 'MAXTREE("mu-"==ABSID, PROBNNe)' ,
	CharmTagger + 'mu_ProbNNmu'      : 'MAXTREE("mu-"==ABSID, PROBNNmu)' ,
	CharmTagger + 'mu_IP'            : 'MAXTREE("mu-"==ABSID, BPVIP())' ,
	CharmTagger + 'mu_MIPCHI2'       : 'MAXTREE("mu-"==ABSID, MIPCHI2DV())' ,
        }

loki_var_Charmtagger = dict( loki_var_Charmtagger.items() + Charmmode_flags.items())
loki_Avar_Charm = {
	CharmTagger + "ADOCA" :  "ADOCA(1,2)",
	CharmTagger + "ACHI2DOCA":"ACHI2DOCA(1,2)",
        #CharmTagger + "AALLSAMEBPV" : "switch(AALLSAMEBPV( 20 , 16 , 0.8  ),0,1)"
        CharmTagger + "AALLSAMEBPV" : "switch(AALLSAMEBPV( -1 , -1 , 0.8  ),0,1)" #Use only fraction of common tracks
	}

#
def addCharmTuple(tuple , decay):
    AllCharmTaggers            = MergedSelection("AllCharmTaggers"+ decay.tuple_name, RequiredSelections = charmModes )
    AllCharmTaggers_seq        = SelectionSequence('AllCharmTaggers_seq' + decay.tuple_name, TopSelection = AllCharmTaggers)
    DaVinci().appendToMainSequence( [ AllCharmTaggers_seq ])
    LoKiVariables_CharmTaggers = tuple.addTupleTool("LoKi::Hybrid::ArrayTupleTool/LoKiV_CTag_"+ decay.tuple_name)
    #LoKiVariables_CharmTaggers             = LoKi__Hybrid__ArrayTupleTool("LoKiVariables_CharmTaggers")
    LoKiVariables_CharmTaggers.Preambulo   = [ "from LoKiTracks.decorators import *" ]
    LoKiVariables_CharmTaggers.Variables   =  loki_var_Charmtagger 
    LoKiVariables_CharmTaggers.AVariables  =  loki_Avar_Charm
    #LoKiVariables_CharmTaggers.Source      = "SOURCE('Phys/Tag_StdD02KPi/Particles', PT>0.05*GeV )" #"Phys/StdAllNoPIDsPions/Particles"
    LoKiVariables_CharmTaggers.Source      = "SOURCE('"+ AllCharmTaggers_seq.outputLocation() +"', ALL )" #"Phys/StdAllNoPIDsPions/Particles"
    LoKiVariables_CharmTaggers.OutputLevel = 1

  

