#!/usr/bin/env python

#Requires at least Ganga v601r23 to use accessURL()

import sys
import os
import subprocess, multiprocessing 
from os.path import isfile
#import ROOT
#from ROOT import *

# Options:
eos_prefix      = 'root://eoslhcb.cern.ch/' 
grid_dir_prefix = '/eos/lhcb/grid/user'                   
#Done =         124, 128, 121, 122
numjobs          = [123, 125, 126, 127]

def IsWhere(Sjobs, where):
    dirac_files = Sjobs.outputfiles.get(DiracFile)
    if dirac_files.__len__()>0:
        dirac_file = dirac_files[0]
        location = dirac_file.locations
        if location.__len__()>0:
            return location[0] == where
        else:
            return False
    else:
        return False

def IsCompleted(Sjobs):
    return Sjobs.status == 'completed'

#def IsCopied(filepath):
#    tryfile = ROOT.TFile.Open(filepath,"READ")
#    if tryfile:
#        tryfile.Close()
#        del tryfile
#        return True
#    else:
#        del tryfile
#        return False

for j in numjobs:

    #i=0

    job = jobs(j)
    print '################ Looking at Job: ', job.id
    print '###### Number of subjobs: ', job.subjobs.__len__()
    nickname = job.name
    if nickname == "":
        print "ERROR: job "+str(j)+" has no nickname"
        continue
    eos_output      = '/eos/lhcb/wg/FlavourTagging/tuples/development/data/B2OC/raw/'+nickname+'/'
    name            = "DTT_"+nickname
    #haddfile        = "listHadd_"+name+".txt"
    print '###### Job name: ', nickname
    print '###### EOS output directory: ', eos_output
    print '###### Output file prefix: ', name
    #os.system("rm -f "+haddfile)

    os.system("eos rm -r "+eos_output) #be careful...
    os.system("eos mkdir -p "+eos_output)

    for sj in job.subjobs:
        print '### Looking at subjob: ' , sj.id

        i = sj.id

        #if sj.id < 3552:
            #i = i+1
        #    continue
        
        if( IsCompleted(sj) ):
            if( IsWhere(sj, 'CERN-USER') ):
                print "Subjob "+str(sj.id)+" output found at CERN"
                Lfns = sj.outputfiles.get(DiracFile)
                if Lfns.__len__()>0:
                    MyLfn      = Lfns[0].lfn
                    input_f    = eos_prefix + grid_dir_prefix + MyLfn
                    print 'Input file: '+input_f
                    os.system("xrdcopy "+input_f+" "+eos_prefix+eos_output+name+"_"+str(i)+".root")
                else:
                    print "LFNs not found. Skipping subjob..."
                    continue
            else:
                print "Subjob "+str(sj.id)+" output not at CERN. Taking URL..."
                try:
                    url = sj.outputfiles.get(DiracFile)[0].accessURL()
                except:
                    print "Exception caught when taking URL. Skipping subjob..."
                    pass
                else:
                    if url.__len__() == 0:
                        print "No url available. Skipping subjob..."
                        continue
                    if IsWhere(sj, 'CNAF-USER') and url[0].startswith('file://'):
                        url[0] = url[0].replace('file://', 'root://xrootd-lhcb.cr.cnaf.infn.it/')
                    if 'clhcbstager' in url[0]:
                        url[0] = url[0].replace('clhcbstager', 'clhcbdlf')
                    input_f = url[0]
            
                    print 'Input file: '+input_f
                    os.system("xrdcopy "+input_f+" "+eos_prefix+eos_output+name+"_"+str(i)+".root")

            #i = i+1
