//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jan 18 18:34:35 2017 by ROOT version 5.34/36
// from TTree DecayTree/DecayTree
// found on file: DTT_B2JpsiX_ALTR.root
//////////////////////////////////////////////////////////

#ifndef DecayTree_h
#define DecayTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

// Fixed size dimensions of array or collections stored in the TTree if any.
   const Int_t kMaxB_ENDVERTEX_COV = 1;
   const Int_t kMaxB_OWNPV_COV = 1;
   const Int_t kMaxJ_psi_1S_ENDVERTEX_COV = 1;
   const Int_t kMaxJ_psi_1S_OWNPV_COV = 1;
   const Int_t kMaxJ_psi_1S_ORIVX_COV = 1;
   const Int_t kMaxmuplus_OWNPV_COV = 1;
   const Int_t kMaxmuplus_ORIVX_COV = 1;
   const Int_t kMaxmuminus_OWNPV_COV = 1;
   const Int_t kMaxmuminus_ORIVX_COV = 1;
   const Int_t kMaxKplus_OWNPV_COV = 1;
   const Int_t kMaxKplus_ORIVX_COV = 1;

class DecayTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Int_t           B_len;
   Float_t         C_T_BPVIP[1000];   //[B_len]
   Float_t         C_T_BPVIPCHI2[1000];   //[B_len]
   Float_t         C_T_CORRM[1000];   //[B_len]
   Float_t         C_T_Charge[1000];   //[B_len]
   Float_t         C_T_Costheta_star1[1000];   //[B_len]
   Float_t         C_T_Costheta_star2[1000];   //[B_len]
   Float_t         C_T_D02KPi[1000];   //[B_len]
   Float_t         C_T_D02KPiPi0[1000];   //[B_len]
   Float_t         C_T_D02KPiPiPi[1000];   //[B_len]
   Float_t         C_T_D2KPipart[1000];   //[B_len]
   Float_t         C_T_D2Kepart[1000];   //[B_len]
   Float_t         C_T_D2Kmupart[1000];   //[B_len]
   Float_t         C_T_DIRA[1000];   //[B_len]
   Float_t         C_T_DOCACHI2MAX[1000];   //[B_len]
   Float_t         C_T_DOCAMAX[1000];   //[B_len]
   Float_t         C_T_DTF_CTAUERR[1000];   //[B_len]
   Float_t         C_T_DTF_CTAUS[1000];   //[B_len]
   Float_t         C_T_DTF_TAU[1000];   //[B_len]
   Float_t         C_T_DTF_VCHI2NDOF[1000];   //[B_len]
   Float_t         C_T_Dp2KPiPi[1000];   //[B_len]
   Float_t         C_T_E[1000];   //[B_len]
   Float_t         C_T_Eta[1000];   //[B_len]
   Float_t         C_T_FD[1000];   //[B_len]
   Float_t         C_T_FDCHI2[1000];   //[B_len]
   Float_t         C_T_FDS[1000];   //[B_len]
   Float_t         C_T_K_IP[1000];   //[B_len]
   Float_t         C_T_K_MIPCHI2[1000];   //[B_len]
   Float_t         C_T_K_P[1000];   //[B_len]
   Float_t         C_T_K_PIDK[1000];   //[B_len]
   Float_t         C_T_K_PIDp[1000];   //[B_len]
   Float_t         C_T_K_PT[1000];   //[B_len]
   Float_t         C_T_K_ProbNNK[1000];   //[B_len]
   Float_t         C_T_K_ProbNNp[1000];   //[B_len]
   Float_t         C_T_K_ProbNNpi[1000];   //[B_len]
   Float_t         C_T_LambdaC2PKPi[1000];   //[B_len]
   Float_t         C_T_M[1000];   //[B_len]
   Float_t         C_T_MAXGHOSTPROB[1000];   //[B_len]
   Float_t         C_T_MinIP[1000];   //[B_len]
   Float_t         C_T_MinIPChi2[1000];   //[B_len]
   Float_t         C_T_P[1000];   //[B_len]
   Float_t         C_T_PT[1000];   //[B_len]
   Float_t         C_T_Phi[1000];   //[B_len]
   Float_t         C_T_SUMPT[1000];   //[B_len]
   Float_t         C_T_THETA[1000];   //[B_len]
   Float_t         C_T_e_IP[1000];   //[B_len]
   Float_t         C_T_e_MIPCHI2[1000];   //[B_len]
   Float_t         C_T_e_P[1000];   //[B_len]
   Float_t         C_T_e_PIDK[1000];   //[B_len]
   Float_t         C_T_e_PIDp[1000];   //[B_len]
   Float_t         C_T_e_PT[1000];   //[B_len]
   Float_t         C_T_e_ProbNNK[1000];   //[B_len]
   Float_t         C_T_e_ProbNNe[1000];   //[B_len]
   Float_t         C_T_e_ProbNNmu[1000];   //[B_len]
   Float_t         C_T_e_ProbNNp[1000];   //[B_len]
   Float_t         C_T_e_ProbNNpi[1000];   //[B_len]
   Float_t         C_T_mu_IP[1000];   //[B_len]
   Float_t         C_T_mu_MIPCHI2[1000];   //[B_len]
   Float_t         C_T_mu_P[1000];   //[B_len]
   Float_t         C_T_mu_PIDK[1000];   //[B_len]
   Float_t         C_T_mu_PIDp[1000];   //[B_len]
   Float_t         C_T_mu_PT[1000];   //[B_len]
   Float_t         C_T_mu_ProbNNK[1000];   //[B_len]
   Float_t         C_T_mu_ProbNNe[1000];   //[B_len]
   Float_t         C_T_mu_ProbNNmu[1000];   //[B_len]
   Float_t         C_T_mu_ProbNNp[1000];   //[B_len]
   Float_t         C_T_mu_ProbNNpi[1000];   //[B_len]
   Float_t         C_T_pi_IP[1000];   //[B_len]
   Float_t         C_T_pi_MIPCHI2[1000];   //[B_len]
   Float_t         C_T_pi_P[1000];   //[B_len]
   Float_t         C_T_pi_PIDK[1000];   //[B_len]
   Float_t         C_T_pi_PIDp[1000];   //[B_len]
   Float_t         C_T_pi_PT[1000];   //[B_len]
   Float_t         C_T_pi_ProbNNK[1000];   //[B_len]
   Float_t         C_T_pi_ProbNNp[1000];   //[B_len]
   Float_t         C_T_pi_ProbNNpi[1000];   //[B_len]
   Float_t         C_T_prot_IP[1000];   //[B_len]
   Float_t         C_T_prot_MIPCHI2[1000];   //[B_len]
   Float_t         C_T_prot_P[1000];   //[B_len]
   Float_t         C_T_prot_PIDK[1000];   //[B_len]
   Float_t         C_T_prot_PIDp[1000];   //[B_len]
   Float_t         C_T_prot_PT[1000];   //[B_len]
   Float_t         C_T_prot_ProbNNK[1000];   //[B_len]
   Float_t         C_T_prot_ProbNNp[1000];   //[B_len]
   Float_t         C_T_prot_ProbNNpi[1000];   //[B_len]
   Float_t         C_T_ACHI2DOCA[1000];   //[B_len]
   Float_t         C_T_ADOCA[1000];   //[B_len]
   Double_t        B_LOKI_ENERGY;
   Double_t        B_LOKI_ETA;
   Double_t        B_LOKI_PHI;
   Double_t        B_ENDVERTEX_X;
   Double_t        B_ENDVERTEX_Y;
   Double_t        B_ENDVERTEX_Z;
   Double_t        B_ENDVERTEX_XERR;
   Double_t        B_ENDVERTEX_YERR;
   Double_t        B_ENDVERTEX_ZERR;
   Double_t        B_ENDVERTEX_CHI2;
   Int_t           B_ENDVERTEX_NDOF;
   Float_t         B_ENDVERTEX_COV_[3][3];
   Double_t        B_OWNPV_X;
   Double_t        B_OWNPV_Y;
   Double_t        B_OWNPV_Z;
   Double_t        B_OWNPV_XERR;
   Double_t        B_OWNPV_YERR;
   Double_t        B_OWNPV_ZERR;
   Double_t        B_OWNPV_CHI2;
   Int_t           B_OWNPV_NDOF;
   Float_t         B_OWNPV_COV_[3][3];
   Double_t        B_IP_OWNPV;
   Double_t        B_IPCHI2_OWNPV;
   Double_t        B_FD_OWNPV;
   Double_t        B_FDCHI2_OWNPV;
   Double_t        B_DIRA_OWNPV;
   Int_t           N;
   Float_t         Tr_T_Sum_of_trackp[500];   //[N]
   Float_t         Tr_T_Sum_of_trackpt[500];   //[N]
   Float_t         Tr_T_Cone_asym_Pt[500];   //[N]
   Float_t         Tr_T_Cone_asym_P[500];   //[N]
   Float_t         Tr_T_ConIso_p_ult[500];   //[N]
   Float_t         Tr_T_ConIso_pt_ult[500];   //[N]
   Float_t         Tr_T_Ntr_incone[500];   //[N]
   Float_t         Tr_T_Mother_VtxChi2[500];   //[N]
   Float_t         Tr_T_IPCHI2_trMother[500];   //[N]
   Float_t         Tr_T_IP_trMother[500];   //[N]
   Float_t         Tr_T_x[500];   //[N]
   Float_t         Tr_T_y[500];   //[N]
   Float_t         Tr_T_xfirst[500];   //[N]
   Float_t         Tr_T_yfirst[500];   //[N]
   Float_t         Tr_T_zfirst[500];   //[N]
   Float_t         Tr_T_SumBDT_sigtr[500];   //[N]
   Float_t         Tr_T_MinBDT_sigtr[500];   //[N]
   Float_t         Tr_T_NbTrNonIso_sigtr[500];   //[N]
   Float_t         Tr_T_SumBDT_ult[500];   //[N]
   Float_t         Tr_T_nBDT_ult[500];   //[N]
   Float_t         Tr_T_NbNonIsoTr_ult[500];   //[N]
   Float_t         Tr_T_Best_PAIR_VCHI2[500];   //[N]
   Float_t         Tr_T_Best_PAIR_D[500];   //[N]
   Float_t         Tr_T_Best_PAIR_DCHI2[500];   //[N]
   Float_t         Tr_T_Best_PAIR_M_fromiso[500];   //[N]
   Double_t        B_P;
   Double_t        B_PT;
   Double_t        B_PE;
   Double_t        B_PX;
   Double_t        B_PY;
   Double_t        B_PZ;
   Double_t        B_MM;
   Double_t        B_MMERR;
   Double_t        B_M;
   Double_t        B_TAU;
   Double_t        B_TAUERR;
   Double_t        B_TAUCHI2;
   Int_t           B_TAGDECISION;
   Double_t        B_TAGOMEGA;
   Int_t           B_TAGDECISION_OS;
   Double_t        B_TAGOMEGA_OS;
   Int_t           B_TAGGER;
   Short_t         B_OS_Muon_DEC;
   Float_t         B_OS_Muon_PROB;
   Short_t         B_OS_Electron_DEC;
   Float_t         B_OS_Electron_PROB;
   Short_t         B_OS_Kaon_DEC;
   Float_t         B_OS_Kaon_PROB;
   Short_t         B_SS_Kaon_DEC;
   Float_t         B_SS_Kaon_PROB;
   Short_t         B_SS_Pion_DEC;
   Float_t         B_SS_Pion_PROB;
   Short_t         B_SS_PionBDT_DEC;
   Float_t         B_SS_PionBDT_PROB;
   Short_t         B_VtxCharge_DEC;
   Float_t         B_VtxCharge_PROB;
   Short_t         B_OS_nnetKaon_DEC;
   Float_t         B_OS_nnetKaon_PROB;
   Short_t         B_SS_nnetKaon_DEC;
   Float_t         B_SS_nnetKaon_PROB;
   Short_t         B_SS_Proton_DEC;
   Float_t         B_SS_Proton_PROB;
   Short_t         B_OS_Charm_DEC;
   Float_t         B_OS_Charm_PROB;
   Int_t           V;
   Float_t         SecVtx_pt1[100];   //[V]
   Float_t         SecVtx_pt2[100];   //[V]
   Float_t         SecVtx_x[100];   //[V]
   Float_t         SecVtx_y[100];   //[V]
   Float_t         SecVtx_z[100];   //[V]
   Float_t         SecVtx_zerr[100];   //[V]
   Float_t         SecVtx_chi2[100];   //[V]
   Float_t         BOVtx_x[100];   //[V]
   Float_t         BOVtx_y[100];   //[V]
   Float_t         BOVtx_z[100];   //[V]
   Float_t         BOVtx_zerr[100];   //[V]
   Float_t         BOVtx_chi2[100];   //[V]
   Int_t           vtags;
   Float_t         IPSV[200];   //[vtags]
   Float_t         IPSVerr[200];   //[vtags]
   Float_t         DOCA[200];   //[vtags]
   Float_t         DOCAerr[200];   //[vtags]
   Float_t         distphi[200];   //[vtags]
   Double_t        B_LOKI_DTF_CTAU;
   Double_t        B_LOKI_MASS_JpsiConstr;
   Int_t           J_psi_1S_len;
   Double_t        J_psi_1S_LOKI_ENERGY;
   Double_t        J_psi_1S_LOKI_ETA;
   Double_t        J_psi_1S_LOKI_PHI;
   Double_t        J_psi_1S_ENDVERTEX_X;
   Double_t        J_psi_1S_ENDVERTEX_Y;
   Double_t        J_psi_1S_ENDVERTEX_Z;
   Double_t        J_psi_1S_ENDVERTEX_XERR;
   Double_t        J_psi_1S_ENDVERTEX_YERR;
   Double_t        J_psi_1S_ENDVERTEX_ZERR;
   Double_t        J_psi_1S_ENDVERTEX_CHI2;
   Int_t           J_psi_1S_ENDVERTEX_NDOF;
   Float_t         J_psi_1S_ENDVERTEX_COV_[3][3];
   Double_t        J_psi_1S_OWNPV_X;
   Double_t        J_psi_1S_OWNPV_Y;
   Double_t        J_psi_1S_OWNPV_Z;
   Double_t        J_psi_1S_OWNPV_XERR;
   Double_t        J_psi_1S_OWNPV_YERR;
   Double_t        J_psi_1S_OWNPV_ZERR;
   Double_t        J_psi_1S_OWNPV_CHI2;
   Int_t           J_psi_1S_OWNPV_NDOF;
   Float_t         J_psi_1S_OWNPV_COV_[3][3];
   Double_t        J_psi_1S_IP_OWNPV;
   Double_t        J_psi_1S_IPCHI2_OWNPV;
   Double_t        J_psi_1S_FD_OWNPV;
   Double_t        J_psi_1S_FDCHI2_OWNPV;
   Double_t        J_psi_1S_DIRA_OWNPV;
   Double_t        J_psi_1S_ORIVX_X;
   Double_t        J_psi_1S_ORIVX_Y;
   Double_t        J_psi_1S_ORIVX_Z;
   Double_t        J_psi_1S_ORIVX_XERR;
   Double_t        J_psi_1S_ORIVX_YERR;
   Double_t        J_psi_1S_ORIVX_ZERR;
   Double_t        J_psi_1S_ORIVX_CHI2;
   Int_t           J_psi_1S_ORIVX_NDOF;
   Float_t         J_psi_1S_ORIVX_COV_[3][3];
   Double_t        J_psi_1S_FD_ORIVX;
   Double_t        J_psi_1S_FDCHI2_ORIVX;
   Double_t        J_psi_1S_DIRA_ORIVX;
   Double_t        J_psi_1S_P;
   Double_t        J_psi_1S_PT;
   Double_t        J_psi_1S_PE;
   Double_t        J_psi_1S_PX;
   Double_t        J_psi_1S_PY;
   Double_t        J_psi_1S_PZ;
   Double_t        J_psi_1S_MM;
   Double_t        J_psi_1S_MMERR;
   Double_t        J_psi_1S_M;
   Double_t        J_psi_1S_TAU;
   Double_t        J_psi_1S_TAUERR;
   Double_t        J_psi_1S_TAUCHI2;
   Int_t           muplus_len;
   Float_t         Tr_T_BPVIP[1000];   //[muplus_len]
   Float_t         Tr_T_BPVIPCHI2[1000];   //[muplus_len]
   Float_t         Tr_T_Charge[1000];   //[muplus_len]
   Float_t         Tr_T_E[1000];   //[muplus_len]
   Float_t         Tr_T_EcalE[1000];   //[muplus_len]
   Float_t         Tr_T_Eta[1000];   //[muplus_len]
   Float_t         Tr_T_HASMUON[1000];   //[muplus_len]
   Float_t         Tr_T_HASRICH[1000];   //[muplus_len]
   Float_t         Tr_T_HcalE[1000];   //[muplus_len]
   Float_t         Tr_T_ISMUON[1000];   //[muplus_len]
   Float_t         Tr_T_MinIP[1000];   //[muplus_len]
   Float_t         Tr_T_MinIPChi2[1000];   //[muplus_len]
   Float_t         Tr_T_NSHARED[1000];   //[muplus_len]
   Float_t         Tr_T_P[1000];   //[muplus_len]
   Float_t         Tr_T_PIDK[1000];   //[muplus_len]
   Float_t         Tr_T_PIDe[1000];   //[muplus_len]
   Float_t         Tr_T_PIDmu[1000];   //[muplus_len]
   Float_t         Tr_T_PIDp[1000];   //[muplus_len]
   Float_t         Tr_T_PROBNNe[1000];   //[muplus_len]
   Float_t         Tr_T_PROBNNghost[1000];   //[muplus_len]
   Float_t         Tr_T_PROBNNk[1000];   //[muplus_len]
   Float_t         Tr_T_PROBNNmu[1000];   //[muplus_len]
   Float_t         Tr_T_PROBNNp[1000];   //[muplus_len]
   Float_t         Tr_T_PROBNNpi[1000];   //[muplus_len]
   Float_t         Tr_T_PT[1000];   //[muplus_len]
   Float_t         Tr_T_Phi[1000];   //[muplus_len]
   Float_t         Tr_T_PrsE[1000];   //[muplus_len]
   Float_t         Tr_T_THETA[1000];   //[muplus_len]
   Float_t         Tr_T_TRCHI2DOF[1000];   //[muplus_len]
   Float_t         Tr_T_TRCLONEDIST[1000];   //[muplus_len]
   Float_t         Tr_T_TRFITMATCHCHI2[1000];   //[muplus_len]
   Float_t         Tr_T_TRFITTCHI2[1000];   //[muplus_len]
   Float_t         Tr_T_TRFITVELOCHI2NDOF[1000];   //[muplus_len]
   Float_t         Tr_T_TRGHOSTPROB[1000];   //[muplus_len]
   Float_t         Tr_T_TRPCHI2[1000];   //[muplus_len]
   Float_t         Tr_T_TRTYPE[1000];   //[muplus_len]
   Float_t         Tr_T_TrFIRSTHITZ[1000];   //[muplus_len]
   Float_t         Tr_T_TrFITTCHI2NDOF[1000];   //[muplus_len]
   Float_t         Tr_T_ACHI2DOCA[1000];   //[muplus_len]
   Float_t         Tr_T_ADOCA[1000];   //[muplus_len]
   Double_t        muplus_LOKI_ENERGY;
   Double_t        muplus_LOKI_ETA;
   Double_t        muplus_LOKI_PHI;
   Double_t        muplus_OWNPV_X;
   Double_t        muplus_OWNPV_Y;
   Double_t        muplus_OWNPV_Z;
   Double_t        muplus_OWNPV_XERR;
   Double_t        muplus_OWNPV_YERR;
   Double_t        muplus_OWNPV_ZERR;
   Double_t        muplus_OWNPV_CHI2;
   Int_t           muplus_OWNPV_NDOF;
   Float_t         muplus_OWNPV_COV_[3][3];
   Double_t        muplus_IP_OWNPV;
   Double_t        muplus_IPCHI2_OWNPV;
   Double_t        muplus_ORIVX_X;
   Double_t        muplus_ORIVX_Y;
   Double_t        muplus_ORIVX_Z;
   Double_t        muplus_ORIVX_XERR;
   Double_t        muplus_ORIVX_YERR;
   Double_t        muplus_ORIVX_ZERR;
   Double_t        muplus_ORIVX_CHI2;
   Int_t           muplus_ORIVX_NDOF;
   Float_t         muplus_ORIVX_COV_[3][3];
   Double_t        muplus_P;
   Double_t        muplus_PT;
   Double_t        muplus_PE;
   Double_t        muplus_PX;
   Double_t        muplus_PY;
   Double_t        muplus_PZ;
   Double_t        muplus_M;
   Int_t           muminus_len;
   Double_t        muminus_LOKI_ENERGY;
   Double_t        muminus_LOKI_ETA;
   Double_t        muminus_LOKI_PHI;
   Double_t        muminus_OWNPV_X;
   Double_t        muminus_OWNPV_Y;
   Double_t        muminus_OWNPV_Z;
   Double_t        muminus_OWNPV_XERR;
   Double_t        muminus_OWNPV_YERR;
   Double_t        muminus_OWNPV_ZERR;
   Double_t        muminus_OWNPV_CHI2;
   Int_t           muminus_OWNPV_NDOF;
   Float_t         muminus_OWNPV_COV_[3][3];
   Double_t        muminus_IP_OWNPV;
   Double_t        muminus_IPCHI2_OWNPV;
   Double_t        muminus_ORIVX_X;
   Double_t        muminus_ORIVX_Y;
   Double_t        muminus_ORIVX_Z;
   Double_t        muminus_ORIVX_XERR;
   Double_t        muminus_ORIVX_YERR;
   Double_t        muminus_ORIVX_ZERR;
   Double_t        muminus_ORIVX_CHI2;
   Int_t           muminus_ORIVX_NDOF;
   Float_t         muminus_ORIVX_COV_[3][3];
   Double_t        muminus_P;
   Double_t        muminus_PT;
   Double_t        muminus_PE;
   Double_t        muminus_PX;
   Double_t        muminus_PY;
   Double_t        muminus_PZ;
   Double_t        muminus_M;
   Int_t           Kplus_len;
   Double_t        Kplus_LOKI_ENERGY;
   Double_t        Kplus_LOKI_ETA;
   Double_t        Kplus_LOKI_PHI;
   Double_t        Kplus_OWNPV_X;
   Double_t        Kplus_OWNPV_Y;
   Double_t        Kplus_OWNPV_Z;
   Double_t        Kplus_OWNPV_XERR;
   Double_t        Kplus_OWNPV_YERR;
   Double_t        Kplus_OWNPV_ZERR;
   Double_t        Kplus_OWNPV_CHI2;
   Int_t           Kplus_OWNPV_NDOF;
   Float_t         Kplus_OWNPV_COV_[3][3];
   Double_t        Kplus_IP_OWNPV;
   Double_t        Kplus_IPCHI2_OWNPV;
   Double_t        Kplus_ORIVX_X;
   Double_t        Kplus_ORIVX_Y;
   Double_t        Kplus_ORIVX_Z;
   Double_t        Kplus_ORIVX_XERR;
   Double_t        Kplus_ORIVX_YERR;
   Double_t        Kplus_ORIVX_ZERR;
   Double_t        Kplus_ORIVX_CHI2;
   Int_t           Kplus_ORIVX_NDOF;
   Float_t         Kplus_ORIVX_COV_[3][3];
   Double_t        Kplus_P;
   Double_t        Kplus_PT;
   Double_t        Kplus_PE;
   Double_t        Kplus_PX;
   Double_t        Kplus_PY;
   Double_t        Kplus_PZ;
   Double_t        Kplus_M;
   UInt_t          nCandidate;
   ULong64_t       totCandidates;
   ULong64_t       EventInSequence;
   UInt_t          runNumber;
   ULong64_t       eventNumber;
   UInt_t          BCID;
   Int_t           BCType;
   UInt_t          OdinTCK;
   UInt_t          L0DUTCK;
   UInt_t          HLT1TCK;
   UInt_t          HLT2TCK;
   ULong64_t       GpsTime;
   Short_t         Polarity;
   Int_t           nPV;
   Float_t         PVX[100];   //[nPV]
   Float_t         PVY[100];   //[nPV]
   Float_t         PVZ[100];   //[nPV]
   Float_t         PVXERR[100];   //[nPV]
   Float_t         PVYERR[100];   //[nPV]
   Float_t         PVZERR[100];   //[nPV]
   Float_t         PVCHI2[100];   //[nPV]
   Float_t         PVNDOF[100];   //[nPV]
   Float_t         PVNTRACKS[100];   //[nPV]
   UInt_t          StrippingBetaSBs2JpsiPhiDetachedLineDecision;
   UInt_t          StrippingBetaSBd2JpsiKstarDetachedLineDecision;
   UInt_t          StrippingBetaSBu2JpsiKDetachedLineDecision;

   // List of branches
   TBranch        *b_B_len;   //!
   TBranch        *b_C_T_BPVIP;   //!
   TBranch        *b_C_T_BPVIPCHI2;   //!
   TBranch        *b_C_T_CORRM;   //!
   TBranch        *b_C_T_Charge;   //!
   TBranch        *b_C_T_Costheta_star1;   //!
   TBranch        *b_C_T_Costheta_star2;   //!
   TBranch        *b_C_T_D02KPi;   //!
   TBranch        *b_C_T_D02KPiPi0;   //!
   TBranch        *b_C_T_D02KPiPiPi;   //!
   TBranch        *b_C_T_D2KPipart;   //!
   TBranch        *b_C_T_D2Kepart;   //!
   TBranch        *b_C_T_D2Kmupart;   //!
   TBranch        *b_C_T_DIRA;   //!
   TBranch        *b_C_T_DOCACHI2MAX;   //!
   TBranch        *b_C_T_DOCAMAX;   //!
   TBranch        *b_C_T_DTF_CTAUERR;   //!
   TBranch        *b_C_T_DTF_CTAUS;   //!
   TBranch        *b_C_T_DTF_TAU;   //!
   TBranch        *b_C_T_DTF_VCHI2NDOF;   //!
   TBranch        *b_C_T_Dp2KPiPi;   //!
   TBranch        *b_C_T_E;   //!
   TBranch        *b_C_T_Eta;   //!
   TBranch        *b_C_T_FD;   //!
   TBranch        *b_C_T_FDCHI2;   //!
   TBranch        *b_C_T_FDS;   //!
   TBranch        *b_C_T_K_IP;   //!
   TBranch        *b_C_T_K_MIPCHI2;   //!
   TBranch        *b_C_T_K_P;   //!
   TBranch        *b_C_T_K_PIDK;   //!
   TBranch        *b_C_T_K_PIDp;   //!
   TBranch        *b_C_T_K_PT;   //!
   TBranch        *b_C_T_K_ProbNNK;   //!
   TBranch        *b_C_T_K_ProbNNp;   //!
   TBranch        *b_C_T_K_ProbNNpi;   //!
   TBranch        *b_C_T_LambdaC2PKPi;   //!
   TBranch        *b_C_T_M;   //!
   TBranch        *b_C_T_MAXGHOSTPROB;   //!
   TBranch        *b_C_T_MinIP;   //!
   TBranch        *b_C_T_MinIPChi2;   //!
   TBranch        *b_C_T_P;   //!
   TBranch        *b_C_T_PT;   //!
   TBranch        *b_C_T_Phi;   //!
   TBranch        *b_C_T_SUMPT;   //!
   TBranch        *b_C_T_THETA;   //!
   TBranch        *b_C_T_e_IP;   //!
   TBranch        *b_C_T_e_MIPCHI2;   //!
   TBranch        *b_C_T_e_P;   //!
   TBranch        *b_C_T_e_PIDK;   //!
   TBranch        *b_C_T_e_PIDp;   //!
   TBranch        *b_C_T_e_PT;   //!
   TBranch        *b_C_T_e_ProbNNK;   //!
   TBranch        *b_C_T_e_ProbNNe;   //!
   TBranch        *b_C_T_e_ProbNNmu;   //!
   TBranch        *b_C_T_e_ProbNNp;   //!
   TBranch        *b_C_T_e_ProbNNpi;   //!
   TBranch        *b_C_T_mu_IP;   //!
   TBranch        *b_C_T_mu_MIPCHI2;   //!
   TBranch        *b_C_T_mu_P;   //!
   TBranch        *b_C_T_mu_PIDK;   //!
   TBranch        *b_C_T_mu_PIDp;   //!
   TBranch        *b_C_T_mu_PT;   //!
   TBranch        *b_C_T_mu_ProbNNK;   //!
   TBranch        *b_C_T_mu_ProbNNe;   //!
   TBranch        *b_C_T_mu_ProbNNmu;   //!
   TBranch        *b_C_T_mu_ProbNNp;   //!
   TBranch        *b_C_T_mu_ProbNNpi;   //!
   TBranch        *b_C_T_pi_IP;   //!
   TBranch        *b_C_T_pi_MIPCHI2;   //!
   TBranch        *b_C_T_pi_P;   //!
   TBranch        *b_C_T_pi_PIDK;   //!
   TBranch        *b_C_T_pi_PIDp;   //!
   TBranch        *b_C_T_pi_PT;   //!
   TBranch        *b_C_T_pi_ProbNNK;   //!
   TBranch        *b_C_T_pi_ProbNNp;   //!
   TBranch        *b_C_T_pi_ProbNNpi;   //!
   TBranch        *b_C_T_prot_IP;   //!
   TBranch        *b_C_T_prot_MIPCHI2;   //!
   TBranch        *b_C_T_prot_P;   //!
   TBranch        *b_C_T_prot_PIDK;   //!
   TBranch        *b_C_T_prot_PIDp;   //!
   TBranch        *b_C_T_prot_PT;   //!
   TBranch        *b_C_T_prot_ProbNNK;   //!
   TBranch        *b_C_T_prot_ProbNNp;   //!
   TBranch        *b_C_T_prot_ProbNNpi;   //!
   TBranch        *b_C_T_ACHI2DOCA;   //!
   TBranch        *b_C_T_ADOCA;   //!
   TBranch        *b_B_LOKI_ENERGY;   //!
   TBranch        *b_B_LOKI_ETA;   //!
   TBranch        *b_B_LOKI_PHI;   //!
   TBranch        *b_B_ENDVERTEX_X;   //!
   TBranch        *b_B_ENDVERTEX_Y;   //!
   TBranch        *b_B_ENDVERTEX_Z;   //!
   TBranch        *b_B_ENDVERTEX_XERR;   //!
   TBranch        *b_B_ENDVERTEX_YERR;   //!
   TBranch        *b_B_ENDVERTEX_ZERR;   //!
   TBranch        *b_B_ENDVERTEX_CHI2;   //!
   TBranch        *b_B_ENDVERTEX_NDOF;   //!
   TBranch        *b_B_ENDVERTEX_COV_;   //!
   TBranch        *b_B_OWNPV_X;   //!
   TBranch        *b_B_OWNPV_Y;   //!
   TBranch        *b_B_OWNPV_Z;   //!
   TBranch        *b_B_OWNPV_XERR;   //!
   TBranch        *b_B_OWNPV_YERR;   //!
   TBranch        *b_B_OWNPV_ZERR;   //!
   TBranch        *b_B_OWNPV_CHI2;   //!
   TBranch        *b_B_OWNPV_NDOF;   //!
   TBranch        *b_B_OWNPV_COV_;   //!
   TBranch        *b_B_IP_OWNPV;   //!
   TBranch        *b_B_IPCHI2_OWNPV;   //!
   TBranch        *b_B_FD_OWNPV;   //!
   TBranch        *b_B_FDCHI2_OWNPV;   //!
   TBranch        *b_B_DIRA_OWNPV;   //!
   TBranch        *b_N;   //!
   TBranch        *b_Tr_T_Sum_of_trackp;   //!
   TBranch        *b_Tr_T_Sum_of_trackpt;   //!
   TBranch        *b_Tr_T_Cone_asym_Pt;   //!
   TBranch        *b_Tr_T_Cone_asym_P;   //!
   TBranch        *b_Tr_T_ConIso_p_ult;   //!
   TBranch        *b_Tr_T_ConIso_pt_ult;   //!
   TBranch        *b_Tr_T_Ntr_incone;   //!
   TBranch        *b_Tr_T_Mother_VtxChi2;   //!
   TBranch        *b_Tr_T_IPCHI2_trMother;   //!
   TBranch        *b_Tr_T_IP_trMother;   //!
   TBranch        *b_Tr_T_x;   //!
   TBranch        *b_Tr_T_y;   //!
   TBranch        *b_Tr_T_xfirst;   //!
   TBranch        *b_Tr_T_yfirst;   //!
   TBranch        *b_Tr_T_zfirst;   //!
   TBranch        *b_Tr_T_SumBDT_sigtr;   //!
   TBranch        *b_Tr_T_MinBDT_sigtr;   //!
   TBranch        *b_Tr_T_NbTrNonIso_sigtr;   //!
   TBranch        *b_Tr_T_SumBDT_ult;   //!
   TBranch        *b_Tr_T_nBDT_ult;   //!
   TBranch        *b_Tr_T_NbNonIsoTr_ult;   //!
   TBranch        *b_Tr_T_Best_PAIR_VCHI2;   //!
   TBranch        *b_Tr_T_Best_PAIR_D;   //!
   TBranch        *b_Tr_T_Best_PAIR_DCHI2;   //!
   TBranch        *b_Tr_T_Best_PAIR_M_fromiso;   //!
   TBranch        *b_B_P;   //!
   TBranch        *b_B_PT;   //!
   TBranch        *b_B_PE;   //!
   TBranch        *b_B_PX;   //!
   TBranch        *b_B_PY;   //!
   TBranch        *b_B_PZ;   //!
   TBranch        *b_B_MM;   //!
   TBranch        *b_B_MMERR;   //!
   TBranch        *b_B_M;   //!
   TBranch        *b_B_TAU;   //!
   TBranch        *b_B_TAUERR;   //!
   TBranch        *b_B_TAUCHI2;   //!
   TBranch        *b_B_TAGDECISION;   //!
   TBranch        *b_B_TAGOMEGA;   //!
   TBranch        *b_B_TAGDECISION_OS;   //!
   TBranch        *b_B_TAGOMEGA_OS;   //!
   TBranch        *b_B_TAGGER;   //!
   TBranch        *b_B_OS_Muon_DEC;   //!
   TBranch        *b_B_OS_Muon_PROB;   //!
   TBranch        *b_B_OS_Electron_DEC;   //!
   TBranch        *b_B_OS_Electron_PROB;   //!
   TBranch        *b_B_OS_Kaon_DEC;   //!
   TBranch        *b_B_OS_Kaon_PROB;   //!
   TBranch        *b_B_SS_Kaon_DEC;   //!
   TBranch        *b_B_SS_Kaon_PROB;   //!
   TBranch        *b_B_SS_Pion_DEC;   //!
   TBranch        *b_B_SS_Pion_PROB;   //!
   TBranch        *b_B_SS_PionBDT_DEC;   //!
   TBranch        *b_B_SS_PionBDT_PROB;   //!
   TBranch        *b_B_VtxCharge_DEC;   //!
   TBranch        *b_B_VtxCharge_PROB;   //!
   TBranch        *b_B_OS_nnetKaon_DEC;   //!
   TBranch        *b_B_OS_nnetKaon_PROB;   //!
   TBranch        *b_B_SS_nnetKaon_DEC;   //!
   TBranch        *b_B_SS_nnetKaon_PROB;   //!
   TBranch        *b_B_SS_Proton_DEC;   //!
   TBranch        *b_B_SS_Proton_PROB;   //!
   TBranch        *b_B_OS_Charm_DEC;   //!
   TBranch        *b_B_OS_Charm_PROB;   //!
   TBranch        *b_V;   //!
   TBranch        *b_SecVtx_pt1;   //!
   TBranch        *b_SecVtx_pt2;   //!
   TBranch        *b_SecVtx_x;   //!
   TBranch        *b_SecVtx_y;   //!
   TBranch        *b_SecVtx_z;   //!
   TBranch        *b_SecVtx_zerr;   //!
   TBranch        *b_SecVtx_chi2;   //!
   TBranch        *b_BOVtx_x;   //!
   TBranch        *b_BOVtx_y;   //!
   TBranch        *b_BOVtx_z;   //!
   TBranch        *b_BOVtx_zerr;   //!
   TBranch        *b_BOVtx_chi2;   //!
   TBranch        *b_vtags;   //!
   TBranch        *b_IPSV;   //!
   TBranch        *b_IPSVerr;   //!
   TBranch        *b_DOCA;   //!
   TBranch        *b_DOCAerr;   //!
   TBranch        *b_distphi;   //!
   TBranch        *b_B_LOKI_DTF_CTAU;   //!
   TBranch        *b_B_LOKI_MASS_JpsiConstr;   //!
   TBranch        *b_J_psi_1S_len;   //!
   TBranch        *b_J_psi_1S_LOKI_ENERGY;   //!
   TBranch        *b_J_psi_1S_LOKI_ETA;   //!
   TBranch        *b_J_psi_1S_LOKI_PHI;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_X;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_Y;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_Z;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_XERR;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_YERR;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_ZERR;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_CHI2;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_NDOF;   //!
   TBranch        *b_J_psi_1S_ENDVERTEX_COV_;   //!
   TBranch        *b_J_psi_1S_OWNPV_X;   //!
   TBranch        *b_J_psi_1S_OWNPV_Y;   //!
   TBranch        *b_J_psi_1S_OWNPV_Z;   //!
   TBranch        *b_J_psi_1S_OWNPV_XERR;   //!
   TBranch        *b_J_psi_1S_OWNPV_YERR;   //!
   TBranch        *b_J_psi_1S_OWNPV_ZERR;   //!
   TBranch        *b_J_psi_1S_OWNPV_CHI2;   //!
   TBranch        *b_J_psi_1S_OWNPV_NDOF;   //!
   TBranch        *b_J_psi_1S_OWNPV_COV_;   //!
   TBranch        *b_J_psi_1S_IP_OWNPV;   //!
   TBranch        *b_J_psi_1S_IPCHI2_OWNPV;   //!
   TBranch        *b_J_psi_1S_FD_OWNPV;   //!
   TBranch        *b_J_psi_1S_FDCHI2_OWNPV;   //!
   TBranch        *b_J_psi_1S_DIRA_OWNPV;   //!
   TBranch        *b_J_psi_1S_ORIVX_X;   //!
   TBranch        *b_J_psi_1S_ORIVX_Y;   //!
   TBranch        *b_J_psi_1S_ORIVX_Z;   //!
   TBranch        *b_J_psi_1S_ORIVX_XERR;   //!
   TBranch        *b_J_psi_1S_ORIVX_YERR;   //!
   TBranch        *b_J_psi_1S_ORIVX_ZERR;   //!
   TBranch        *b_J_psi_1S_ORIVX_CHI2;   //!
   TBranch        *b_J_psi_1S_ORIVX_NDOF;   //!
   TBranch        *b_J_psi_1S_ORIVX_COV_;   //!
   TBranch        *b_J_psi_1S_FD_ORIVX;   //!
   TBranch        *b_J_psi_1S_FDCHI2_ORIVX;   //!
   TBranch        *b_J_psi_1S_DIRA_ORIVX;   //!
   TBranch        *b_J_psi_1S_P;   //!
   TBranch        *b_J_psi_1S_PT;   //!
   TBranch        *b_J_psi_1S_PE;   //!
   TBranch        *b_J_psi_1S_PX;   //!
   TBranch        *b_J_psi_1S_PY;   //!
   TBranch        *b_J_psi_1S_PZ;   //!
   TBranch        *b_J_psi_1S_MM;   //!
   TBranch        *b_J_psi_1S_MMERR;   //!
   TBranch        *b_J_psi_1S_M;   //!
   TBranch        *b_J_psi_1S_TAU;   //!
   TBranch        *b_J_psi_1S_TAUERR;   //!
   TBranch        *b_J_psi_1S_TAUCHI2;   //!
   TBranch        *b_muplus_len;   //!
   TBranch        *b_Tr_T_BPVIP;   //!
   TBranch        *b_Tr_T_BPVIPCHI2;   //!
   TBranch        *b_Tr_T_Charge;   //!
   TBranch        *b_Tr_T_E;   //!
   TBranch        *b_Tr_T_EcalE;   //!
   TBranch        *b_Tr_T_Eta;   //!
   TBranch        *b_Tr_T_HASMUON;   //!
   TBranch        *b_Tr_T_HASRICH;   //!
   TBranch        *b_Tr_T_HcalE;   //!
   TBranch        *b_Tr_T_ISMUON;   //!
   TBranch        *b_Tr_T_MinIP;   //!
   TBranch        *b_Tr_T_MinIPChi2;   //!
   TBranch        *b_Tr_T_NSHARED;   //!
   TBranch        *b_Tr_T_P;   //!
   TBranch        *b_Tr_T_PIDK;   //!
   TBranch        *b_Tr_T_PIDe;   //!
   TBranch        *b_Tr_T_PIDmu;   //!
   TBranch        *b_Tr_T_PIDp;   //!
   TBranch        *b_Tr_T_PROBNNe;   //!
   TBranch        *b_Tr_T_PROBNNghost;   //!
   TBranch        *b_Tr_T_PROBNNk;   //!
   TBranch        *b_Tr_T_PROBNNmu;   //!
   TBranch        *b_Tr_T_PROBNNp;   //!
   TBranch        *b_Tr_T_PROBNNpi;   //!
   TBranch        *b_Tr_T_PT;   //!
   TBranch        *b_Tr_T_Phi;   //!
   TBranch        *b_Tr_T_PrsE;   //!
   TBranch        *b_Tr_T_THETA;   //!
   TBranch        *b_Tr_T_TRCHI2DOF;   //!
   TBranch        *b_Tr_T_TRCLONEDIST;   //!
   TBranch        *b_Tr_T_TRFITMATCHCHI2;   //!
   TBranch        *b_Tr_T_TRFITTCHI2;   //!
   TBranch        *b_Tr_T_TRFITVELOCHI2NDOF;   //!
   TBranch        *b_Tr_T_TRGHOSTPROB;   //!
   TBranch        *b_Tr_T_TRPCHI2;   //!
   TBranch        *b_Tr_T_TRTYPE;   //!
   TBranch        *b_Tr_T_TrFIRSTHITZ;   //!
   TBranch        *b_Tr_T_TrFITTCHI2NDOF;   //!
   TBranch        *b_Tr_T_ACHI2DOCA;   //!
   TBranch        *b_Tr_T_ADOCA;   //!
   TBranch        *b_muplus_LOKI_ENERGY;   //!
   TBranch        *b_muplus_LOKI_ETA;   //!
   TBranch        *b_muplus_LOKI_PHI;   //!
   TBranch        *b_muplus_OWNPV_X;   //!
   TBranch        *b_muplus_OWNPV_Y;   //!
   TBranch        *b_muplus_OWNPV_Z;   //!
   TBranch        *b_muplus_OWNPV_XERR;   //!
   TBranch        *b_muplus_OWNPV_YERR;   //!
   TBranch        *b_muplus_OWNPV_ZERR;   //!
   TBranch        *b_muplus_OWNPV_CHI2;   //!
   TBranch        *b_muplus_OWNPV_NDOF;   //!
   TBranch        *b_muplus_OWNPV_COV_;   //!
   TBranch        *b_muplus_IP_OWNPV;   //!
   TBranch        *b_muplus_IPCHI2_OWNPV;   //!
   TBranch        *b_muplus_ORIVX_X;   //!
   TBranch        *b_muplus_ORIVX_Y;   //!
   TBranch        *b_muplus_ORIVX_Z;   //!
   TBranch        *b_muplus_ORIVX_XERR;   //!
   TBranch        *b_muplus_ORIVX_YERR;   //!
   TBranch        *b_muplus_ORIVX_ZERR;   //!
   TBranch        *b_muplus_ORIVX_CHI2;   //!
   TBranch        *b_muplus_ORIVX_NDOF;   //!
   TBranch        *b_muplus_ORIVX_COV_;   //!
   TBranch        *b_muplus_P;   //!
   TBranch        *b_muplus_PT;   //!
   TBranch        *b_muplus_PE;   //!
   TBranch        *b_muplus_PX;   //!
   TBranch        *b_muplus_PY;   //!
   TBranch        *b_muplus_PZ;   //!
   TBranch        *b_muplus_M;   //!
   TBranch        *b_muminus_len;   //!
   TBranch        *b_muminus_LOKI_ENERGY;   //!
   TBranch        *b_muminus_LOKI_ETA;   //!
   TBranch        *b_muminus_LOKI_PHI;   //!
   TBranch        *b_muminus_OWNPV_X;   //!
   TBranch        *b_muminus_OWNPV_Y;   //!
   TBranch        *b_muminus_OWNPV_Z;   //!
   TBranch        *b_muminus_OWNPV_XERR;   //!
   TBranch        *b_muminus_OWNPV_YERR;   //!
   TBranch        *b_muminus_OWNPV_ZERR;   //!
   TBranch        *b_muminus_OWNPV_CHI2;   //!
   TBranch        *b_muminus_OWNPV_NDOF;   //!
   TBranch        *b_muminus_OWNPV_COV_;   //!
   TBranch        *b_muminus_IP_OWNPV;   //!
   TBranch        *b_muminus_IPCHI2_OWNPV;   //!
   TBranch        *b_muminus_ORIVX_X;   //!
   TBranch        *b_muminus_ORIVX_Y;   //!
   TBranch        *b_muminus_ORIVX_Z;   //!
   TBranch        *b_muminus_ORIVX_XERR;   //!
   TBranch        *b_muminus_ORIVX_YERR;   //!
   TBranch        *b_muminus_ORIVX_ZERR;   //!
   TBranch        *b_muminus_ORIVX_CHI2;   //!
   TBranch        *b_muminus_ORIVX_NDOF;   //!
   TBranch        *b_muminus_ORIVX_COV_;   //!
   TBranch        *b_muminus_P;   //!
   TBranch        *b_muminus_PT;   //!
   TBranch        *b_muminus_PE;   //!
   TBranch        *b_muminus_PX;   //!
   TBranch        *b_muminus_PY;   //!
   TBranch        *b_muminus_PZ;   //!
   TBranch        *b_muminus_M;   //!
   TBranch        *b_Kplus_len;   //!
   TBranch        *b_Kplus_LOKI_ENERGY;   //!
   TBranch        *b_Kplus_LOKI_ETA;   //!
   TBranch        *b_Kplus_LOKI_PHI;   //!
   TBranch        *b_Kplus_OWNPV_X;   //!
   TBranch        *b_Kplus_OWNPV_Y;   //!
   TBranch        *b_Kplus_OWNPV_Z;   //!
   TBranch        *b_Kplus_OWNPV_XERR;   //!
   TBranch        *b_Kplus_OWNPV_YERR;   //!
   TBranch        *b_Kplus_OWNPV_ZERR;   //!
   TBranch        *b_Kplus_OWNPV_CHI2;   //!
   TBranch        *b_Kplus_OWNPV_NDOF;   //!
   TBranch        *b_Kplus_OWNPV_COV_;   //!
   TBranch        *b_Kplus_IP_OWNPV;   //!
   TBranch        *b_Kplus_IPCHI2_OWNPV;   //!
   TBranch        *b_Kplus_ORIVX_X;   //!
   TBranch        *b_Kplus_ORIVX_Y;   //!
   TBranch        *b_Kplus_ORIVX_Z;   //!
   TBranch        *b_Kplus_ORIVX_XERR;   //!
   TBranch        *b_Kplus_ORIVX_YERR;   //!
   TBranch        *b_Kplus_ORIVX_ZERR;   //!
   TBranch        *b_Kplus_ORIVX_CHI2;   //!
   TBranch        *b_Kplus_ORIVX_NDOF;   //!
   TBranch        *b_Kplus_ORIVX_COV_;   //!
   TBranch        *b_Kplus_P;   //!
   TBranch        *b_Kplus_PT;   //!
   TBranch        *b_Kplus_PE;   //!
   TBranch        *b_Kplus_PX;   //!
   TBranch        *b_Kplus_PY;   //!
   TBranch        *b_Kplus_PZ;   //!
   TBranch        *b_Kplus_M;   //!
   TBranch        *b_nCandidate;   //!
   TBranch        *b_totCandidates;   //!
   TBranch        *b_EventInSequence;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_BCType;   //!
   TBranch        *b_OdinTCK;   //!
   TBranch        *b_L0DUTCK;   //!
   TBranch        *b_HLT1TCK;   //!
   TBranch        *b_HLT2TCK;   //!
   TBranch        *b_GpsTime;   //!
   TBranch        *b_Polarity;   //!
   TBranch        *b_nPV;   //!
   TBranch        *b_PVX;   //!
   TBranch        *b_PVY;   //!
   TBranch        *b_PVZ;   //!
   TBranch        *b_PVXERR;   //!
   TBranch        *b_PVYERR;   //!
   TBranch        *b_PVZERR;   //!
   TBranch        *b_PVCHI2;   //!
   TBranch        *b_PVNDOF;   //!
   TBranch        *b_PVNTRACKS;   //!
   TBranch        *b_StrippingBetaSBs2JpsiPhiDetachedLineDecision;   //!
   TBranch        *b_StrippingBetaSBd2JpsiKstarDetachedLineDecision;   //!
   TBranch        *b_StrippingBetaSBu2JpsiKDetachedLineDecision;   //!

   DecayTree(TTree *tree=0);
   virtual ~DecayTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef DecayTree_cxx
DecayTree::DecayTree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("DTT_B2JpsiX_ALTR.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("DTT_B2JpsiX_ALTR.root");
      }
      TDirectory * dir = (TDirectory*)f->Get("DTT_B2JpsiX_ALTR.root:/Bu2JpsiKDetached");
      dir->GetObject("DecayTree",tree);

   }
   Init(tree);
}

DecayTree::~DecayTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t DecayTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t DecayTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void DecayTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("B_len", &B_len, &b_B_len);
   fChain->SetBranchAddress("C_T_BPVIP", C_T_BPVIP, &b_C_T_BPVIP);
   fChain->SetBranchAddress("C_T_BPVIPCHI2", C_T_BPVIPCHI2, &b_C_T_BPVIPCHI2);
   fChain->SetBranchAddress("C_T_CORRM", C_T_CORRM, &b_C_T_CORRM);
   fChain->SetBranchAddress("C_T_Charge", C_T_Charge, &b_C_T_Charge);
   fChain->SetBranchAddress("C_T_Costheta_star1", C_T_Costheta_star1, &b_C_T_Costheta_star1);
   fChain->SetBranchAddress("C_T_Costheta_star2", C_T_Costheta_star2, &b_C_T_Costheta_star2);
   fChain->SetBranchAddress("C_T_D02KPi", C_T_D02KPi, &b_C_T_D02KPi);
   fChain->SetBranchAddress("C_T_D02KPiPi0", C_T_D02KPiPi0, &b_C_T_D02KPiPi0);
   fChain->SetBranchAddress("C_T_D02KPiPiPi", C_T_D02KPiPiPi, &b_C_T_D02KPiPiPi);
   fChain->SetBranchAddress("C_T_D2KPipart", C_T_D2KPipart, &b_C_T_D2KPipart);
   fChain->SetBranchAddress("C_T_D2Kepart", C_T_D2Kepart, &b_C_T_D2Kepart);
   fChain->SetBranchAddress("C_T_D2Kmupart", C_T_D2Kmupart, &b_C_T_D2Kmupart);
   fChain->SetBranchAddress("C_T_DIRA", C_T_DIRA, &b_C_T_DIRA);
   fChain->SetBranchAddress("C_T_DOCACHI2MAX", C_T_DOCACHI2MAX, &b_C_T_DOCACHI2MAX);
   fChain->SetBranchAddress("C_T_DOCAMAX", C_T_DOCAMAX, &b_C_T_DOCAMAX);
   fChain->SetBranchAddress("C_T_DTF_CTAUERR", C_T_DTF_CTAUERR, &b_C_T_DTF_CTAUERR);
   fChain->SetBranchAddress("C_T_DTF_CTAUS", C_T_DTF_CTAUS, &b_C_T_DTF_CTAUS);
   fChain->SetBranchAddress("C_T_DTF_TAU", C_T_DTF_TAU, &b_C_T_DTF_TAU);
   fChain->SetBranchAddress("C_T_DTF_VCHI2NDOF", C_T_DTF_VCHI2NDOF, &b_C_T_DTF_VCHI2NDOF);
   fChain->SetBranchAddress("C_T_Dp2KPiPi", C_T_Dp2KPiPi, &b_C_T_Dp2KPiPi);
   fChain->SetBranchAddress("C_T_E", C_T_E, &b_C_T_E);
   fChain->SetBranchAddress("C_T_Eta", C_T_Eta, &b_C_T_Eta);
   fChain->SetBranchAddress("C_T_FD", C_T_FD, &b_C_T_FD);
   fChain->SetBranchAddress("C_T_FDCHI2", C_T_FDCHI2, &b_C_T_FDCHI2);
   fChain->SetBranchAddress("C_T_FDS", C_T_FDS, &b_C_T_FDS);
   fChain->SetBranchAddress("C_T_K_IP", C_T_K_IP, &b_C_T_K_IP);
   fChain->SetBranchAddress("C_T_K_MIPCHI2", C_T_K_MIPCHI2, &b_C_T_K_MIPCHI2);
   fChain->SetBranchAddress("C_T_K_P", C_T_K_P, &b_C_T_K_P);
   fChain->SetBranchAddress("C_T_K_PIDK", C_T_K_PIDK, &b_C_T_K_PIDK);
   fChain->SetBranchAddress("C_T_K_PIDp", C_T_K_PIDp, &b_C_T_K_PIDp);
   fChain->SetBranchAddress("C_T_K_PT", C_T_K_PT, &b_C_T_K_PT);
   fChain->SetBranchAddress("C_T_K_ProbNNK", C_T_K_ProbNNK, &b_C_T_K_ProbNNK);
   fChain->SetBranchAddress("C_T_K_ProbNNp", C_T_K_ProbNNp, &b_C_T_K_ProbNNp);
   fChain->SetBranchAddress("C_T_K_ProbNNpi", C_T_K_ProbNNpi, &b_C_T_K_ProbNNpi);
   fChain->SetBranchAddress("C_T_LambdaC2PKPi", C_T_LambdaC2PKPi, &b_C_T_LambdaC2PKPi);
   fChain->SetBranchAddress("C_T_M", C_T_M, &b_C_T_M);
   fChain->SetBranchAddress("C_T_MAXGHOSTPROB", C_T_MAXGHOSTPROB, &b_C_T_MAXGHOSTPROB);
   fChain->SetBranchAddress("C_T_MinIP", C_T_MinIP, &b_C_T_MinIP);
   fChain->SetBranchAddress("C_T_MinIPChi2", C_T_MinIPChi2, &b_C_T_MinIPChi2);
   fChain->SetBranchAddress("C_T_P", C_T_P, &b_C_T_P);
   fChain->SetBranchAddress("C_T_PT", C_T_PT, &b_C_T_PT);
   fChain->SetBranchAddress("C_T_Phi", C_T_Phi, &b_C_T_Phi);
   fChain->SetBranchAddress("C_T_SUMPT", C_T_SUMPT, &b_C_T_SUMPT);
   fChain->SetBranchAddress("C_T_THETA", C_T_THETA, &b_C_T_THETA);
   fChain->SetBranchAddress("C_T_e_IP", C_T_e_IP, &b_C_T_e_IP);
   fChain->SetBranchAddress("C_T_e_MIPCHI2", C_T_e_MIPCHI2, &b_C_T_e_MIPCHI2);
   fChain->SetBranchAddress("C_T_e_P", C_T_e_P, &b_C_T_e_P);
   fChain->SetBranchAddress("C_T_e_PIDK", C_T_e_PIDK, &b_C_T_e_PIDK);
   fChain->SetBranchAddress("C_T_e_PIDp", C_T_e_PIDp, &b_C_T_e_PIDp);
   fChain->SetBranchAddress("C_T_e_PT", C_T_e_PT, &b_C_T_e_PT);
   fChain->SetBranchAddress("C_T_e_ProbNNK", C_T_e_ProbNNK, &b_C_T_e_ProbNNK);
   fChain->SetBranchAddress("C_T_e_ProbNNe", C_T_e_ProbNNe, &b_C_T_e_ProbNNe);
   fChain->SetBranchAddress("C_T_e_ProbNNmu", C_T_e_ProbNNmu, &b_C_T_e_ProbNNmu);
   fChain->SetBranchAddress("C_T_e_ProbNNp", C_T_e_ProbNNp, &b_C_T_e_ProbNNp);
   fChain->SetBranchAddress("C_T_e_ProbNNpi", C_T_e_ProbNNpi, &b_C_T_e_ProbNNpi);
   fChain->SetBranchAddress("C_T_mu_IP", C_T_mu_IP, &b_C_T_mu_IP);
   fChain->SetBranchAddress("C_T_mu_MIPCHI2", C_T_mu_MIPCHI2, &b_C_T_mu_MIPCHI2);
   fChain->SetBranchAddress("C_T_mu_P", C_T_mu_P, &b_C_T_mu_P);
   fChain->SetBranchAddress("C_T_mu_PIDK", C_T_mu_PIDK, &b_C_T_mu_PIDK);
   fChain->SetBranchAddress("C_T_mu_PIDp", C_T_mu_PIDp, &b_C_T_mu_PIDp);
   fChain->SetBranchAddress("C_T_mu_PT", C_T_mu_PT, &b_C_T_mu_PT);
   fChain->SetBranchAddress("C_T_mu_ProbNNK", C_T_mu_ProbNNK, &b_C_T_mu_ProbNNK);
   fChain->SetBranchAddress("C_T_mu_ProbNNe", C_T_mu_ProbNNe, &b_C_T_mu_ProbNNe);
   fChain->SetBranchAddress("C_T_mu_ProbNNmu", C_T_mu_ProbNNmu, &b_C_T_mu_ProbNNmu);
   fChain->SetBranchAddress("C_T_mu_ProbNNp", C_T_mu_ProbNNp, &b_C_T_mu_ProbNNp);
   fChain->SetBranchAddress("C_T_mu_ProbNNpi", C_T_mu_ProbNNpi, &b_C_T_mu_ProbNNpi);
   fChain->SetBranchAddress("C_T_pi_IP", C_T_pi_IP, &b_C_T_pi_IP);
   fChain->SetBranchAddress("C_T_pi_MIPCHI2", C_T_pi_MIPCHI2, &b_C_T_pi_MIPCHI2);
   fChain->SetBranchAddress("C_T_pi_P", C_T_pi_P, &b_C_T_pi_P);
   fChain->SetBranchAddress("C_T_pi_PIDK", C_T_pi_PIDK, &b_C_T_pi_PIDK);
   fChain->SetBranchAddress("C_T_pi_PIDp", C_T_pi_PIDp, &b_C_T_pi_PIDp);
   fChain->SetBranchAddress("C_T_pi_PT", C_T_pi_PT, &b_C_T_pi_PT);
   fChain->SetBranchAddress("C_T_pi_ProbNNK", C_T_pi_ProbNNK, &b_C_T_pi_ProbNNK);
   fChain->SetBranchAddress("C_T_pi_ProbNNp", C_T_pi_ProbNNp, &b_C_T_pi_ProbNNp);
   fChain->SetBranchAddress("C_T_pi_ProbNNpi", C_T_pi_ProbNNpi, &b_C_T_pi_ProbNNpi);
   fChain->SetBranchAddress("C_T_prot_IP", C_T_prot_IP, &b_C_T_prot_IP);
   fChain->SetBranchAddress("C_T_prot_MIPCHI2", C_T_prot_MIPCHI2, &b_C_T_prot_MIPCHI2);
   fChain->SetBranchAddress("C_T_prot_P", C_T_prot_P, &b_C_T_prot_P);
   fChain->SetBranchAddress("C_T_prot_PIDK", C_T_prot_PIDK, &b_C_T_prot_PIDK);
   fChain->SetBranchAddress("C_T_prot_PIDp", C_T_prot_PIDp, &b_C_T_prot_PIDp);
   fChain->SetBranchAddress("C_T_prot_PT", C_T_prot_PT, &b_C_T_prot_PT);
   fChain->SetBranchAddress("C_T_prot_ProbNNK", C_T_prot_ProbNNK, &b_C_T_prot_ProbNNK);
   fChain->SetBranchAddress("C_T_prot_ProbNNp", C_T_prot_ProbNNp, &b_C_T_prot_ProbNNp);
   fChain->SetBranchAddress("C_T_prot_ProbNNpi", C_T_prot_ProbNNpi, &b_C_T_prot_ProbNNpi);
   fChain->SetBranchAddress("C_T_ACHI2DOCA", C_T_ACHI2DOCA, &b_C_T_ACHI2DOCA);
   fChain->SetBranchAddress("C_T_ADOCA", C_T_ADOCA, &b_C_T_ADOCA);
   fChain->SetBranchAddress("B_LOKI_ENERGY", &B_LOKI_ENERGY, &b_B_LOKI_ENERGY);
   fChain->SetBranchAddress("B_LOKI_ETA", &B_LOKI_ETA, &b_B_LOKI_ETA);
   fChain->SetBranchAddress("B_LOKI_PHI", &B_LOKI_PHI, &b_B_LOKI_PHI);
   fChain->SetBranchAddress("B_ENDVERTEX_X", &B_ENDVERTEX_X, &b_B_ENDVERTEX_X);
   fChain->SetBranchAddress("B_ENDVERTEX_Y", &B_ENDVERTEX_Y, &b_B_ENDVERTEX_Y);
   fChain->SetBranchAddress("B_ENDVERTEX_Z", &B_ENDVERTEX_Z, &b_B_ENDVERTEX_Z);
   fChain->SetBranchAddress("B_ENDVERTEX_XERR", &B_ENDVERTEX_XERR, &b_B_ENDVERTEX_XERR);
   fChain->SetBranchAddress("B_ENDVERTEX_YERR", &B_ENDVERTEX_YERR, &b_B_ENDVERTEX_YERR);
   fChain->SetBranchAddress("B_ENDVERTEX_ZERR", &B_ENDVERTEX_ZERR, &b_B_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("B_ENDVERTEX_CHI2", &B_ENDVERTEX_CHI2, &b_B_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("B_ENDVERTEX_NDOF", &B_ENDVERTEX_NDOF, &b_B_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("B_ENDVERTEX_COV_", B_ENDVERTEX_COV_, &b_B_ENDVERTEX_COV_);
   fChain->SetBranchAddress("B_OWNPV_X", &B_OWNPV_X, &b_B_OWNPV_X);
   fChain->SetBranchAddress("B_OWNPV_Y", &B_OWNPV_Y, &b_B_OWNPV_Y);
   fChain->SetBranchAddress("B_OWNPV_Z", &B_OWNPV_Z, &b_B_OWNPV_Z);
   fChain->SetBranchAddress("B_OWNPV_XERR", &B_OWNPV_XERR, &b_B_OWNPV_XERR);
   fChain->SetBranchAddress("B_OWNPV_YERR", &B_OWNPV_YERR, &b_B_OWNPV_YERR);
   fChain->SetBranchAddress("B_OWNPV_ZERR", &B_OWNPV_ZERR, &b_B_OWNPV_ZERR);
   fChain->SetBranchAddress("B_OWNPV_CHI2", &B_OWNPV_CHI2, &b_B_OWNPV_CHI2);
   fChain->SetBranchAddress("B_OWNPV_NDOF", &B_OWNPV_NDOF, &b_B_OWNPV_NDOF);
   fChain->SetBranchAddress("B_OWNPV_COV_", B_OWNPV_COV_, &b_B_OWNPV_COV_);
   fChain->SetBranchAddress("B_IP_OWNPV", &B_IP_OWNPV, &b_B_IP_OWNPV);
   fChain->SetBranchAddress("B_IPCHI2_OWNPV", &B_IPCHI2_OWNPV, &b_B_IPCHI2_OWNPV);
   fChain->SetBranchAddress("B_FD_OWNPV", &B_FD_OWNPV, &b_B_FD_OWNPV);
   fChain->SetBranchAddress("B_FDCHI2_OWNPV", &B_FDCHI2_OWNPV, &b_B_FDCHI2_OWNPV);
   fChain->SetBranchAddress("B_DIRA_OWNPV", &B_DIRA_OWNPV, &b_B_DIRA_OWNPV);
   fChain->SetBranchAddress("N", &N, &b_N);
   fChain->SetBranchAddress("Tr_T_Sum_of_trackp", Tr_T_Sum_of_trackp, &b_Tr_T_Sum_of_trackp);
   fChain->SetBranchAddress("Tr_T_Sum_of_trackpt", Tr_T_Sum_of_trackpt, &b_Tr_T_Sum_of_trackpt);
   fChain->SetBranchAddress("Tr_T_Cone_asym_Pt", Tr_T_Cone_asym_Pt, &b_Tr_T_Cone_asym_Pt);
   fChain->SetBranchAddress("Tr_T_Cone_asym_P", Tr_T_Cone_asym_P, &b_Tr_T_Cone_asym_P);
   fChain->SetBranchAddress("Tr_T_ConIso_p_ult", Tr_T_ConIso_p_ult, &b_Tr_T_ConIso_p_ult);
   fChain->SetBranchAddress("Tr_T_ConIso_pt_ult", Tr_T_ConIso_pt_ult, &b_Tr_T_ConIso_pt_ult);
   fChain->SetBranchAddress("Tr_T_Ntr_incone", Tr_T_Ntr_incone, &b_Tr_T_Ntr_incone);
   fChain->SetBranchAddress("Tr_T_Mother_VtxChi2", Tr_T_Mother_VtxChi2, &b_Tr_T_Mother_VtxChi2);
   fChain->SetBranchAddress("Tr_T_IPCHI2_trMother", Tr_T_IPCHI2_trMother, &b_Tr_T_IPCHI2_trMother);
   fChain->SetBranchAddress("Tr_T_IP_trMother", Tr_T_IP_trMother, &b_Tr_T_IP_trMother);
   fChain->SetBranchAddress("Tr_T_x", Tr_T_x, &b_Tr_T_x);
   fChain->SetBranchAddress("Tr_T_y", Tr_T_y, &b_Tr_T_y);
   fChain->SetBranchAddress("Tr_T_xfirst", Tr_T_xfirst, &b_Tr_T_xfirst);
   fChain->SetBranchAddress("Tr_T_yfirst", Tr_T_yfirst, &b_Tr_T_yfirst);
   fChain->SetBranchAddress("Tr_T_zfirst", Tr_T_zfirst, &b_Tr_T_zfirst);
   fChain->SetBranchAddress("Tr_T_SumBDT_sigtr", Tr_T_SumBDT_sigtr, &b_Tr_T_SumBDT_sigtr);
   fChain->SetBranchAddress("Tr_T_MinBDT_sigtr", Tr_T_MinBDT_sigtr, &b_Tr_T_MinBDT_sigtr);
   fChain->SetBranchAddress("Tr_T_NbTrNonIso_sigtr", Tr_T_NbTrNonIso_sigtr, &b_Tr_T_NbTrNonIso_sigtr);
   fChain->SetBranchAddress("Tr_T_SumBDT_ult", Tr_T_SumBDT_ult, &b_Tr_T_SumBDT_ult);
   fChain->SetBranchAddress("Tr_T_nBDT_ult", Tr_T_nBDT_ult, &b_Tr_T_nBDT_ult);
   fChain->SetBranchAddress("Tr_T_NbNonIsoTr_ult", Tr_T_NbNonIsoTr_ult, &b_Tr_T_NbNonIsoTr_ult);
   fChain->SetBranchAddress("Tr_T_Best_PAIR_VCHI2", Tr_T_Best_PAIR_VCHI2, &b_Tr_T_Best_PAIR_VCHI2);
   fChain->SetBranchAddress("Tr_T_Best_PAIR_D", Tr_T_Best_PAIR_D, &b_Tr_T_Best_PAIR_D);
   fChain->SetBranchAddress("Tr_T_Best_PAIR_DCHI2", Tr_T_Best_PAIR_DCHI2, &b_Tr_T_Best_PAIR_DCHI2);
   fChain->SetBranchAddress("Tr_T_Best_PAIR_M_fromiso", Tr_T_Best_PAIR_M_fromiso, &b_Tr_T_Best_PAIR_M_fromiso);
   fChain->SetBranchAddress("B_P", &B_P, &b_B_P);
   fChain->SetBranchAddress("B_PT", &B_PT, &b_B_PT);
   fChain->SetBranchAddress("B_PE", &B_PE, &b_B_PE);
   fChain->SetBranchAddress("B_PX", &B_PX, &b_B_PX);
   fChain->SetBranchAddress("B_PY", &B_PY, &b_B_PY);
   fChain->SetBranchAddress("B_PZ", &B_PZ, &b_B_PZ);
   fChain->SetBranchAddress("B_MM", &B_MM, &b_B_MM);
   fChain->SetBranchAddress("B_MMERR", &B_MMERR, &b_B_MMERR);
   fChain->SetBranchAddress("B_M", &B_M, &b_B_M);
   fChain->SetBranchAddress("B_TAU", &B_TAU, &b_B_TAU);
   fChain->SetBranchAddress("B_TAUERR", &B_TAUERR, &b_B_TAUERR);
   fChain->SetBranchAddress("B_TAUCHI2", &B_TAUCHI2, &b_B_TAUCHI2);
   fChain->SetBranchAddress("B_TAGDECISION", &B_TAGDECISION, &b_B_TAGDECISION);
   fChain->SetBranchAddress("B_TAGOMEGA", &B_TAGOMEGA, &b_B_TAGOMEGA);
   fChain->SetBranchAddress("B_TAGDECISION_OS", &B_TAGDECISION_OS, &b_B_TAGDECISION_OS);
   fChain->SetBranchAddress("B_TAGOMEGA_OS", &B_TAGOMEGA_OS, &b_B_TAGOMEGA_OS);
   fChain->SetBranchAddress("B_TAGGER", &B_TAGGER, &b_B_TAGGER);
   fChain->SetBranchAddress("B_OS_Muon_DEC", &B_OS_Muon_DEC, &b_B_OS_Muon_DEC);
   fChain->SetBranchAddress("B_OS_Muon_PROB", &B_OS_Muon_PROB, &b_B_OS_Muon_PROB);
   fChain->SetBranchAddress("B_OS_Electron_DEC", &B_OS_Electron_DEC, &b_B_OS_Electron_DEC);
   fChain->SetBranchAddress("B_OS_Electron_PROB", &B_OS_Electron_PROB, &b_B_OS_Electron_PROB);
   fChain->SetBranchAddress("B_OS_Kaon_DEC", &B_OS_Kaon_DEC, &b_B_OS_Kaon_DEC);
   fChain->SetBranchAddress("B_OS_Kaon_PROB", &B_OS_Kaon_PROB, &b_B_OS_Kaon_PROB);
   fChain->SetBranchAddress("B_SS_Kaon_DEC", &B_SS_Kaon_DEC, &b_B_SS_Kaon_DEC);
   fChain->SetBranchAddress("B_SS_Kaon_PROB", &B_SS_Kaon_PROB, &b_B_SS_Kaon_PROB);
   fChain->SetBranchAddress("B_SS_Pion_DEC", &B_SS_Pion_DEC, &b_B_SS_Pion_DEC);
   fChain->SetBranchAddress("B_SS_Pion_PROB", &B_SS_Pion_PROB, &b_B_SS_Pion_PROB);
   fChain->SetBranchAddress("B_SS_PionBDT_DEC", &B_SS_PionBDT_DEC, &b_B_SS_PionBDT_DEC);
   fChain->SetBranchAddress("B_SS_PionBDT_PROB", &B_SS_PionBDT_PROB, &b_B_SS_PionBDT_PROB);
   fChain->SetBranchAddress("B_VtxCharge_DEC", &B_VtxCharge_DEC, &b_B_VtxCharge_DEC);
   fChain->SetBranchAddress("B_VtxCharge_PROB", &B_VtxCharge_PROB, &b_B_VtxCharge_PROB);
   fChain->SetBranchAddress("B_OS_nnetKaon_DEC", &B_OS_nnetKaon_DEC, &b_B_OS_nnetKaon_DEC);
   fChain->SetBranchAddress("B_OS_nnetKaon_PROB", &B_OS_nnetKaon_PROB, &b_B_OS_nnetKaon_PROB);
   fChain->SetBranchAddress("B_SS_nnetKaon_DEC", &B_SS_nnetKaon_DEC, &b_B_SS_nnetKaon_DEC);
   fChain->SetBranchAddress("B_SS_nnetKaon_PROB", &B_SS_nnetKaon_PROB, &b_B_SS_nnetKaon_PROB);
   fChain->SetBranchAddress("B_SS_Proton_DEC", &B_SS_Proton_DEC, &b_B_SS_Proton_DEC);
   fChain->SetBranchAddress("B_SS_Proton_PROB", &B_SS_Proton_PROB, &b_B_SS_Proton_PROB);
   fChain->SetBranchAddress("B_OS_Charm_DEC", &B_OS_Charm_DEC, &b_B_OS_Charm_DEC);
   fChain->SetBranchAddress("B_OS_Charm_PROB", &B_OS_Charm_PROB, &b_B_OS_Charm_PROB);
   fChain->SetBranchAddress("V", &V, &b_V);
   fChain->SetBranchAddress("SecVtx_pt1", SecVtx_pt1, &b_SecVtx_pt1);
   fChain->SetBranchAddress("SecVtx_pt2", SecVtx_pt2, &b_SecVtx_pt2);
   fChain->SetBranchAddress("SecVtx_x", SecVtx_x, &b_SecVtx_x);
   fChain->SetBranchAddress("SecVtx_y", SecVtx_y, &b_SecVtx_y);
   fChain->SetBranchAddress("SecVtx_z", SecVtx_z, &b_SecVtx_z);
   fChain->SetBranchAddress("SecVtx_zerr", SecVtx_zerr, &b_SecVtx_zerr);
   fChain->SetBranchAddress("SecVtx_chi2", SecVtx_chi2, &b_SecVtx_chi2);
   fChain->SetBranchAddress("BOVtx_x", BOVtx_x, &b_BOVtx_x);
   fChain->SetBranchAddress("BOVtx_y", BOVtx_y, &b_BOVtx_y);
   fChain->SetBranchAddress("BOVtx_z", BOVtx_z, &b_BOVtx_z);
   fChain->SetBranchAddress("BOVtx_zerr", BOVtx_zerr, &b_BOVtx_zerr);
   fChain->SetBranchAddress("BOVtx_chi2", BOVtx_chi2, &b_BOVtx_chi2);
   fChain->SetBranchAddress("vtags", &vtags, &b_vtags);
   fChain->SetBranchAddress("IPSV", IPSV, &b_IPSV);
   fChain->SetBranchAddress("IPSVerr", IPSVerr, &b_IPSVerr);
   fChain->SetBranchAddress("DOCA", DOCA, &b_DOCA);
   fChain->SetBranchAddress("DOCAerr", DOCAerr, &b_DOCAerr);
   fChain->SetBranchAddress("distphi", distphi, &b_distphi);
   fChain->SetBranchAddress("B_LOKI_DTF_CTAU", &B_LOKI_DTF_CTAU, &b_B_LOKI_DTF_CTAU);
   fChain->SetBranchAddress("B_LOKI_MASS_JpsiConstr", &B_LOKI_MASS_JpsiConstr, &b_B_LOKI_MASS_JpsiConstr);
   fChain->SetBranchAddress("J_psi_1S_len", &J_psi_1S_len, &b_J_psi_1S_len);
   fChain->SetBranchAddress("J_psi_1S_LOKI_ENERGY", &J_psi_1S_LOKI_ENERGY, &b_J_psi_1S_LOKI_ENERGY);
   fChain->SetBranchAddress("J_psi_1S_LOKI_ETA", &J_psi_1S_LOKI_ETA, &b_J_psi_1S_LOKI_ETA);
   fChain->SetBranchAddress("J_psi_1S_LOKI_PHI", &J_psi_1S_LOKI_PHI, &b_J_psi_1S_LOKI_PHI);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_X", &J_psi_1S_ENDVERTEX_X, &b_J_psi_1S_ENDVERTEX_X);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_Y", &J_psi_1S_ENDVERTEX_Y, &b_J_psi_1S_ENDVERTEX_Y);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_Z", &J_psi_1S_ENDVERTEX_Z, &b_J_psi_1S_ENDVERTEX_Z);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_XERR", &J_psi_1S_ENDVERTEX_XERR, &b_J_psi_1S_ENDVERTEX_XERR);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_YERR", &J_psi_1S_ENDVERTEX_YERR, &b_J_psi_1S_ENDVERTEX_YERR);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_ZERR", &J_psi_1S_ENDVERTEX_ZERR, &b_J_psi_1S_ENDVERTEX_ZERR);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_CHI2", &J_psi_1S_ENDVERTEX_CHI2, &b_J_psi_1S_ENDVERTEX_CHI2);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_NDOF", &J_psi_1S_ENDVERTEX_NDOF, &b_J_psi_1S_ENDVERTEX_NDOF);
   fChain->SetBranchAddress("J_psi_1S_ENDVERTEX_COV_", J_psi_1S_ENDVERTEX_COV_, &b_J_psi_1S_ENDVERTEX_COV_);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_X", &J_psi_1S_OWNPV_X, &b_J_psi_1S_OWNPV_X);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_Y", &J_psi_1S_OWNPV_Y, &b_J_psi_1S_OWNPV_Y);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_Z", &J_psi_1S_OWNPV_Z, &b_J_psi_1S_OWNPV_Z);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_XERR", &J_psi_1S_OWNPV_XERR, &b_J_psi_1S_OWNPV_XERR);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_YERR", &J_psi_1S_OWNPV_YERR, &b_J_psi_1S_OWNPV_YERR);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_ZERR", &J_psi_1S_OWNPV_ZERR, &b_J_psi_1S_OWNPV_ZERR);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_CHI2", &J_psi_1S_OWNPV_CHI2, &b_J_psi_1S_OWNPV_CHI2);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_NDOF", &J_psi_1S_OWNPV_NDOF, &b_J_psi_1S_OWNPV_NDOF);
   fChain->SetBranchAddress("J_psi_1S_OWNPV_COV_", J_psi_1S_OWNPV_COV_, &b_J_psi_1S_OWNPV_COV_);
   fChain->SetBranchAddress("J_psi_1S_IP_OWNPV", &J_psi_1S_IP_OWNPV, &b_J_psi_1S_IP_OWNPV);
   fChain->SetBranchAddress("J_psi_1S_IPCHI2_OWNPV", &J_psi_1S_IPCHI2_OWNPV, &b_J_psi_1S_IPCHI2_OWNPV);
   fChain->SetBranchAddress("J_psi_1S_FD_OWNPV", &J_psi_1S_FD_OWNPV, &b_J_psi_1S_FD_OWNPV);
   fChain->SetBranchAddress("J_psi_1S_FDCHI2_OWNPV", &J_psi_1S_FDCHI2_OWNPV, &b_J_psi_1S_FDCHI2_OWNPV);
   fChain->SetBranchAddress("J_psi_1S_DIRA_OWNPV", &J_psi_1S_DIRA_OWNPV, &b_J_psi_1S_DIRA_OWNPV);
   fChain->SetBranchAddress("J_psi_1S_ORIVX_X", &J_psi_1S_ORIVX_X, &b_J_psi_1S_ORIVX_X);
   fChain->SetBranchAddress("J_psi_1S_ORIVX_Y", &J_psi_1S_ORIVX_Y, &b_J_psi_1S_ORIVX_Y);
   fChain->SetBranchAddress("J_psi_1S_ORIVX_Z", &J_psi_1S_ORIVX_Z, &b_J_psi_1S_ORIVX_Z);
   fChain->SetBranchAddress("J_psi_1S_ORIVX_XERR", &J_psi_1S_ORIVX_XERR, &b_J_psi_1S_ORIVX_XERR);
   fChain->SetBranchAddress("J_psi_1S_ORIVX_YERR", &J_psi_1S_ORIVX_YERR, &b_J_psi_1S_ORIVX_YERR);
   fChain->SetBranchAddress("J_psi_1S_ORIVX_ZERR", &J_psi_1S_ORIVX_ZERR, &b_J_psi_1S_ORIVX_ZERR);
   fChain->SetBranchAddress("J_psi_1S_ORIVX_CHI2", &J_psi_1S_ORIVX_CHI2, &b_J_psi_1S_ORIVX_CHI2);
   fChain->SetBranchAddress("J_psi_1S_ORIVX_NDOF", &J_psi_1S_ORIVX_NDOF, &b_J_psi_1S_ORIVX_NDOF);
   fChain->SetBranchAddress("J_psi_1S_ORIVX_COV_", J_psi_1S_ORIVX_COV_, &b_J_psi_1S_ORIVX_COV_);
   fChain->SetBranchAddress("J_psi_1S_FD_ORIVX", &J_psi_1S_FD_ORIVX, &b_J_psi_1S_FD_ORIVX);
   fChain->SetBranchAddress("J_psi_1S_FDCHI2_ORIVX", &J_psi_1S_FDCHI2_ORIVX, &b_J_psi_1S_FDCHI2_ORIVX);
   fChain->SetBranchAddress("J_psi_1S_DIRA_ORIVX", &J_psi_1S_DIRA_ORIVX, &b_J_psi_1S_DIRA_ORIVX);
   fChain->SetBranchAddress("J_psi_1S_P", &J_psi_1S_P, &b_J_psi_1S_P);
   fChain->SetBranchAddress("J_psi_1S_PT", &J_psi_1S_PT, &b_J_psi_1S_PT);
   fChain->SetBranchAddress("J_psi_1S_PE", &J_psi_1S_PE, &b_J_psi_1S_PE);
   fChain->SetBranchAddress("J_psi_1S_PX", &J_psi_1S_PX, &b_J_psi_1S_PX);
   fChain->SetBranchAddress("J_psi_1S_PY", &J_psi_1S_PY, &b_J_psi_1S_PY);
   fChain->SetBranchAddress("J_psi_1S_PZ", &J_psi_1S_PZ, &b_J_psi_1S_PZ);
   fChain->SetBranchAddress("J_psi_1S_MM", &J_psi_1S_MM, &b_J_psi_1S_MM);
   fChain->SetBranchAddress("J_psi_1S_MMERR", &J_psi_1S_MMERR, &b_J_psi_1S_MMERR);
   fChain->SetBranchAddress("J_psi_1S_M", &J_psi_1S_M, &b_J_psi_1S_M);
   fChain->SetBranchAddress("J_psi_1S_TAU", &J_psi_1S_TAU, &b_J_psi_1S_TAU);
   fChain->SetBranchAddress("J_psi_1S_TAUERR", &J_psi_1S_TAUERR, &b_J_psi_1S_TAUERR);
   fChain->SetBranchAddress("J_psi_1S_TAUCHI2", &J_psi_1S_TAUCHI2, &b_J_psi_1S_TAUCHI2);
   fChain->SetBranchAddress("muplus_len", &muplus_len, &b_muplus_len);
   fChain->SetBranchAddress("Tr_T_BPVIP", Tr_T_BPVIP, &b_Tr_T_BPVIP);
   fChain->SetBranchAddress("Tr_T_BPVIPCHI2", Tr_T_BPVIPCHI2, &b_Tr_T_BPVIPCHI2);
   fChain->SetBranchAddress("Tr_T_Charge", Tr_T_Charge, &b_Tr_T_Charge);
   fChain->SetBranchAddress("Tr_T_E", Tr_T_E, &b_Tr_T_E);
   fChain->SetBranchAddress("Tr_T_EcalE", Tr_T_EcalE, &b_Tr_T_EcalE);
   fChain->SetBranchAddress("Tr_T_Eta", Tr_T_Eta, &b_Tr_T_Eta);
   fChain->SetBranchAddress("Tr_T_HASMUON", Tr_T_HASMUON, &b_Tr_T_HASMUON);
   fChain->SetBranchAddress("Tr_T_HASRICH", Tr_T_HASRICH, &b_Tr_T_HASRICH);
   fChain->SetBranchAddress("Tr_T_HcalE", Tr_T_HcalE, &b_Tr_T_HcalE);
   fChain->SetBranchAddress("Tr_T_ISMUON", Tr_T_ISMUON, &b_Tr_T_ISMUON);
   fChain->SetBranchAddress("Tr_T_MinIP", Tr_T_MinIP, &b_Tr_T_MinIP);
   fChain->SetBranchAddress("Tr_T_MinIPChi2", Tr_T_MinIPChi2, &b_Tr_T_MinIPChi2);
   fChain->SetBranchAddress("Tr_T_NSHARED", Tr_T_NSHARED, &b_Tr_T_NSHARED);
   fChain->SetBranchAddress("Tr_T_P", Tr_T_P, &b_Tr_T_P);
   fChain->SetBranchAddress("Tr_T_PIDK", Tr_T_PIDK, &b_Tr_T_PIDK);
   fChain->SetBranchAddress("Tr_T_PIDe", Tr_T_PIDe, &b_Tr_T_PIDe);
   fChain->SetBranchAddress("Tr_T_PIDmu", Tr_T_PIDmu, &b_Tr_T_PIDmu);
   fChain->SetBranchAddress("Tr_T_PIDp", Tr_T_PIDp, &b_Tr_T_PIDp);
   fChain->SetBranchAddress("Tr_T_PROBNNe", Tr_T_PROBNNe, &b_Tr_T_PROBNNe);
   fChain->SetBranchAddress("Tr_T_PROBNNghost", Tr_T_PROBNNghost, &b_Tr_T_PROBNNghost);
   fChain->SetBranchAddress("Tr_T_PROBNNk", Tr_T_PROBNNk, &b_Tr_T_PROBNNk);
   fChain->SetBranchAddress("Tr_T_PROBNNmu", Tr_T_PROBNNmu, &b_Tr_T_PROBNNmu);
   fChain->SetBranchAddress("Tr_T_PROBNNp", Tr_T_PROBNNp, &b_Tr_T_PROBNNp);
   fChain->SetBranchAddress("Tr_T_PROBNNpi", Tr_T_PROBNNpi, &b_Tr_T_PROBNNpi);
   fChain->SetBranchAddress("Tr_T_PT", Tr_T_PT, &b_Tr_T_PT);
   fChain->SetBranchAddress("Tr_T_Phi", Tr_T_Phi, &b_Tr_T_Phi);
   fChain->SetBranchAddress("Tr_T_PrsE", Tr_T_PrsE, &b_Tr_T_PrsE);
   fChain->SetBranchAddress("Tr_T_THETA", Tr_T_THETA, &b_Tr_T_THETA);
   fChain->SetBranchAddress("Tr_T_TRCHI2DOF", Tr_T_TRCHI2DOF, &b_Tr_T_TRCHI2DOF);
   fChain->SetBranchAddress("Tr_T_TRCLONEDIST", Tr_T_TRCLONEDIST, &b_Tr_T_TRCLONEDIST);
   fChain->SetBranchAddress("Tr_T_TRFITMATCHCHI2", Tr_T_TRFITMATCHCHI2, &b_Tr_T_TRFITMATCHCHI2);
   fChain->SetBranchAddress("Tr_T_TRFITTCHI2", Tr_T_TRFITTCHI2, &b_Tr_T_TRFITTCHI2);
   fChain->SetBranchAddress("Tr_T_TRFITVELOCHI2NDOF", Tr_T_TRFITVELOCHI2NDOF, &b_Tr_T_TRFITVELOCHI2NDOF);
   fChain->SetBranchAddress("Tr_T_TRGHOSTPROB", Tr_T_TRGHOSTPROB, &b_Tr_T_TRGHOSTPROB);
   fChain->SetBranchAddress("Tr_T_TRPCHI2", Tr_T_TRPCHI2, &b_Tr_T_TRPCHI2);
   fChain->SetBranchAddress("Tr_T_TRTYPE", Tr_T_TRTYPE, &b_Tr_T_TRTYPE);
   fChain->SetBranchAddress("Tr_T_TrFIRSTHITZ", Tr_T_TrFIRSTHITZ, &b_Tr_T_TrFIRSTHITZ);
   fChain->SetBranchAddress("Tr_T_TrFITTCHI2NDOF", Tr_T_TrFITTCHI2NDOF, &b_Tr_T_TrFITTCHI2NDOF);
   fChain->SetBranchAddress("Tr_T_ACHI2DOCA", Tr_T_ACHI2DOCA, &b_Tr_T_ACHI2DOCA);
   fChain->SetBranchAddress("Tr_T_ADOCA", Tr_T_ADOCA, &b_Tr_T_ADOCA);
   fChain->SetBranchAddress("muplus_LOKI_ENERGY", &muplus_LOKI_ENERGY, &b_muplus_LOKI_ENERGY);
   fChain->SetBranchAddress("muplus_LOKI_ETA", &muplus_LOKI_ETA, &b_muplus_LOKI_ETA);
   fChain->SetBranchAddress("muplus_LOKI_PHI", &muplus_LOKI_PHI, &b_muplus_LOKI_PHI);
   fChain->SetBranchAddress("muplus_OWNPV_X", &muplus_OWNPV_X, &b_muplus_OWNPV_X);
   fChain->SetBranchAddress("muplus_OWNPV_Y", &muplus_OWNPV_Y, &b_muplus_OWNPV_Y);
   fChain->SetBranchAddress("muplus_OWNPV_Z", &muplus_OWNPV_Z, &b_muplus_OWNPV_Z);
   fChain->SetBranchAddress("muplus_OWNPV_XERR", &muplus_OWNPV_XERR, &b_muplus_OWNPV_XERR);
   fChain->SetBranchAddress("muplus_OWNPV_YERR", &muplus_OWNPV_YERR, &b_muplus_OWNPV_YERR);
   fChain->SetBranchAddress("muplus_OWNPV_ZERR", &muplus_OWNPV_ZERR, &b_muplus_OWNPV_ZERR);
   fChain->SetBranchAddress("muplus_OWNPV_CHI2", &muplus_OWNPV_CHI2, &b_muplus_OWNPV_CHI2);
   fChain->SetBranchAddress("muplus_OWNPV_NDOF", &muplus_OWNPV_NDOF, &b_muplus_OWNPV_NDOF);
   fChain->SetBranchAddress("muplus_OWNPV_COV_", muplus_OWNPV_COV_, &b_muplus_OWNPV_COV_);
   fChain->SetBranchAddress("muplus_IP_OWNPV", &muplus_IP_OWNPV, &b_muplus_IP_OWNPV);
   fChain->SetBranchAddress("muplus_IPCHI2_OWNPV", &muplus_IPCHI2_OWNPV, &b_muplus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("muplus_ORIVX_X", &muplus_ORIVX_X, &b_muplus_ORIVX_X);
   fChain->SetBranchAddress("muplus_ORIVX_Y", &muplus_ORIVX_Y, &b_muplus_ORIVX_Y);
   fChain->SetBranchAddress("muplus_ORIVX_Z", &muplus_ORIVX_Z, &b_muplus_ORIVX_Z);
   fChain->SetBranchAddress("muplus_ORIVX_XERR", &muplus_ORIVX_XERR, &b_muplus_ORIVX_XERR);
   fChain->SetBranchAddress("muplus_ORIVX_YERR", &muplus_ORIVX_YERR, &b_muplus_ORIVX_YERR);
   fChain->SetBranchAddress("muplus_ORIVX_ZERR", &muplus_ORIVX_ZERR, &b_muplus_ORIVX_ZERR);
   fChain->SetBranchAddress("muplus_ORIVX_CHI2", &muplus_ORIVX_CHI2, &b_muplus_ORIVX_CHI2);
   fChain->SetBranchAddress("muplus_ORIVX_NDOF", &muplus_ORIVX_NDOF, &b_muplus_ORIVX_NDOF);
   fChain->SetBranchAddress("muplus_ORIVX_COV_", muplus_ORIVX_COV_, &b_muplus_ORIVX_COV_);
   fChain->SetBranchAddress("muplus_P", &muplus_P, &b_muplus_P);
   fChain->SetBranchAddress("muplus_PT", &muplus_PT, &b_muplus_PT);
   fChain->SetBranchAddress("muplus_PE", &muplus_PE, &b_muplus_PE);
   fChain->SetBranchAddress("muplus_PX", &muplus_PX, &b_muplus_PX);
   fChain->SetBranchAddress("muplus_PY", &muplus_PY, &b_muplus_PY);
   fChain->SetBranchAddress("muplus_PZ", &muplus_PZ, &b_muplus_PZ);
   fChain->SetBranchAddress("muplus_M", &muplus_M, &b_muplus_M);
   fChain->SetBranchAddress("muminus_len", &muminus_len, &b_muminus_len);
   fChain->SetBranchAddress("muminus_LOKI_ENERGY", &muminus_LOKI_ENERGY, &b_muminus_LOKI_ENERGY);
   fChain->SetBranchAddress("muminus_LOKI_ETA", &muminus_LOKI_ETA, &b_muminus_LOKI_ETA);
   fChain->SetBranchAddress("muminus_LOKI_PHI", &muminus_LOKI_PHI, &b_muminus_LOKI_PHI);
   fChain->SetBranchAddress("muminus_OWNPV_X", &muminus_OWNPV_X, &b_muminus_OWNPV_X);
   fChain->SetBranchAddress("muminus_OWNPV_Y", &muminus_OWNPV_Y, &b_muminus_OWNPV_Y);
   fChain->SetBranchAddress("muminus_OWNPV_Z", &muminus_OWNPV_Z, &b_muminus_OWNPV_Z);
   fChain->SetBranchAddress("muminus_OWNPV_XERR", &muminus_OWNPV_XERR, &b_muminus_OWNPV_XERR);
   fChain->SetBranchAddress("muminus_OWNPV_YERR", &muminus_OWNPV_YERR, &b_muminus_OWNPV_YERR);
   fChain->SetBranchAddress("muminus_OWNPV_ZERR", &muminus_OWNPV_ZERR, &b_muminus_OWNPV_ZERR);
   fChain->SetBranchAddress("muminus_OWNPV_CHI2", &muminus_OWNPV_CHI2, &b_muminus_OWNPV_CHI2);
   fChain->SetBranchAddress("muminus_OWNPV_NDOF", &muminus_OWNPV_NDOF, &b_muminus_OWNPV_NDOF);
   fChain->SetBranchAddress("muminus_OWNPV_COV_", muminus_OWNPV_COV_, &b_muminus_OWNPV_COV_);
   fChain->SetBranchAddress("muminus_IP_OWNPV", &muminus_IP_OWNPV, &b_muminus_IP_OWNPV);
   fChain->SetBranchAddress("muminus_IPCHI2_OWNPV", &muminus_IPCHI2_OWNPV, &b_muminus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("muminus_ORIVX_X", &muminus_ORIVX_X, &b_muminus_ORIVX_X);
   fChain->SetBranchAddress("muminus_ORIVX_Y", &muminus_ORIVX_Y, &b_muminus_ORIVX_Y);
   fChain->SetBranchAddress("muminus_ORIVX_Z", &muminus_ORIVX_Z, &b_muminus_ORIVX_Z);
   fChain->SetBranchAddress("muminus_ORIVX_XERR", &muminus_ORIVX_XERR, &b_muminus_ORIVX_XERR);
   fChain->SetBranchAddress("muminus_ORIVX_YERR", &muminus_ORIVX_YERR, &b_muminus_ORIVX_YERR);
   fChain->SetBranchAddress("muminus_ORIVX_ZERR", &muminus_ORIVX_ZERR, &b_muminus_ORIVX_ZERR);
   fChain->SetBranchAddress("muminus_ORIVX_CHI2", &muminus_ORIVX_CHI2, &b_muminus_ORIVX_CHI2);
   fChain->SetBranchAddress("muminus_ORIVX_NDOF", &muminus_ORIVX_NDOF, &b_muminus_ORIVX_NDOF);
   fChain->SetBranchAddress("muminus_ORIVX_COV_", muminus_ORIVX_COV_, &b_muminus_ORIVX_COV_);
   fChain->SetBranchAddress("muminus_P", &muminus_P, &b_muminus_P);
   fChain->SetBranchAddress("muminus_PT", &muminus_PT, &b_muminus_PT);
   fChain->SetBranchAddress("muminus_PE", &muminus_PE, &b_muminus_PE);
   fChain->SetBranchAddress("muminus_PX", &muminus_PX, &b_muminus_PX);
   fChain->SetBranchAddress("muminus_PY", &muminus_PY, &b_muminus_PY);
   fChain->SetBranchAddress("muminus_PZ", &muminus_PZ, &b_muminus_PZ);
   fChain->SetBranchAddress("muminus_M", &muminus_M, &b_muminus_M);
   fChain->SetBranchAddress("Kplus_len", &Kplus_len, &b_Kplus_len);
   fChain->SetBranchAddress("Kplus_LOKI_ENERGY", &Kplus_LOKI_ENERGY, &b_Kplus_LOKI_ENERGY);
   fChain->SetBranchAddress("Kplus_LOKI_ETA", &Kplus_LOKI_ETA, &b_Kplus_LOKI_ETA);
   fChain->SetBranchAddress("Kplus_LOKI_PHI", &Kplus_LOKI_PHI, &b_Kplus_LOKI_PHI);
   fChain->SetBranchAddress("Kplus_OWNPV_X", &Kplus_OWNPV_X, &b_Kplus_OWNPV_X);
   fChain->SetBranchAddress("Kplus_OWNPV_Y", &Kplus_OWNPV_Y, &b_Kplus_OWNPV_Y);
   fChain->SetBranchAddress("Kplus_OWNPV_Z", &Kplus_OWNPV_Z, &b_Kplus_OWNPV_Z);
   fChain->SetBranchAddress("Kplus_OWNPV_XERR", &Kplus_OWNPV_XERR, &b_Kplus_OWNPV_XERR);
   fChain->SetBranchAddress("Kplus_OWNPV_YERR", &Kplus_OWNPV_YERR, &b_Kplus_OWNPV_YERR);
   fChain->SetBranchAddress("Kplus_OWNPV_ZERR", &Kplus_OWNPV_ZERR, &b_Kplus_OWNPV_ZERR);
   fChain->SetBranchAddress("Kplus_OWNPV_CHI2", &Kplus_OWNPV_CHI2, &b_Kplus_OWNPV_CHI2);
   fChain->SetBranchAddress("Kplus_OWNPV_NDOF", &Kplus_OWNPV_NDOF, &b_Kplus_OWNPV_NDOF);
   fChain->SetBranchAddress("Kplus_OWNPV_COV_", Kplus_OWNPV_COV_, &b_Kplus_OWNPV_COV_);
   fChain->SetBranchAddress("Kplus_IP_OWNPV", &Kplus_IP_OWNPV, &b_Kplus_IP_OWNPV);
   fChain->SetBranchAddress("Kplus_IPCHI2_OWNPV", &Kplus_IPCHI2_OWNPV, &b_Kplus_IPCHI2_OWNPV);
   fChain->SetBranchAddress("Kplus_ORIVX_X", &Kplus_ORIVX_X, &b_Kplus_ORIVX_X);
   fChain->SetBranchAddress("Kplus_ORIVX_Y", &Kplus_ORIVX_Y, &b_Kplus_ORIVX_Y);
   fChain->SetBranchAddress("Kplus_ORIVX_Z", &Kplus_ORIVX_Z, &b_Kplus_ORIVX_Z);
   fChain->SetBranchAddress("Kplus_ORIVX_XERR", &Kplus_ORIVX_XERR, &b_Kplus_ORIVX_XERR);
   fChain->SetBranchAddress("Kplus_ORIVX_YERR", &Kplus_ORIVX_YERR, &b_Kplus_ORIVX_YERR);
   fChain->SetBranchAddress("Kplus_ORIVX_ZERR", &Kplus_ORIVX_ZERR, &b_Kplus_ORIVX_ZERR);
   fChain->SetBranchAddress("Kplus_ORIVX_CHI2", &Kplus_ORIVX_CHI2, &b_Kplus_ORIVX_CHI2);
   fChain->SetBranchAddress("Kplus_ORIVX_NDOF", &Kplus_ORIVX_NDOF, &b_Kplus_ORIVX_NDOF);
   fChain->SetBranchAddress("Kplus_ORIVX_COV_", Kplus_ORIVX_COV_, &b_Kplus_ORIVX_COV_);
   fChain->SetBranchAddress("Kplus_P", &Kplus_P, &b_Kplus_P);
   fChain->SetBranchAddress("Kplus_PT", &Kplus_PT, &b_Kplus_PT);
   fChain->SetBranchAddress("Kplus_PE", &Kplus_PE, &b_Kplus_PE);
   fChain->SetBranchAddress("Kplus_PX", &Kplus_PX, &b_Kplus_PX);
   fChain->SetBranchAddress("Kplus_PY", &Kplus_PY, &b_Kplus_PY);
   fChain->SetBranchAddress("Kplus_PZ", &Kplus_PZ, &b_Kplus_PZ);
   fChain->SetBranchAddress("Kplus_M", &Kplus_M, &b_Kplus_M);
   fChain->SetBranchAddress("nCandidate", &nCandidate, &b_nCandidate);
   fChain->SetBranchAddress("totCandidates", &totCandidates, &b_totCandidates);
   fChain->SetBranchAddress("EventInSequence", &EventInSequence, &b_EventInSequence);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("BCType", &BCType, &b_BCType);
   fChain->SetBranchAddress("OdinTCK", &OdinTCK, &b_OdinTCK);
   fChain->SetBranchAddress("L0DUTCK", &L0DUTCK, &b_L0DUTCK);
   fChain->SetBranchAddress("HLT1TCK", &HLT1TCK, &b_HLT1TCK);
   fChain->SetBranchAddress("HLT2TCK", &HLT2TCK, &b_HLT2TCK);
   fChain->SetBranchAddress("GpsTime", &GpsTime, &b_GpsTime);
   fChain->SetBranchAddress("Polarity", &Polarity, &b_Polarity);
   fChain->SetBranchAddress("nPV", &nPV, &b_nPV);
   fChain->SetBranchAddress("PVX", PVX, &b_PVX);
   fChain->SetBranchAddress("PVY", PVY, &b_PVY);
   fChain->SetBranchAddress("PVZ", PVZ, &b_PVZ);
   fChain->SetBranchAddress("PVXERR", PVXERR, &b_PVXERR);
   fChain->SetBranchAddress("PVYERR", PVYERR, &b_PVYERR);
   fChain->SetBranchAddress("PVZERR", PVZERR, &b_PVZERR);
   fChain->SetBranchAddress("PVCHI2", PVCHI2, &b_PVCHI2);
   fChain->SetBranchAddress("PVNDOF", PVNDOF, &b_PVNDOF);
   fChain->SetBranchAddress("PVNTRACKS", PVNTRACKS, &b_PVNTRACKS);
   fChain->SetBranchAddress("StrippingBetaSBs2JpsiPhiDetachedLineDecision", &StrippingBetaSBs2JpsiPhiDetachedLineDecision, &b_StrippingBetaSBs2JpsiPhiDetachedLineDecision);
   fChain->SetBranchAddress("StrippingBetaSBd2JpsiKstarDetachedLineDecision", &StrippingBetaSBd2JpsiKstarDetachedLineDecision, &b_StrippingBetaSBd2JpsiKstarDetachedLineDecision);
   fChain->SetBranchAddress("StrippingBetaSBu2JpsiKDetachedLineDecision", &StrippingBetaSBu2JpsiKDetachedLineDecision, &b_StrippingBetaSBu2JpsiKDetachedLineDecision);
   Notify();
}

Bool_t DecayTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void DecayTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t DecayTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef DecayTree_cxx
