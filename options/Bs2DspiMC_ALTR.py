"""
 Script to create a DecayTreeTuple for Bd2Dpi, Bs2Dspi and Bu2Dpi analyses:
"""
__author__ = "Ulrich Eitschberger <ulrich.eitschberger@tu-dortmund.de>"

##############################################################################################
##### SETTINGS
##############################################################################################
isMC = True # False
run =   "2016" # "2016" # 

from Configurables import (DaVinci ,
                           TupleToolIsoGeneric,
                           TupleToolVtxCharge,
                           MCTupleToolHierarchy,
                           TupleToolMCBackgroundInfo,
                           BackgroundCategory)
                           
DaVinci().EvtMax = -1
DaVinci().SkipEvents = 0
DaVinci().PrintFreq = 500
DaVinci().DataType = run
DaVinci().Simulation   = isMC
DaVinci().Lumi = True
DaVinci().TupleFile = "DTT_B2Dpi_ALTR_%s.root"%run

## do the restripping (only applicable to simulation)
restrip_mc = False

## use the DecayTreeFitter
dtf = False 
import os , sys
os.getcwd()
sys.path.append(os.getcwd())
## number of events to process
max_events = 200
skip_events = -1

##############################################################################################
##### GLOBALS
##############################################################################################
prefix = ''
if isMC:
  if run == "2012":
    prefix = '/Event/b0s2dssth.Strip' #2012
  if run == "2016":
    prefix = '/Event/AllStreams' #2016
    print "prefix = %s"%prefix
    
else:
  prefix = 'BhadronCompleteEvent'
  
print("prefix is " + prefix)
##############################################################################################
##### IMPORTS
##############################################################################################

import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
import copy
from Configurables import *
from ParticleTagger import *
#from CharmTagger import *

import re

from PhysSelPython.Wrappers import Selection, SelectionSequence

from Configurables import TrackScaleState
from Configurables import CondDB

#if isMC:
#  from Configurables import TupleToolMCDecayTree

class Decay(object):
  def __init__(self, other=None):
    if other != None:
      self.__dict__ = other.__dict__.copy()

##############################################################################################
##### SELECTIONS
##############################################################################################
seqB2OC          = GaudiSequencer('seqB2OC')
TupleSeq         = GaudiSequencer('TupleSeq')

##############################################################################################

##############################################################################################
##### DECAYS
##############################################################################################

#----------------------
bs2dspi_detached_dict = {
 "tuple_name"             : "Bs2DspiDetached",
 "decay_descriptor"       : "[[B0]CC -> ^(D- -> ^K+ ^K- ^pi-) ^pi+]CC",
# "inputs"                 : "/Phys/B02DPiD2HHHBeauty2CharmLine/Particles",
 "inputs"                 : prefix + "/Phys/B02DPiD2HHHBeauty2CharmLine/Particles",
 "daughters_to_constrain" : [["D_s-"]],
 "descriptor_B"           : "^([[B0]CC -> (D- -> K+ K- pi-) pi+]CC)"
}
bs2dspi_detached = Decay()
bs2dspi_detached.__dict__ = bs2dspi_detached_dict

#----------------------
bd2dpi_detached_dict = {
  "tuple_name"             : "Bd2DpiDetached",
  "decay_descriptor"       : "[[B0]CC -> ^(D- -> ^pi- ^pi- ^K+) ^pi+]CC",
  "inputs"                 : prefix + "/Phys/B02DPiD2HHHBeauty2CharmLine/Particles",
  "daughters_to_constrain" : [["D-"]],
  "descriptor_B"           : "^([[B0]CC -> (D- -> pi- pi- K+) pi+]CC)",
  "BTA_name"               : "Bd2Dpi_TBA"
  }

bd2dpi_detached = Decay()
bd2dpi_detached.__dict__ = bd2dpi_detached_dict

#----------------------
bu2dpi_detached_dict = {
  "tuple_name"             : "Bu2DpiDetached",
  "decay_descriptor"       : "(B- -> ^(D0 -> ^K- ^pi+) ^pi-) || (B- -> ^(D0 -> ^K+ ^pi-) ^pi-) || (B+ -> ^(D0 -> ^K+ ^pi-) ^pi+) || (B+ -> ^(D0 -> ^K- ^pi+) ^pi+)",
  "inputs"                 : prefix + "/Phys/B2D0PiD2HHBeauty2CharmLine/Particles",
  "daughters_to_constrain" : [["D0"]],
  "descriptor_B"           : "^((B- -> (D0 -> K- pi+) pi-) || (B- -> (D0 -> K+ pi-) pi-) || (B+ -> (D0 -> K+ pi-) pi+) || (B+ -> (D0 -> K- pi+) pi+))",
  "BTA_name"               : "Bu2Dpi_TBA"
  }
bu2dpi_detached = Decay()
bu2dpi_detached.__dict__ = bu2dpi_detached_dict


##############################################################################################
##### TOOLS AND TRIGGERS AND STRIPPING LINES AND LOKI VARIABLES
##############################################################################################
tuple_tools = ["TupleToolPropertime",
               "TupleToolVtxCharge",
               "TupleToolPrimaries"]

loki_variables = {"LOKI_ENERGY"     : "E",
                  "LOKI_ETA"        : "ETA",
                  "LOKI_PHI"        : "PHI",
                  "LOKI_TRCHI2DOF"  : "TRCHI2DOF",
                  "LOKI_BPVIPCHI2"  : "BPVIPCHI2()"
}

loki_variables_B = {
  "LOKI_MIPCHI2DV_PRIMARY" : "MIPCHI2DV(PRIMARY)",
  "LOKI_BPVLTIME"          : "BPVLTIME('PropertimeFitter/properTime:PUBLIC')",
  "LOKI_FDCHI2"            : "BPVVDCHI2",
  "LOKI_FDS"               : "BPVDLS",
  "LOKI_DIRA"              : "BPVDIRA",
  ###### DecayTreeFitVariables
  "LOKI_DTF_CTAU"                   : "DTF_CTAU( 0, True )",
  "LOKI_DTF_CTAUS"                  : "DTF_CTAUSIGNIFICANCE( 0, True )",
  "LOKI_DTF_CHI2NDOF"               : "DTF_CHI2NDOF( True )",
  "LOKI_DTF_CTAUERR"                : "DTF_CTAUERR( 0, True )",
  "LOKI_MASS_DConstr"               : "DTF_FUN ( M , True , 'D_s-' )" ,
}

stripping_lines = ['StrippingB02DPiD2HHHBeauty2CharmLineDecision'
                  ,'StrippingB2D0PiD2HHBeauty2CharmLineDecision']

l0_lines = [
  "L0PhysicsDecision"
  ,"L0HadronDecision"]

hlt1_lines = [
  "Hlt1TrackAllL0Decision"
  ,"Hlt1TrackMVADecision"
  ,"Hlt1TrackAllL0TightDecision"]


hlt2_lines = ["Hlt2Topo2BodyDecision"
              ,"Hlt2Topo3BodyDecision"
              ,"Hlt2Topo4BodyDecision"
              ,"Hlt2Topo2BodyBBDTDecision"
              ,"Hlt2Topo3BodyBBDTDecision"
              ,"Hlt2Topo4BodyBBDTDecision"
              ,"Hlt2IncPhiDecision"
              ,"Hlt2IncPhiSidebandsDecision"
              ,"Hlt2B2HHDecision"]

trigger_lines = l0_lines + hlt1_lines + hlt2_lines

if DaVinci().DataType == "2012":
  trigger_lines = trigger_lines
elif DaVinci().DataType == "2011":
  trigger_lines = trigger_lines
elif DaVinci().DataType == "2015":
  trigger_lines = trigger_lines
elif DaVinci().DataType == "2016":
  trigger_lines = trigger_lines

def CreateTupleTool(decay):
  print "-----------------------------------------------------------------------"
  print "Adding decay " + decay.decay_descriptor
  print "Location:", decay.inputs
  print "Tuple name:", decay.tuple_name
  tuple = DecayTreeTuple(decay.tuple_name)
  tuple.Inputs =  [decay.inputs] 
  tuple.Decay = decay.decay_descriptor
  tuple.ReFitPVs = True


  tools = copy.copy(tuple_tools)
  # if isMC:
  #   tt_mct = tuple.addTupleTool("TupleToolMCTruth")
  #   tt_mct.ToolList += ["MCTupleToolKinematic", "MCTupleToolHierarchy"]
  #   tools.extend(tt_mct)#tuple_tools_mc)

  tuple.ToolList = tools

  print tuple.ToolList
 
  ## specific tuple tools ( no need for event tuples on data )
  tt_eventinfo = tuple.addTupleTool("TupleToolEventInfo")
  tt_eventinfo.Verbose = True
  # prepare tagging :
  #######################################################  
  # addParticleTaggerTuple( tuple, decay )
  addParticleTaggerMCTuple( tuple, decay )
  #addCharmTuple(tuple ,decay)
  #######################################################  
  tt_stripping = tuple.addTupleTool("TupleToolStripping")
  tt_stripping.Verbose = False
  tt_stripping.StrippingList = stripping_lines
 
  tt_tagging = tuple.addTupleTool("TupleToolTagging")
  tt_tagging.Verbose = True
  tt_tagging.StoreTaggersInfo = False

  tt_trigger = tuple.addTupleTool("TupleToolTrigger")
  tt_trigger.Verbose = True
  tt_trigger.TriggerList = trigger_lines
 
  tt_kinematic = tuple.addTupleTool("TupleToolKinematic")
  tt_kinematic.Verbose = True

  tt_loki_general = tuple.addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_general")
  tt_loki_general.Variables = loki_variables

  tt_recostats = tuple.addTupleTool("TupleToolRecoStats")
  tt_recostats.Verbose = False

  tt_geometry = tuple.addTupleTool("TupleToolGeometry")
  tt_geometry.Verbose = False
  tt_geometry.RefitPVs = False
  tt_geometry.FillMultiPV = False

  tt_trackinfo = tuple.addTupleTool("TupleToolTrackInfo")
  tt_trackinfo.Verbose = False

  tt_pid = tuple.addTupleTool("TupleToolPid")
  tt_pid.Verbose = True

  branches = {}
  name_bbranch = "lab0"
  branches[name_bbranch] = decay.descriptor_B

  print decay.descriptor_B

  print "Decay branches:", branches
  tuple.addBranches(branches)

  tt_loki_B = tuple.__getattr__(name_bbranch).addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_B")
  if "loki_variables_B" in decay.__dict__:
    tt_loki_B.Variables = dict(loki_variables_B.items() + decay.loki_variables_B.items())
  else:
    tt_loki_B.Variables = loki_variables_B

  tuple.UseLabXSyntax = True
  tuple.RevertToPositiveID = False

  tuple.addTupleTool("TupleToolDecay/lab0")
  tt_tistos_b = tuple.__getattr__(name_bbranch).addTupleTool("TupleToolTISTOS")
  tt_tistos_b.VerboseL0 = True
  tt_tistos_b.VerboseHlt1 = True
  tt_tistos_b.VerboseHlt2 = True
  tt_tistos_b.TriggerList = trigger_lines


  if isMC:
    MCTruth = tuple.addTupleTool("TupleToolMCTruth")
    MCTruth.addTupleTool("MCTupleToolHierarchy")
    BkgInfo = tuple.addTupleTool("TupleToolMCBackgroundInfo")
    BkgInfo.addTool(BackgroundCategory, name='BackgroundCategory')
    BkgInfo.BackgroundCategory.InclusiveDecay=True
    BkgInfo.BackgroundCategory.SemileptonicDecay=False


  return tuple

#TupleSeq.Members += [ CreateBTA(bs2dspi_detached)[0] , CreateTupleTool(bs2dspi_detached) ,  CreateBTA(bs2dspi_detached)[1] ]
TupleSeq.Members += [  CreateTupleTool(bs2dspi_detached)   ]
#TupleSeq.Members += [  CreateTupleTool(bd2dpi_detached)    ]
#TupleSeq.Members += [  CreateTupleTool(bu2dpi_detached)    ]

TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False

# check PVs
checkPV = CheckPV("CheckForOnePV")
checkPV.MinPVs = 1
print checkPV

##############################################################################

seqB2OC.Members      += [checkPV,TupleSeq]
seqB2OC.ModeOR        = True
seqB2OC.ShortCircuit  = False

evtTuple                 = EventTuple()
evtTuple.ToolList       += ["TupleToolEventInfo", "TupleToolTrigger"]

DaVinci().EvtMax   = max_events
#DaVinci().EvtMax   = -1
DaVinci().SkipEvents = skip_events
DaVinci().Lumi=False

# database tags
CondDB( LatestGlobalTagByDataType = run )

tupleName = "DTT_B2Dpi_ALTR"
if isMC:
  tupleName = "DTT_B2Dpi_MC_%s"%run
DaVinci().TupleFile       = "%s.root"%tupleName


from Configurables import GaudiSequencer
subseq_annpid = GaudiSequencer('SeqANNPID')

from Configurables import ChargedProtoANNPIDConf
conf_ann_pid = ChargedProtoANNPIDConf('ANNPIDConf')
conf_ann_pid.RecoSequencer = subseq_annpid


if run == "2016":
  conf_ann_pid.NetworkVersions[run] = "MC15TuneV1"
elif run == "2012":
  conf_ann_pid.NetworkVersions[run] = "MC12TuneV2"
conf_ann_pid.DataType = run

DaVinci().UserAlgorithms += [subseq_annpid]


if not isMC:
  scaler = TrackScaleState('scaler')
  DaVinci().UserAlgorithms += [scaler]

DaVinci().UserAlgorithms += [seqB2OC]

from GaudiConf import IOHelper

if isMC:
  if run == "2012":
    IOHelper().inputFiles([
      'root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/DST/MC/00052822_00000001_1.b0s2dssth.strip.dst'    # MC12
    ], clear=True)
  if run == "2016":
    IOHelper().inputFiles([
      #'root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/DST/MC/00058677_00004930_7.AllStreams.dst'    # MC16
      'root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/DST/MC/00058679_00000001_7.AllStreams.dst'    # MC16
    ], clear=True)
else:
  IOHelper().inputFiles([
  'root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/DST/00041834_00000016_1.bhadroncompleteevent.dst' # DATA
  ], clear=True)

