# Basem khanji,basem.khanji@cern.ch: ganga script to submit tagging tuples for RunI, RunII data (both DIMUON & BHADRON streams) in one go !
#
import os , re
#os.getcwd()
#sys.path.append(os.getcwd())

#year         = [ '11'   ,    '12'    ,  '15'   , '16'   ]
#energy       = [ '3500' ,    '4000'  ,  '6500' , '6500' ]  
#strip_v      = [ '21r1' ,      '21'  ,  '24'   , '26'   ]
#Reco_v       = [  '14'  ,     '14'   ,  '15a'  , '16'  ]

#/LHCb/Collision16/90000000/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28/BHADRONCOMPLETEEVENT.DST
year         = [ '16'   ]
energy       = [ '6500' ]  
strip_v      = [ '28r1'   ]
Reco_v       = [ '16'  ]

polarity     = [  'Up'  ,    'Down'  ]
streams      = ['BHADRONCOMPLETEEVENT']

# make the paths list
job_setting   = { }
List_Of_Paths = []

for stm in streams:
    for pol in polarity:
        for i in range(len(year)):
            PATH_name ='/LHCb/Collision'+year[i]+'/Beam'+energy[i]+'GeV-VeloClosed-Mag'+pol+'/Real Data/Reco'+Reco_v[i]+'/Stripping'+strip_v[i]+'/90000000/'+stm+'.DST'
            print PATH_name
            job_name =  '20' +year[i] + '_Reco' + Reco_v[i] + 'Strip' + strip_v[i] + '_' + pol + '_' + stm 
            job_setting[job_name] = PATH_name 
            List_Of_Paths.append( PATH_name )

print '========================================'
print 'Filled the list of PATHS for ganga jobs'
print '========================================'
print job_setting            


for job_name , path_dict in job_setting.items():
    print '======================================'   
    print 'Sumbitting a new job ...'
    print path_dict , ','  , job_name
    bk_query = BKQuery( path  =  path_dict,
                        credential_requirements = DiracProxy (
            dirac_env = None,
            validTime = None,
            group = 'lhcb_lowpriouser',
            encodeDefaultProxyFileName = False
            ) )
    print "bk_query: ", bk_query
    dataset = bk_query.getDataset()
    print "dataset: ", dataset
    myApp = GaudiExec()
    myApp.platform = 'x86_64-slc6-gcc62-opt'
    myApp.directory = "../DaVinciDev_v42r1"
    myApp.options = [bool('DIMUON' in job_name)*'./B2JpsiX_ALTR.py'  +  bool('BHADRONCOMPLETEEVENT' in job_name)*'./B2Dpi_ALTR.py']
    
    job=Job(name = job_name, backend=Dirac() )
    Year = bool('2011' in job_name)*' "2011" '+bool('2012' in job_name)*' "2012" '+ bool('2015' in job_name)*' "2015"  ' + bool('2016' in job_name)*' "2016"  '
    job.application= myApp
#    job.inputfiles=['./CharmTagger.py'  , './ParticleTagger.py' ]
    #job.application.optsfile = [
    #    bool('DIMUON' in job_name)*'./B2JpsiX_ALTR.py'  +  bool('BHADRONCOMPLETEEVENT' in job_name)*'./B2Dpi_ALTR.py'
    #    ]
    job.application.extraOpts  =     (
        'from Configurables import DaVinci                     ; ' +
        'DaVinci().TupleFile     = "DTT_'+ job_name + '.root"  ; ' +
        'DaVinci().EvtMax        =              -1             ; ' +
        'from Configurables import CondDB                      ; ' + 
        'CondDB().UseLatestTags  =    ['+ Year +']             ; ' +
        'DaVinci().DataType      =     '+ Year +'              ; '
        )

    print "Create job for the jobs: ", job.name
    job.inputdata  = dataset 
    #job.outputfiles= [LocalFile(namePattern='*.root')] #['DTT_B02DstD_data.root']
    #job.outputfiles= [LocalFile(namePattern='*.root')] # keep my Tuples on grid element (retrive manually) 
    job.outputfiles= [DiracFile(namePattern='*.root',locations=['CERN-USER']) ] # keep my Tuples on grid element (retrive manually) 
    print "Calls splitter"
    job.splitter = SplitByFiles(filesPerJob = 5 , maxFiles = -1, ignoremissing = True)
    # for LSF :
    print "Finally submit the job"
    job.submit()
    # queues.add(job.submit)
    print '======================================'   
    print "job: ", job.name + " submitted" 
    print '======================================'   
print " Jobs submitted .... bye "

