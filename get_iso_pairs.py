#!/bin/env python2
'''
Input: ROOT file with TTree containing isolation variables and flag "isInDecay"
Output: (Leightweight) ROOT file with seperate trees for isolating and non-isolating pairs of tracks
        Additionally a tree with all (iso & non-iso) tracks is made.
Usage: ./get_iso_pairs.py root_file.root
'''
import ROOT
from array import array as arr
import numpy as np
import sys

# Import an ntuple
f = ROOT.TFile(sys.argv[1])#"merged.root"
t=f.Get("Bd2DpiDetached/DecayTree")

# Book arrays for interesting variables
isovarnames=[
"MINIPCHI2",
"isInDecay",
"PT",
"MATCHCHI2",
"PVDIS",
"SVDIS",
"DOCA",
"ANGLE",
"FC_MU",
"VtxChi2ndof",
]
isovarnames = ["lab4_Tr_"+name for name in isovarnames]
isovars={name: arr('f') for name in isovarnames}
isopairs={name: arr('f') for name in isovarnames}
nonisopairs={name: arr('f') for name in isovarnames}
#isovars={name: np.array([]) for name in isovarnames}

# Loop over the tuple and extract interesting variables
for i,e in enumerate(t):
  isIso=True
  for j in range(e.NTr):
    for key in isovarnames:
      isovars[key].append(eval('e.'+key+'['+str(j)+']'))
      #isovars[key]=np.append(isovars[key], [eval('e.'+key+'['+str(j)+']')])
      if e.lab4_Tr_isInDecay[j]:
        # If the track is not the bachelor pion
        # (The bachelor pion doesn't belong anywhere)
        if int(e.lab4_Tr_PT[j] - e.lab5_PT) != 0:
          nonisopairs[key].append(eval('e.'+key+'['+str(j)+']'))
      else:
        isopairs[key].append(eval('e.'+key+'['+str(j)+']'))

# Convert to numpy for easier processing
for key in isovarnames:
  isovars[key]=np.asarray(isovars[key], dtype=np.float32)
  isopairs[key]=np.asarray(isopairs[key], dtype=np.float32)
  nonisopairs[key]=np.asarray(nonisopairs[key], dtype=np.float32)

# Fill up the tuples
fout = ROOT.TFile(sys.argv[2], 'recreate')
tall = ROOT.TTree('tall', 'tall')
tsig = ROOT.TTree('tsig', 'tsig')
tbkg = ROOT.TTree('tbkg', 'tbkg')
for key in isovarnames:
  tall.Branch(key, isovars[key], 'isovars[{}]/F'.format(isovars[key].size))
  tsig.Branch(key, nonisopairs[key], 'nonisopairs[{}]/F'.format(nonisopairs[key].size))
  tbkg.Branch(key, isopairs[key], 'isopairs[{}]/F'.format(isopairs[key].size))
tall.Fill()
tsig.Fill()
tbkg.Fill()
fout.Write()
fout.Close()
