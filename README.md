Based on the Bassem's TupleProduction from https://gitlab.cern.ch/lhcb-ft/TupleProduction/


Old README:

# Steps to setup the needed environment for the tuple production


----------------------------------------------------------------
Set the latest DaVinci version environment in which 
the latest LokiArrayFunctors code compiles

`cd $HOME`

`lb-dev DaVinci v42r1`

`cd DaVinciDev_v42r1/`

`git init`

`git lb-use Analysis`

----------------------------------------------------------------
Checkout the latest DecayTreeTuple codes

`git lb-checkout Analysis/v18r0 Phys/DecayTreeTuple`

----------------------------------------------------------------
Clone the latest TupleProduction codes (proceedure to be optimised)

`cd $HOME`

`git clone https://:@gitlab.cern.ch:8443/lhcb-ft/TupleProduction.git`

`cd $HOME/DaVinciDev_v42r1/`

`cp $HOME/TupleProduction/src/* $HOME/DaVinciDev_v42r1/Phys/DecayTreeTuple/src/.`

----------------------------------------------------------------
Compile everything (here with 8 cores)

`make -j8`

----------------------------------------------------------------
Run the option files (locally)

`cd $HOME/TupleProduction/options/`

`$HOME/DaVinciDev_v42r1/run gaudirun.py [your_Decay_Option_Script.py]`

----------------------------------------------------------------
Run the option files (on the grid)

`cd $HOME/TupleProduction/options/`

`lb-run Ganga $SHELL`

`ganga gangaSubmit_CMAKE.py`





----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------

# DEPRECATED SETUP VERSION 
Instructions to submit tuple production:

1.) Setup the environment with the special TupleTools
  - Go to your `cmtuser` directory.
  - Get the TupleProduction repository: `git clone https://:@gitlab.cern.ch:8443/lhcb-ft/TupleProduction.git`
  - Setup DaVinci: `SetupProject DaVinci v40r2 --build-env`. You should automatically end up in `~/cmtuser/DaVinci_v40r2`.
  - Copy the TupleTools to the DaVinci directory: `cp -r ../TupleProduction/Aux/Phys .`
  - Compile with `cmt br cmt make` and run another `SetupProject DaVinci v40r2`.

2.) Run the option files
  - run `SetupProject DaVinci v40r2`
  - run locally `cd ~/cmtuser/TupleProduction/options`
  - (optional) Main option files there are: B2Dpi_ALTR.py, B2JpsiX_ALTR.py, you can run over them to check if all is Ok: `gaudirun.py B2Dpi_ALTR.py` (uncomment the link to a test DST on eos in the option file, comment to run with ganga).
  - When you are happy, on a fresh terminal: `SetupProject ganga` and submit your jobs using the script: `TupleProduction/options/gangaSubmit.py`. This script is able to submit *all* the jobs in "one go" without interference from the analyst. However, you can run over individual streams by choosing: `streams   = ['DIMUON' , 'BHADRONCOMPLETEEVENT' ]`.  

3.) Retrival of jobs: By default the tuples will be stored against your grid quota. To copy the tuples to eos FT area you need to run a script run the script:  CopyFromGridToEos after you checked that your jobs have finished. To run you need to type: "SetupGanga ; ganga CopyFromGridToEos.py", Observe the argument :grid_dir_prefix and eos_ana_dir, those refers to your personal grid area and the eos of FTWG.
