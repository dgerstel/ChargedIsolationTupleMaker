// Include files
// from LOKI
#include "GaudiKernel/ToolFactory.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "Event/Particle.h"
#include "Kernel/IPVReFitter.h"
#include "Kernel/IDVAlgorithm.h"
#include <Kernel/GetIDVAlgorithm.h>
#include <Kernel/IDistanceCalculator.h>
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackStateProvider.h"
#include <Kernel/DaVinciFun.h>
#include "TMath.h"
#include "TupleToolVtxCharge.h"
#include <utility>
#include <iostream>
#include <algorithm>
#include "TaggingHelpers.h"
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <algorithm>

//-----------------------------------------------------------------------------
// Implementation file for class : BTaggingAnalysis
//
// Authors: Marco Musy, Marc Grabalosa
//
//-----------------------------------------------------------------------------
using namespace LHCb ;
using namespace DaVinci;
using namespace Gaudi::Units;
using namespace ROOT::Math;
// Declaration of the Algorithm Factory
DECLARE_TOOL_FACTORY( TupleToolVtxCharge  )
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolVtxCharge::TupleToolVtxCharge( const std::string& type,
                                        const std::string& name,
                                        const IInterface* parent)
  : TupleToolBase ( type, name , parent ),
  
  m_utilFT(0),
  m_util(0),
  m_descend(0),
  m_pvReFitter(0),
  m_svtool(0),
  m_fitter(0)
{
  declareInterface<IParticleTupleTool>(this);
  declareProperty("ParticlePath",
                  m_ParticlePath="/Event/Phys/StdAllNoPIDsPions/Particles"
                  );
  declareProperty( "BHypoCriterium",          m_BHypoCriterium   = "MinChi2");
  declareProperty( "ChoosePVCriterium",       m_ChoosePV         = "bestPV" );
  declareProperty( "PVReFit",                 m_ReFitPVs         = true );
  declareProperty( "TagOutputLocation",       m_TagLocation = FlavourTagLocation::Default );
  declareProperty( "SecondaryVertexToolName", m_SecondaryVertexToolName = "SVertexOneSeedTool");
  //preselection cuts
  declareProperty( "IPPU_cut",                m_IPPU_cut    = 3.0 );
  declareProperty( "distphi_cut",             m_distphi_cut = 0.005 );
  declareProperty( "thetaMin_cut",            m_thetaMin    = 0.012 );
  declareProperty( "ghostprob_cut",           m_ghostprob_cut = 0.5 );
}
//=============================================================================
StatusCode TupleToolVtxCharge::initialize() {
  
  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;
  
  m_dva = Gaudi::Utils::getIDVAlgorithm ( contextSvc(), this ) ;
  if (!m_dva) return Error("Couldn't get parent DVAlgorithm");
    
  m_descend = tool<IParticleDescendants> ( "ParticleDescendants", this );
  if( ! m_descend ) {
    fatal() << "Unable to retrieve ParticleDescendants tool "<< endmsg;
    return StatusCode::FAILURE;
  }
  m_util = tool<ITaggingUtilsChecker> ( "TaggingUtilsChecker", this );
  if( ! m_util ) {
    fatal() << "Unable to retrieve TaggingUtilsChecker tool "<< endmsg;
    return StatusCode::FAILURE;
  }
  m_utilFT = tool<ITaggingUtils> ( "TaggingUtils", this );
  if( ! m_utilFT ) {
    fatal() << "Unable to retrieve TaggingUtils tool "<< endmsg;
    return StatusCode::FAILURE;
  }
  m_pvReFitter = tool<IPVReFitter>("LoKi::PVReFitter:PUBLIC", this );
  if(! m_pvReFitter) {
    fatal() << "Unable to retrieve LoKi::PVReFitter:PUBLIC " << endmsg;
    return StatusCode::FAILURE;
  }
  m_svtool = tool<ISecondaryVertexTool> (m_SecondaryVertexToolName, this);
  if(! m_svtool) {
    warning()<< "*** No Vertex Charge tag will be used! "
             << m_SecondaryVertexToolName << endmsg;
  }
  m_fitter = tool<IVertexFit>("LoKi::VertexFitter");
  if ( !m_fitter ) {
    err() << "Unable to Retrieve Default VertexFitter" << endmsg;
    return StatusCode::FAILURE;
  }
  return sc; 
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode TupleToolVtxCharge::fill(const LHCb::Particle* mother
                                     , const LHCb::Particle* P
				     , const std::string& head
                                     , Tuples::Tuple& tuple)
{
  Assert( P && m_dva
          , "No mother or particle, or tools misconfigured." );
  bool test = true;
  if(!P->particleID().hasBottom()) return StatusCode::SUCCESS; 
  //PhysDeskTop
  //const Particle::Range& parts = this->particles();
  // const Particle::Range& parts =  m_dva->particles(); // this is not working !!!
  LHCb::Particle::Range parts ;
  if (exist<LHCb::Particle::Range>(m_ParticlePath))
  {parts = get<LHCb::Particle::Range>(m_ParticlePath); }  
  else {return Error("Nothing found at "+ m_ParticlePath , StatusCode::SUCCESS,1); }
  //----------------------------------------------
  //const RecVertex::Range PVs = m_dva->primaryVertices(); // need to refit ?
  const RecVertex::Range verts=get<RecVertex::Range>(RecVertexLocation::Primary);
  RecVertex PV(0);
  const RecVertex::ConstVector PileUpVtx = choosePrimary(mother, verts, PV);
  const RecVertex* RecVert= (const RecVertex*) &PV;
  if( !RecVert ) {
    err() <<"No Reconstructed Vertex!! Skip." <<endmsg;
    return StatusCode::SUCCESS;
  }
  Particle::ConstVector axdaugh = m_descend->descendants(mother);
  //------------------------------------------------------------------------------
  //fill particle tagging candidates
  const Particle::ConstVector vtags = chooseCandidates(parts, axdaugh, PileUpVtx);
  //std::cout<< "-------- my Tagging Candidates: " << vtags.size() <<std::endl;
  //std::cout<< "-------- particles from desktop: " << parts.size() <<std::endl;
  //------------------------------------------------------------------------------
  //fill seed info for vtxch
  std::vector<Vertex> svertices(0);
  const Particle *SVpart1, *SVpart2;
  FillSeedInfo(tuple, RecVert, vtags, svertices, SVpart1, SVpart2);
  //std::cout << "  Loop on seeds done " << std::endl ;
  std::vector<double>  pIPSV(0), pIPSVerr(0), pDOCA(0), pDOCAerr(0) , pdistphi(0), pdistPhi(0);
  Particle::ConstVector::const_iterator ip;
  
  for( ip = vtags.begin(); ip != vtags.end(); ++ip ) 
  {
    const Particle* axp = (*ip);
    const ProtoParticle* proto = axp->proto();
    double distphi(0);
    m_util->isinTree( axp, axdaugh, distphi );
    //calculate min IP wrt SV
    double ipSV=0, iperrSV=0, docaSV=0, docaErrSV=0;
    if(!svertices.empty()) {
      //std::cout<<" secondary vertices information ============= "<<std::endl ;
      Vertex mySV = svertices.at(0);
      verbose()<< " vertex size: "<<svertices.size()<<endmsg;
      verbose()<< " svpart1 "<<SVpart1->pt()/GeV<<" svpart2 "<<SVpart2->pt()/GeV<<endmsg;
      if( proto != SVpart1->proto() && proto != SVpart2->proto()){
        m_util->calcIP( axp, &mySV, ipSV, iperrSV );
        ipSV=fabs(ipSV);
        verbose()<<" ipSV: "<<ipSV<<endmsg;
        verbose()<<" svpart for doca! svpart1 pt: "<<SVpart1->pt()/GeV<<endmsg;
        m_util->calcDOCAmin( axp, SVpart1, SVpart2, docaSV, docaErrSV);
        //std::cout<<" doca: "<<docaSV<<std::endl;
        
      }else{
        //std::cout<<" proto is = to the SEED particles "<< std::endl ;
      }
    }
    pDOCA       .push_back(docaSV);
    pDOCAerr    .push_back(docaErrSV);
    pIPSV       .push_back(ipSV);
    pIPSVerr    .push_back(iperrSV);
    pdistPhi    .push_back(distphi);
  }
  
  //-------------------------------------------------------
  test &=tuple -> farray ("IPSV",    pIPSV, "vtags", 200);
  test &=tuple -> farray ("IPSVerr", pIPSVerr, "vtags", 200);
  test &=tuple -> farray ("DOCA",    pDOCA, "vtags", 200);
  test &=tuple -> farray ("DOCAerr", pDOCAerr, "vtags", 200);
  test &=tuple -> farray ("distphi", pdistPhi, "vtags", 200);
  ///----------------------------------------------------------------------
  return StatusCode(test) ; 
}
//=============================================================================
const Particle::ConstVector
TupleToolVtxCharge::chooseCandidates(const Particle::Range& parts,
                                     Particle::ConstVector axdaugh,
                                     const RecVertex::ConstVector PileUpVtx) {
  
  double distphi;
  Particle::ConstVector vtags;
  vtags.reserve(32);
  std::vector<const LHCb::Particle*> clones;
  //std::cout<<"executing Fillseed"<<std::endl;
  Particle::Range::const_iterator ip;
  //loop over Particles, preselect tags
  for ( ip = parts.begin(); ip != parts.end(); ++ip){
    //std::cout<<" insdie the loop "<<std::endl;
    const LHCb::Particle* p    = *ip;
    const ProtoParticle* proto = (*ip)->proto();
    if( !proto )                                     continue;
    //std::cout<<"check for protos "<<std::endl;
    if( !proto->track() )                            continue;
    //std::cout<<"check for tracks "<<std::endl;
    /*std::cout <<"   part ID="<<(*ip)->particleID().pid()
              <<" p="<<(*ip)->p()/GeV
              <<" pt="<<(*ip)->pt()/GeV
              <<" theta="<<p->momentum().theta()
              <<" ghostprob="<<proto->track()->ghostProbability()
              <<" PIDm="<<proto->info( ProtoParticle::CombDLLmu, 0)
              <<" PIDe="<<proto->info( ProtoParticle::CombDLLe, 0)
              <<" PIDk="<<proto->info( ProtoParticle::CombDLLk, 0)
              <<" proto="<< proto << std::endl;
    */
    if( p->charge() == 0 )                           continue;
    //std::cout<<"passed neutral veto "<<std::endl;
    bool trackTypeOK = (proto->track()->type() == LHCb::Track::Long) ||
      (proto->track()->type() == LHCb::Track::Upstream);
    if( !trackTypeOK )                               continue;
    //std::cout<<"passed tracktype cuts"<<std::endl;
    if( p->p()/GeV < 2.0 || p->p()/GeV  > 200. )     continue;
    //std::cout<<"passed momentum cuts"<<std::endl;
    if( p->pt()/GeV >  10. )                         continue;
    //std::cout<<"passed second momentum cuts"<<std::endl;
    if( p->momentum().theta()  < m_thetaMin )        continue;
    //std::cout<<"passed theta cuts"<<std::endl;
    if( proto->track()->ghostProbability() > m_ghostprob_cut ) continue;
    //std::cout<<"passed ghost probability cuts"<<std::endl;
    // remove tracks compatible with the signal
    if( m_util->isinTree(*ip, axdaugh, distphi) )    continue;
    //std::cout<<"passed isintree cuts"<<std::endl;
    if (distphi < m_distphi_cut )                    continue;
    //std::cout<<"passed distphi cuts"<<std::endl;
    //calculate the min IP wrt all pileup vtxs
    //eliminate  all parts coming from a pileup vtx
    double ippu, ippuerr;
    m_util->calcIP( *ip, PileUpVtx, ippu, ippuerr );
    if(ippuerr) if( ippu/ippuerr < m_IPPU_cut )      continue;
    //std::cout<<"passed ip cuts"<<std::endl;
    // exclude "trivial" clones: particles with same protoparticle or same
    // underlying track
    //
    // FIXME: this uses the first such track for now, since they have the same
    // hit content, this should be ok...
    using TaggingHelpers::SameTrackStatus;
    using TaggingHelpers::isSameTrack;
    using TaggingHelpers::toString;
    SameTrackStatus isSame = TaggingHelpers::DifferentParticles;
    clones.clear();
    BOOST_FOREACH(const LHCb::Particle* q, vtags) {
      isSame = isSameTrack(*p, *q);
      if (!isSame) continue;
      //std::cout<<"passed same track cuts"<<std::endl;
      // only skip all the rest if actually same track
      if (isSame >= TaggingHelpers::SameTrack) break;
      // otherwise, we may need some form of clone killing, because tracks
      // may be slightly different and we want to pick the "best" one, so
      // save the other contenders for "best track"
      clones.push_back(q);
    }
    if (isSame) {
      counter(std::string("clone/") + toString(isSame))++;
      // if it's actually the same underlying track object, we can throw away
      // this candidate because it is already in vtags
      if (isSame >= TaggingHelpers::SameTrack) continue;
      //std::cout<<"passed same clone track cuts"<<std::endl;
    }
    // exclude pre-flagged clones we did not catch ourselves
    if (proto->track()->hasInfo(LHCb::Track::CloneDist)) {
      if (proto->track()->info(LHCb::Track::CloneDist, 999999.) < 5000.) {
        counter("clone/KL-clone")++;
        //std::cout<<"passed same track cuts 2"<<std::endl;
        continue;
      }
    }

    // ok, if p is a potential clone, we need to find the "best" track and keep
    // only that one
    if (clones.empty()) {
      // no clone, so just store tagger candidate
      //std::cout<<"no clones found "<<std::endl;
      vtags.push_back(p);
    } else { // choose among clones the one to keep
      // complete list of clones
      //std::cout<<"a clones is found and stored in different container "<<std::endl;
      clones.push_back(p);
      counter("clones.size()") += clones.size();

      // functor to sort by quality (want to keep "best" track)
      struct dummy {
        static bool byIncreasingQual(const LHCb::Particle* p, const LHCb::Particle* q){
          //std::cout<<" Hi 1 "<<std::endl;
          if (p->proto() && q->proto()) {
            //std::cout<<" Hi 2 "<<std::endl;
            if (p->proto()->track() && q->proto()->track()) {
              //std::cout<<" Hi 3 "<<std::endl;
              const LHCb::Track &t1 = *p->proto()->track();
              const LHCb::Track &t2 = *q->proto()->track();
              // prefer tracks which have more subdetectors in
              // where TT counts as half a subdetector
              const unsigned nSub1 = (t1.hasVelo() ? 2 : 0) +
                (t1.hasTT() ? 1 : 0) + (t1.hasT() ? 2 : 0);
              const unsigned nSub2 = (t2.hasVelo() ? 2 : 0) +
                (t2.hasTT() ? 1 : 0) + (t2.hasT() ? 2 : 0);
              if (nSub1 < nSub2) return true;
              if (nSub1 > nSub2) return false;
              // if available, prefer lower ghost probability
              const double ghProb1 = t1.ghostProbability();
              const double ghProb2 = t2.ghostProbability();
              if (-0. <= ghProb1 && ghProb1 <= 1. &&
                  -0. <= ghProb2 && ghProb2 <= 1.) {
                if (ghProb1 > ghProb2) return true;
                //std::cout<<" Hi 4 "<<std::endl;
                if (ghProb1 < ghProb2) return false;
              }
              // prefer longer tracks
              if (t1.nLHCbIDs() < t2.nLHCbIDs()) return true;
              if (t1.nLHCbIDs() > t2.nLHCbIDs()) return false;
              // for same length tracks, have chi^2/ndf decide
              const double chi1 = t1.chi2() / double(t1.nDoF());
              const double chi2 = t2.chi2() / double(t2.nDoF());
              if (chi1 > chi2) return true;
              if (chi1 < chi2) return false;
            }
          }
          // fall back on a pT comparison (higher is better) as last resort
          return p->pt() < q->pt();
          //std::cout<<" Hi 5 "<<std::endl;
        }
      };
      // sort clones by quality (stable_sort since byIncreasingQual does not
      // impose a total ordering, so use stable_sort to ensure that the
      // resulting order does not depend on sort implementation)
      std::stable_sort(clones.begin(), clones.end(),dummy::byIncreasingQual);
      // remove all potential clones from vtags
      BOOST_FOREACH(const LHCb::Particle* q, clones) {
        // find potential clone in vtags
        Particle::ConstVector::iterator it =
          std::find(vtags.begin(), vtags.end(), q);
        // if in vtags, remove
        if (vtags.end() != it) vtags.erase(it);
      }
      // get rid of the clones

      // make a disjoint set of tracks by removing the worst track which
      // is a clone of another track until the set is clone-free
      for (std::vector<const LHCb::Particle*>::iterator it = clones.begin(); clones.end() != it; ) {
        bool elim = false;
        for (std::vector<const LHCb::Particle*>::iterator jt = it + 1; clones.end() != jt; ++jt) {
          SameTrackStatus status = isSameTrack(*(*it), *(*jt));
          if (status) {
            // it is a redundant track, remove it from clones and
            // start over
            if (status == TaggingHelpers::ConvertedGamma) {
              // if we have a converted photon, remove the other leg
              // as well
              jt = clones.erase(jt);
            }
            it = clones.erase(it);
            elim = true;
            break;
          }
        }
        if (elim) continue;
        ++it;
      }
      // insert disjoint set of tracks into vtags
      if (!clones.empty()) vtags.insert(vtags.end(), clones.begin(), clones.end());
    }


    /* if (msgLevel(MSG::DEBUG))
      std::cout <<"   part ID (end)="<<(*ip)->particleID().pid()
      <<" p="<<(*ip)->p()/GeV
      <<" pt="<<(*ip)->pt()/GeV
      <<" PIDm="<<proto->info( ProtoParticle::CombDLLmu, 0)
      <<" PIDe="<<proto->info( ProtoParticle::CombDLLe, 0)
      <<" PIDk="<<proto->info( ProtoParticle::CombDLLk, 0)
      <<" proto="<<proto<< std::endl; */
  }
  counter("nCands") += vtags.size();
  return vtags;
}

//=============================================================================
void TupleToolVtxCharge::FillSeedInfo(Tuple& tuple, const RecVertex* RecVert,
                                    const Particle::ConstVector& vtags,
                                    std::vector<Vertex>& svertices,
                                    const Particle *&SVpart1,
                                    const Particle *&SVpart2 ) {
			
  //look for a secondary Vtx due to opposite B (inclusive sec vertex)
  std::vector<double>
    pBOVtx_x(0),  pBOVtx_y(0), pBOVtx_z(0),   pBOVtx_chi2(0),  pBOVtx_zerr(0),
    pSecVtx_x(0), pSecVtx_y(0), pSecVtx_z(0), pSecVtx_chi2(0), pSecVtx_zerr(0),
    pSecVtx_pt1(0), pSecVtx_pt2(0);


  svertices.clear();
  SVpart1=0;
  SVpart2=0;

  //std::cout<<" filling the seeds now "<<std::endl;
  if(m_svtool) svertices = m_svtool -> buildVertex( *RecVert, vtags ); // SVertexOneSeedTool from FT

  counter("nVertices_VtxTag") += svertices.size();

  //std::cout << "  Look seed vertex position " <<  std::endl;
  //std::cout << "  vertices: " <<svertices.size()<<std::endl;

  Vertex tmpseed, tmp_BOvtx;
  int seeds=0;
  //save NEW seed vertex positions
  std::vector<Vertex>::const_iterator isv;
  for (isv=svertices.begin(); isv!=svertices.end(); ++isv) {

    //We keep only the first two tracks, which are the ones which
    //makes the seed. The vertexvector inherits from an old way to
    //to obtain all the possibles seed

    //the svertices.clear is to put the vector at zero and breaks because is the only one
    //the svertices.at(0) put the seed, fitted again with the 2 tracks, breaks because is the only one
    //std::cout<<" inside vertex loop "<<std::endl;
    if (seeds>0) break;//select only first seed
    ++seeds;

    Particle::ConstVector Pfit = (*isv).outgoingParticlesVector();
    //std::cout << "seed 2 tracks, vtx "<< Pfit.size()<< std::endl;
    if(Pfit.size()==0) {
      //std::cout<<" size is zero , why ? "<<std::endl;
      svertices.clear();
      break;
    }

    SVpart1=Pfit.at(0);
    SVpart2=Pfit.at(1);
    m_fitter->fit(tmpseed, *SVpart1,*SVpart2);
    tmpseed.addToOutgoingParticles(SVpart1);
    tmpseed.addToOutgoingParticles(SVpart2);

    pSecVtx_x.push_back(tmpseed.position().x()/mm);
    pSecVtx_y.push_back(tmpseed.position().y()/mm);
    pSecVtx_z.push_back(tmpseed.position().z()/mm);
    pSecVtx_zerr.push_back(tmpseed.covMatrix().At(2,2));
    //std::cout<<"seed xposition: "<<tmpseed.position().x()/mm<<std::endl;
    //std::cout<<"     yposition: "<<tmpseed.position().y()/mm<<std::endl;
    //std::cout<<"     zposition: "<<tmpseed.position().z()/mm<<std::endl;
    pSecVtx_chi2.push_back(tmpseed.chi2PerDoF());
    //std::cout<<"Pfit.at(0) pt: "<<Pfit.at(0)->pt()/GeV<< std::endl ;
    pSecVtx_pt1.push_back(Pfit.at(0)->pt()/GeV);
    pSecVtx_pt2.push_back(Pfit.at(1)->pt()/GeV);
    svertices.at(0)=tmpseed;

    m_fitter->fit(tmp_BOvtx, Pfit);
    pBOVtx_x.push_back(tmp_BOvtx.position().x()/mm);
    pBOVtx_y.push_back(tmp_BOvtx.position().y()/mm);
    pBOVtx_z.push_back(tmp_BOvtx.position().z()/mm);
    pBOVtx_zerr.push_back(tmp_BOvtx.covMatrix().At(2,2));
    pBOVtx_chi2.push_back(tmp_BOvtx.chi2PerDoF());
    
    break;
  }

  tuple -> farray ("SecVtx_pt1", pSecVtx_pt1, "V", 100);
  tuple -> farray ("SecVtx_pt2", pSecVtx_pt2, "V", 100);
  tuple -> farray ("SecVtx_x",   pSecVtx_x, "V", 100);
  tuple -> farray ("SecVtx_y",   pSecVtx_y, "V", 100);
  tuple -> farray ("SecVtx_z",   pSecVtx_z, "V", 100);
  tuple -> farray ("SecVtx_zerr",pSecVtx_zerr, "V", 100);
  tuple -> farray ("SecVtx_chi2",pSecVtx_chi2, "V", 100);
  
  // fill the info of the BO opposite vertex (seed + additional tracks)
  tuple -> farray ("BOVtx_x",    pBOVtx_x, "V", 100);
  tuple -> farray ("BOVtx_y",    pBOVtx_y, "V", 100);
  tuple -> farray ("BOVtx_z",    pBOVtx_z, "V", 100);
  tuple -> farray ("BOVtx_zerr", pBOVtx_zerr, "V", 100);
  tuple -> farray ("BOVtx_chi2", pBOVtx_chi2, "V", 100);
  return ;
  
}

//============================================================================
const RecVertex::ConstVector
TupleToolVtxCharge::choosePrimary(const Particle* AXB,
                                const RecVertex::Range& verts,
                                RecVertex& RecVert) 
{
  const RecVertex* PV(0);
  RecVertex::ConstVector PileUpVtx(0);
  //will contain all the other primary vtx's                                                                         
  if (m_ChoosePV == "bestPV") 
  {
    //choose bestPV according IRelatedPVFinder                                                                                
    PV = (const RecVertex*) m_dva->bestVertex(AXB);
  }
  else 
  {
    double kdmin = 1000000;
    RecVertex::Range::const_iterator iv;
    for(iv=verts.begin(); iv!=verts.end(); ++iv)
    {
      double var(0), ip(0), iperr(0);
      if(m_ChoosePV == "CheatPV") 
      {
      }
      else if(m_ChoosePV=="PVbyIP") 
      {
        m_util->calcIP(AXB, *iv, ip, iperr);
        var=fabs(ip);
      }
      else if(m_ChoosePV=="PVbyIPs") 
      {
        m_util->calcIP(AXB, *iv, ip, iperr);
        if(!iperr)
        {
          err()<<"IPerror zero or nan, skip vertex: "<<iperr<<endmsg;
          continue;
        }
        var=fabs(ip/iperr);
      }
      else 
      {
        err()<<"Invalid option ChoosePVCriterium: "<<m_ChoosePV<<endmsg;
        return PileUpVtx;
      }
      if( var < kdmin ) 
      {
        kdmin = var;
        PV = (*iv);
      }
    }
  }
  if(!PV) 
  {
    err() <<"PV vertex found! Skip. "<<endmsg;
    return PileUpVtx;
  }
  if(m_ReFitPVs)
  {
    RecVertex newPV(*PV);
    Particle newPart(*AXB);
    StatusCode sc = m_pvReFitter->remove(&newPart, &newPV);
    if(!sc) 
    {
      err()<<"ReFitter fails!"<<endmsg;
    }
    else 
    {
      RecVert = newPV;
    }
  }
  else 
  {
    RecVert = *PV;
  }
  double min_chiPV=1000;
  double the_chiPV=1000;
  int nPV=0;
  RecVertex::Range::const_iterator jv;
  for(jv=verts.begin(); jv!=verts.end(); ++jv)
  {
    double chiPV = sqrt(
                        pow((RecVert.position().x()-(*jv)->position().x()),2)/RecVert.covMatrix()(0,0) +
                        pow((RecVert.position().y()-(*jv)->position().y()),2)/RecVert.covMatrix()(1,1) +
                        pow((RecVert.position().z()-(*jv)->position().z()),2)/RecVert.covMatrix()(2,2)
                        );
    if(chiPV < min_chiPV) min_chiPV = chiPV;
    if(chiPV < 3) 
    {
      the_chiPV = chiPV;
      ++nPV;
      continue;
    }
    else
      PileUpVtx.push_back(*jv);
  }
  if(min_chiPV!=the_chiPV || nPV!=1 ) 
  {
    PileUpVtx.clear();
    for(jv=verts.begin(); jv!=verts.end(); ++jv)
    {
      double chiPV = sqrt(
                          pow((RecVert.position().x()-(*jv)->position().x()),2)/RecVert.covMatrix()(0,0) +
                          pow((RecVert.position().y()-(*jv)->position().y()),2)/RecVert.covMatrix()(1,1) +
                          pow((RecVert.position().z()-(*jv)->position().z()),2)/RecVert.covMatrix()(2,2)
                          );
      if(chiPV == min_chiPV) continue;
      else PileUpVtx.push_back(*jv);
    }
  }
  return PileUpVtx;
}

