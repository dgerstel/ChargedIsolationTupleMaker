#ifndef TUPLETOOLVTXCHARGE_H 
#define TUPLETOOLVTXCHARGE_H 1

// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"
#include <Kernel/IDVAlgorithm.h>
#include <Kernel/GetIDVAlgorithm.h>
// from Event 
#include "Event/FlavourTag.h"
//ntuple
#include "GaudiKernel/INTupleSvc.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Kernel/ISecondaryVertexTool.h"
#include "Kernel/IBTaggingTool.h"
#include "MCInterfaces/IPrintMCDecayTreeTool.h"

#include "Kernel/IParticleDescendants.h"
#include "Kernel/IPVReFitter.h"
#include "Kernel/IVertexFit.h"
#include "TrackInterfaces/ITrackManipulator.h"
#include "FlavourTagging/ITaggingUtils.h"
// from local
#include "ITaggingUtilsChecker.h"

/** @class BTaggingAnalysis BTaggingAnalysis.h 
 *  
 *  @author Marco Musy
 *  @date   28/02/2007
 */

using namespace LHCb;
using namespace Gaudi::Units;

class IDVAlgorithm;
class IDistanceCalculator;
class IPVReFitter;
class IVertexFit;


class TupleToolVtxCharge : public TupleToolBase, virtual public IParticleTupleTool {

public: 
  /// Standard constructor
  TupleToolVtxCharge(  const std::string& type,
                       const std::string& name,
                       const IInterface* parent); 
  
  virtual ~TupleToolVtxCharge( ) {}; 
  
  virtual StatusCode initialize();   
  
  virtual StatusCode fill( const LHCb::Particle*
                           , const LHCb::Particle*
                           , const std::string&
                           , Tuples::Tuple& );
  

private:
  std::string m_ParticlePath;
  IDVAlgorithm* m_dva;
  
  const Particle::ConstVector 
  chooseCandidates(const Particle::Range& ,
                   Particle::ConstVector,
                   const RecVertex::ConstVector);

  void FillSeedInfo(Tuple& tuple, 
		    const RecVertex* RecVert,
		    const Particle::ConstVector& vtags,
		    std::vector<Vertex>& svertices,
		    const Particle *&SVpart1, 
		    const Particle *&SVpart2 ) ;
  const RecVertex::ConstVector choosePrimary(const Particle* AXB,
                                             const RecVertex::Range& verts,
                                             RecVertex& RecVert) ;
  std::string m_SecondaryVertexToolName,
    m_TagLocation, 
    m_BHypoCriterium, 
    m_ChoosePV, 
    m_taggerLocation;
  bool m_ReFitPVs;
  
  ITaggingUtils*             m_utilFT;
  ITaggingUtilsChecker*      m_util;
  IParticleDescendants*      m_descend;
  IPVReFitter*               m_pvReFitter;
  ISecondaryVertexTool*      m_svtool;
  IVertexFit *               m_fitter;
  double m_IPPU_cut, m_distphi_cut, m_thetaMin, m_ghostprob_cut;

};

//===========================================================================//
#endif // USER_BTAGGINGANALYSIS_H
